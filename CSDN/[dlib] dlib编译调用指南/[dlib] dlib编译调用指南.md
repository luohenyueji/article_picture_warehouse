dlib是一个C++工具包（DLIB中也有Python接口，但是主要编程语言为C++），包含绝大多数常用的机器学习算法，许多图像处理算法和深度学习算法，被工业界和学术界广泛应用于机器人、嵌入式设备、移动电话和大型高性能计算环境等领域。dlib的开源许可允许您在任何应用程序中免费使用它。在工程实践中，dlib通常和OpenCV结合使用，OpenCV提供图像处理算法，dlib提供机器学习算法。对于从事计算机视觉行业的人，非常推荐学习使用dlib。本文主要讲述dlib在Windows（win10）和linux(ubuntu18.04)下面向C++的编译安装调用。dlib具体介绍见其官网[dlib官网](http://dlib.net/)。

[toc]

# 1 资源
在dlib的github仓库下载对应版本文件。链接地址为：[dlib](https://github.com/davisking/dlib)。此外，在使用dlib中需要使用dlib提供的模型，下载地址见[dlib-models](https://github.com/davisking/dlib-models)。如果网速太慢，可以看看gitee备份。地址为：[dlib-gitee](https://gitee.com/luohenyueji/dlib)和[dlib-models-gitee](https://gitee.com/luohenyueji/dlib-models)。

本文判断dlib是否成功编译的示例代码为dlib\examples目录下的3d_point_cloud_ex.cpp。调用该示例代码后会可视化一个简单的3维点云数据。
代码如下：
```C++
// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This is an example illustrating the use of the perspective_window tool
    in the dlib C++ Library.  It is a simple tool for displaying 3D point 
    clouds on the screen.

*/

#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>
#include <cmath>

using namespace dlib;
using namespace std;

// ----------------------------------------------------------------------------------------

int main()
{
    // Let's make a point cloud that looks like a 3D spiral.
    std::vector<perspective_window::overlay_dot> points;
    dlib::rand rnd;
    for (double i = 0; i < 20; i+=0.001)
    {
        // Get a point on a spiral
        dlib::vector<double> val(sin(i),cos(i),i/4);

        // Now add some random noise to it
        dlib::vector<double> temp(rnd.get_random_gaussian(),
                                  rnd.get_random_gaussian(),
                                  rnd.get_random_gaussian());
        val += temp/20;

        // Pick a color based on how far we are along the spiral
        rgb_pixel color = colormap_jet(i,0,20);

        // And add the point to the list of points we will display
        points.push_back(perspective_window::overlay_dot(val, color));
    }

    // Now finally display the point cloud.
    perspective_window win;
    win.set_title("perspective_window 3D point cloud");
    win.add_overlay(points);
    win.wait_until_closed();
}

//  ----------------------------------------------------------------------------
```

# 2 Windows下C++编译安装调用dlib
Windows下的编译使用很简单，不需要其他的额外安装库，直接拉取dlib的仓库就行了。本文将dlib仓库放在D:\packages\dlib路径下，然后通过cmake和vs2017编译使用dlib。这一部分参考文章[【C++】VS2019+Dlib安装及整合详细步骤](https://blog.csdn.net/Feeryman_Lee/article/details/103203152)。
## 2.1 编译
**step1** 
在D:\packages\dlib路径下创建build文件夹和install文件夹，build文件夹用于存放相关编译数据，install保存最后需要调用的生成文件。文件结构如下图所示：
![](image/1.jpg)
**step2** 配置dlib库
打开cmake-gui，配置dlib库。cmake安装地址为：[cmake](https://cmake.org/download/)。打开cmake-gui后设置源代码目录地址和生成文件地址，点击configure。如图所示：
![](image/2.jpg)
然后配置编译器，选择对应的编译器版本，再选择编译平台版本x64。最后点击finish，cmake将会自动编译文件。
![](image/3.jpg)

在编译过程会出现找不到cuda的情况，忽视就行了。然后注意将指定的安装目录CMAKE_INSTALL_PREFIX这一项改为D:\packages\dlib\install。然后再点击configure，如果没有标红，点击generate。

![](image/4.jpg)

**step3** 
生成相关文件，generate成功后打开OpenProject即可，这样就会打开vs2017。 如下图所示：
![](image/5.jpg)
打开后的vs2017界面如下图所示，确定编译平台为debug/x64，然后点击生成-生成解决方法即可，如下图所示。这样的好处是知道哪些模块生成失败。如果有生成译错误，检查即可。这一过程约3分钟，不同机器时间不一样。
![](image/6.jpg)
生成成功后，如下图所示。可以有生成跳过，但是不能有生成失败。
![](image/7.jpg)

如果生成后没有失败的，选择解决方案-INSTALL-仅用于项目-仅生成INSTALL，如下图所示：
![](image/8.jpg)

此外以上操作只能生成dlib Debug版本。Release版本需要修改配置平台，重复以上操作。如下图所示：
![](image/9.jpg)

最后如果install文件夹中有include文件夹和lib文件夹，表明编译过程成功。如下图所示：
![](image/10.jpg)

## 2.2 配置与使用
新建vs2017项目，然后选择属性管理器，新建属性列表dlib如下图所示。这样该dlib属性列表以后可以重复导入使用，不需要每次新建工程都配置。
![](image/11.jpg)
修改Release|X64模式下的dlib属性，修改VC++目录下的可执行目录，库目录。如下图所示：
![](image/12.jpg)

包含目录设置如下，添加头文件：
> D:\packages\dlib\install\include

库目录设置如下：
> D:\packages\dlib\install\lib

然后修改链接器-输入-附加依赖项，如下图所示：
![](image/13.jpg)

附加依赖项设置如下。注意不同版本dlib编译生成的lib不一样，注意区分，如果是debug就调用相应的debug版本，都在install\lib目录下。
> dlib19.22.99_release_64bit_msvc1916.lib

然后调用本文在第一节提到的示例代码3d_point_cloud_ex.cpp文件，结果如下图所示，就是一个展示3维点云的代码，可以拖动或放大缩小图形。
![](image/14.jpg)

# 3 Ubuntu下C++编译安装调用dlib
## 3.1 编译
ubuntu下编译使用dlib比windows下稍微复杂，因为ubuntu缺少一些dlib所需要的库，有时会出现莫名其妙的错误，一般来说都是缺少图形显示库，比如linux下dlib需要x11图像化界面（windows不需要，其他系统图像化界面可能不一样），所以先安装x11。如果不安装可能会出现X11相关错误。这一部分参考文章[ubuntu下使用 dlib](https://blog.csdn.net/qq_34106574/article/details/85626465)和[cmake-cant-find-x11](https://askubuntu.com/questions/526848/cmake-cant-find-x11)

> sudo apt-get install libx11-dev

然后进入dlib根目录，输入以下命令：
> mkdir build; cd build; cmake .. -DUSE_AVX_INSTRUCTIONS=1; make -j12

上述命令会编译AVX，来加速CPU运行，如果出现错误使用以下命令编译：
> mkdir build; cd build; cmake ..; make -j12

## 3.2 配置与使用
新建一个文件夹dlib-test，将3d_point_cloud_ex.cpp复制到该文件夹。进入该文件夹，新建CMakeList.txt文件，CMakeList.txt内容如下。注意CMake文件链接dlib库的路径地址

```
cmake_minimum_required(VERSION 2.8.12)
# 工程名
project(dlib_test)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O2 -DDLIB_JPEG_SUPPORT")
 
IF(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Weverything")
ELSEIF(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")
ENDIF()

# 包含OpenCV
find_package(OpenCV REQUIRED)
if (OpenCV_FOUND)
   include_directories(${OpenCV_INCLUDE_DIRS})
   message("OpenCV found")
endif()

# 包含X11
find_package(X11 REQUIRED)
if (X11_FOUND)
   include_directories(${X11_INCLUDE_DIR})
   message("X11 found")
endif()

#  添加dlib
include_directories(/home/{yourpath}/dlib)
link_directories(/home/{yourpath}/dlib/build/dlib/)

# 添加代码
add_executable(dlib_test 3d_point_cloud_ex.cpp)
# 链接库
# libdlib.a - lpthread - lX11分别表示链接dlib，多线程，X11
target_link_libraries(dlib_test ${OpenCV_INCLUDE_LIBS} ${X11_LIBRARIES} libdlib.a -lpthread -lX11)
```

然后输入以下命令，即可编译运行示例代码：
> mkdir build; cd build; cmake ..; make -j12; ./dlib_test

如果出现第二节的三维点云图形，表明dlib安装成功。

# 4 Python安装调用dlib
通常直接用pip install dlib会出错，所以需要安装一系列的库,主要是cmake、Boost。具体如下：
**windows**:
```
pip install cmake
pip install dlib
```

**linux**:
```
sudo apt-get install libx11-dev
sudo apt-get install libboost-all-dev
sudo apt-get install cmake
python3 -m pip install dlib
```

安装成功后，在python环境import dlib即表明安装成功，当然也可以编译安装，具体步骤见官方仓库文档。


# 5 参考
+ [dlib官网](http://dlib.net/)
+ [dlib](https://github.com/davisking/dlib)
+ [dlib-models](https://github.com/davisking/dlib-models)
+ [dlib-gitee](https://gitee.com/luohenyueji/dlib)
+ [dlib-models-gitee](https://gitee.com/luohenyueji/dlib-models)
+ [【C++】VS2019+Dlib安装及整合详细步骤](https://blog.csdn.net/Feeryman_Lee/article/details/103203152)
+ [ubuntu下使用 dlib](https://blog.csdn.net/qq_34106574/article/details/85626465)
+ [cmake-cant-find-x11](https://askubuntu.com/questions/526848/cmake-cant-find-x11)
