Python的Diagrams库允许通过简单的Python代码绘制云系统架构，实现对新的系统架构进行原型设计。Diagrams的官方仓库地址见：[diagrams](https://github.com/mingrammer/diagrams)。Diagrams的官方文档和使用示例见：[diagrams-doc](https://diagrams.mingrammer.com/docs/getting-started/examples)。

# 0 安装

Diagrams库依赖于开源的图可视化工具[Graphviz](https://graphviz.org/)。Python下的Graphviz使用见[python模块graphviz使用入门](https://blog.csdn.net/LuohenYJ/article/details/106172201)。Graphviz在windows下的安装见[Graphviz安装配置教程](https://blog.csdn.net/qq_42257666/article/details/121688656)。在linux下的安装，命令行输入以下指令即可：

> sudo apt-get install graphviz

> sudo apt-get install graphviz graphviz-doc

Diagrams库需要在Python3.6及以上环境运行，安装graphviz后，输入以下指令安装diagrams库：

> pip install -U diagrams

安装完成后，以下代码成功输出结果表示Diagrams库安装成功。


```python
# 代码示例
from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB

with Diagram("Web Service", show=False):
    ELB("lb") >> EC2("web") >> RDS("userdb")
```


```python
# 展示图片
# from IPython.display import Image
# Image(url= "web_service.png")

# 使用Pillow库
from PIL import Image  

image = Image.open("web_service.png")
image.show()
```


![png](output_4_0.png)


# 1 模块说明

## 1.1 Diagrams

Diagrams是绘制图像的主要模块，Diagrams用于设置全局图上下文。

Diagram构造函数的第一个参数将设置图片名称，如果没有设置输出图片文件名，将用图片名称做为输出图片的文件名。


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2

# 输出文件图片名会自动将大写字母转为小写，空格转为下划线，并默认输出png格式图片
with Diagram("SIMPLE IMAGE",show=False):
    EC2("web")
```


```python
# 展示图片
image = Image.open("simple_image.png")
image.show()
```


![png](output_9_0.png)


可以通过设置outformat和filename来设置输出图片的格式和文件名。


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2

with Diagram("Simple Diagram", outformat="jpg", filename="MY Diagram",show=False):
    EC2("web")
```


```python
# 展示图片
image = Image.open("MY Diagram.jpg")
image.show()
```


![png](output_12_0.png)


此外还可以设置Graphviz中的graph_attr, node_attr和edge_attr属性来控制图形参数，这些属性具体介绍含义见[Graphviz_Attributes](https://www.graphviz.org/doc/info/attrs.html)。


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2

graph_attr = {
    "fontsize": "24",
    "bgcolor": "transparent",
    "fontcolor": "red"
}

with Diagram("Simple Diagram", show=False, graph_attr=graph_attr):
    EC2("web")
```


```python
# 展示图片
image = Image.open("simple_diagram.png")
image.show()
```


![png](output_15_0.png)


## 1.2 Nodes

节点是图表的基本组成部分。一个节点对象由三部分组成：提供者、资源类型和节点名称。

例如下面的示例，aws表示提供者，compute表示资源类型，EC2表示节点名称。可以查看官方文档，看看不同节点的使用。比如Azure、Alibaba、IBM提供的节点。

```Python
from diagrams import Diagram
from diagrams.aws.compute import EC2

with Diagram("Simple Diagram"):
    EC2("web")
```

通过使用以下运算符连接节点来表示数据流：
```Python
>> : 从左到右连接节点。
<< : 从右到左连接节点。
- ：无方向连接节点。无向的。
```


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB
from diagrams.aws.storage import S3

# 先声明的数据流后渲染
with Diagram("Web Services", show=False):
    ELB("lb") >> EC2("web") >> RDS("userdb") >> S3("store")
    ELB("lb") >> EC2("web") >> RDS("userdb") << EC2("stat")
    (ELB("lb") >> EC2("web")) - EC2("web") >> RDS("userdb")
```


```python
# 展示图片
image = Image.open("web_services.png")
image.show()
```


![png](output_19_0.png)


通过设置Diagram的direction参数，可以设置图表数据流的方向，默认从左到右（LR)，参数如下：

+ TB，从上往下
+ BT，从下往上
+ LR，从左往右
+ RL，从右往左


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB

with Diagram("Workers", show=False, direction="BT"):
    lb = ELB("lb")
    db = RDS("events")
    lb >> EC2("worker1") >> db
    lb >> EC2("worker2") >> db
    lb >> EC2("worker3") >> db
    lb >> EC2("worker4") >> db
    lb >> EC2("worker5") >> db

# 展示图片
image = Image.open("workers.png")
image.show()
```


![png](output_21_0.png)


当然也可以将多个节点表示为一组，即将多个节点放入一个python列表中，但是不支持列表与列表之间进行直接连接。


```python
from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB

with Diagram("Grouped Workers", show=False, direction="TB"):
    ELB("lb") >> [EC2("worker1"),
                  EC2("worker2"),
                  EC2("worker3"),
                  EC2("worker4"),
                  EC2("worker5")] >> RDS("events")

# 展示图片
image = Image.open("grouped_workers.png")
image.show()
```


![png](output_23_0.png)


## 1.3 Clusters

通过创建Cluster部件，可以将部分节点进行聚合，并与其他节点隔离开来。


```python
from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS
from diagrams.aws.database import RDS
from diagrams.aws.network import Route53

with Diagram("Simple Cluster", show=False):
    dns = Route53("dns")
    web = ECS("service")
    
    # 设置集群，第一值为集群名
    with Cluster("DB Cluster"):
        db_primary = RDS("primary")
        db_primary - [RDS("replica1"),
                     RDS("replica2")]
    # 设置数据流方向
    dns >> web >> db_primary

# 展示图片
image = Image.open("simple_cluster.png")
image.show()
```


![png](output_25_0.png)


同时也支持创建嵌套节点，嵌套节点中也可以创建多个cluster。


```python
from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS, EKS, Lambda
from diagrams.aws.database import Redshift
from diagrams.aws.integration import SQS
from diagrams.aws.storage import S3

with Diagram("Event Processing", show=False):
    source = EKS("k8s source")

    with Cluster("Event Flows"):
        with Cluster("Event Workers"):
            workers = [ECS("worker1"),
                       ECS("worker2"),
                       ECS("worker3")]

        queue = SQS("event queue")

        with Cluster("Processing"):
            handlers = [Lambda("proc1"),
                        Lambda("proc2"),
                        Lambda("proc3")]

    store = S3("events store")
    dw = Redshift("analytics")
    
    # 设置数据流方向
    source >> workers >> queue >> handlers
    handlers >> store
    handlers >> dw

# 展示图片
image = Image.open("event_processing.png")
image.show()
```


![png](output_27_0.png)


## 1.4 Edges

edge用于设置节点间连接线的属性。包含三个属性：标签label、颜色color和样式style，它们对应于Graphviz的edge属性设置。


```python
from diagrams import Cluster, Diagram, Edge
from diagrams.alibabacloud.compute import ElasticSearch
from diagrams.alibabacloud.compute import FunctionCompute
from diagrams.alibabacloud.compute import ContainerService

with Diagram(name="Web Service", show=False):
    # 设置三个节点
    service = ContainerService("service")
    compute = FunctionCompute("compute")
    search = ElasticSearch("search")
    # 设置连接方式
    service >> Edge(color="firebrick", style="dashed") >> compute
    # 设置连接方式
    compute - Edge(color="orange", label="link") >> search
    service  >> search


# 展示图片
image = Image.open("web_service.png")
image.show()
```


![png](output_29_0.png)


# 2 参考

+ [diagrams](https://github.com/mingrammer/diagrams)
+ [diagrams-doc](https://diagrams.mingrammer.com/docs/getting-started/examples)
+ [Graphviz](https://graphviz.org/)
+ [python模块graphviz使用入门](https://blog.csdn.net/LuohenYJ/article/details/106172201)
+ [Graphviz安装配置教程](https://blog.csdn.net/qq_42257666/article/details/121688656)
+ [Graphviz_Attributes](https://www.graphviz.org/doc/info/attrs.html)
