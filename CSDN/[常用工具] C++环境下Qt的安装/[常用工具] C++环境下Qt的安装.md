

# 1 Qt（C++）版本的选择
Qt（C++）是一个跨平台的C++图形用户界面库，Qt安装程序分为商业版本和开源版本。个人和小型团队选择开源版本更实用。Qt的版本号一般由3个数值表示，如5.14.2是完整的Qt版本号，第一个数字5表示大版本号（major），第二个数字14表示小版本号（minor），第三个数字2表示补丁号（patch）。当两个版本的大版本号和小版本号数字相同，那么这两个Qt版本的功能就是一致的，比如5.14.*系列各版本功能都是一样的。但是大版本号更高并不如意味着功能越多，比如Qt 6系列更新到了Qt 6.2版本，才覆盖Qt 5.15中大部分的功能。
目前Qt系列最新版本为Qt 6.3.1（2022年6月发布），但是国内选择Qt 6进行开发的团队不多，一方面是因为Qt 6系列功能不稳定，对win10以下系统支持不友好。另外一方面因为Qt 5.14.2（2020年4月发布）版本之后，开源版本只提供在线安装程序，离线安装程序必须购买商业版本。不过新的项目在win10下使用Qt 6.2 以上版本进行开发还是非常不错的选择，毕竟Qt 6解决了许多Qt 5中的bug。如果是追求稳定，那么Qt最后一个离线安装版本Qt 5.14.2是比较好的选择。因此这里介绍Qt 6.3.1和Qt 5.14.2的安装。

# 2 Qt 安装
## 2.1 Qt 6.3.1的安装

Qt 6的安装程序下载地址如下：
商用版本下载：
https://www.qt.io/download
开源版本下载（国内下载太慢）：
https://download.qt.io/official_releases/online_installers/
清华镜像（国内使用）：
https://mirrors.tuna.tsinghua.edu.cn/qt/development_releases/online_installers/
所有Qt版本下载列表：
https://download.qt.io/archive/qt/


**Step 0**

从下面开源版本列表中选择window-online，下载最新的Qt安装器。

![](image/image001.png)

如果是国内使用，推荐使用清华源镜像：

![](image/image002.jpg)

**Step 1**

安装程序的各个语言安装版本内容是一样的，打开安装程序后，第一个界面是欢迎welcome。需要登录Qt用户，没有点击注册即可。或者在线注册也可以，注册网站：https://login.qt.io/register。

![](image/image003.png)

**Step 2**

登录后进入开源义务界面open source obligations，开源版本条件勾选两个，注意核对版权信息。

![](image/image004.jpg)

**Step 3**

安装程序界面Setup，就是一句欢迎词，直接点击下一步。点击后会远程同步一些内容。

![](image/image005.jpg)

**Step 4**

询问你是否运行Qt收集个人使用数据，一般不同意。

![](image/image006.png)

**Step 5**

安装文件夹界面Installation Folder，主要设置安装路径，Qt 6支持安装在任意路径下，但是安装目录名以及路径中所有目录名都应为英文字符，且不能有空格。所以建议只更改安装盘符，比如将C改为D。安装方式选择Custom installation，这样可以自行勾选安装组件，然后直接点击下一步即可。

![](image/image007.jpg)

**Step 6**

对于组件的选择看自己的安装环境，要安装的组件一般根据个人需求进行选择，全部安装非常耗时和占用硬盘空间。这里选择的是Qt 6.3.1安装，选择的是MinGW编译环境。MSVC指微软的编译器Microsoft Visual C++ Compiler。如果选择MSVC2019作为编译器，还需要安装vs2019，并且安装相应的build tool。MSVC2019版本相关库比较齐全，但是仅仅针对pc平台，而且安装Qt过程比较麻烦，但对于一些第三方库如OpenCV配置比较简单。MinGW跨平台，Qt安装简单，一些第三方库如OpenCV则需要自行通过cmake源码编译。这里为了方便，用的是Qt自带的开发环境Qt Creator，就选择了MinGW。

![](image/image008.jpg)

此外，在组件选择最下方 Developer and Designer Tools，如果选择了MinGW作为编译器，还需要选择MinGW版本和调试工具。CMake编辑工具和Ninja系统构建工具建议也选择。然后直接下一步。有些组件没安装不需要担心，安装完后，在线安装允许修改删除添加组件。

![](image/image009.jpg)

**Step 7**

许可协议License Agreement，直接选择同意就好，下一步。

![](image/image010.jpg)

**Step 8**

开始菜单快捷方式，这个一般默认就行了，直接下一步。

![](image/image011.jpg)

**Step 9**

准备安装，看看磁盘是否这么多空间，然后点击安装。

![](image/image012.jpg)

**Step 10**

正在安装，Qt开始下载，视网速决定下载速度。

![](image/image013.jpg)

**Step 11**

打开Qt Creator集成开发环境，点击文件-新建项目，然后测试安装即可。

![](image/image014.jpg)

自Qt 6开始，已经默认禁用了Qt Quick Designer，如果要更改插件。点击帮助-关于插件就可以重新选用。

![](image/image015.jpg)

## 2.2 Qt 5.14.2的安装

Qt 5的安装程序下载地址如下：
https://download.qt.io/archive/qt/5.14/5.14.2/

**Step 0**

点击下载qt-opensource-windows-x86-5.14.2.exe即可。这是离线安装包，需要一定的下载时间。

![](image/image016.jpg)

**Step 1**

点击下载好的Qt 5.14.2安装包，进入欢迎页面，直接下一步。

![](image/image017.jpg)

**Step 2**

Qt 账户登录，如果断开网络会跳过这个界面。

![](image/image018.jpg)

点击下一步。

![](image/image019.jpg)

**Step 3**

选择安装文件夹，安装目录名以及路径中所有目录名都应为英文字符，且不能有空格。如需更改路径建议只更改安装盘符，比如将C改为D。

![](image/image020.jpg)

**Step 4**

选择要安装的组件。对于Qt 5.14.2选项，如果选择MSVC要安装对应版本的visual studio。这里选择安装MinGW 64位版本。对于Developer and Designer Tools选项，选择Qt creator调试工具，还有对应的MinGW 64位版本。

![](image/image021.jpg)

**Step 5**

许可协议，直接同意，进入下一步。

![](image/image022.jpg)

**Step 6**

安装程序快捷方式，默认即可。

![](image/image023.jpg)

**Step 7**

直接安装即可，注意磁盘空间是足够的。

![](image/image024.jpg)

**Step 8**

等待安装完成即可。

![](image/image025.jpg)

**Step 9**

打开Qt Creator集成开发环境，点击文件-新建项目，然后测试安装即可。

![](image/image026.jpg)

# 3 Qt 其他版本安装

Qt 有一个官方资源下载网站：https://download.qt.io/。
该网站各个目录如下所示。红字表示能够下载到各个发布版的安装程序。推荐进入archive目录进行下载，该目录下安装版本最全。

![](image/image027.jpg)

archive目录下各文件夹介绍如下：Qt 5.15版本及以上进入online_installers目录进行下载安装。Qt 其他版本进入qt/目录下载安装包。

![](image/image028.jpg)

进入qt/目录后，能够看到各历史Qt 版本源代码目录，进入对应的版本目录即可下载安装包，安装步骤和Qt 5.14差不多，大概安装步骤就这些。

![](image/image029.jpg)