cvat是一个非常好用的标注工具，但是也是非常难以安装的标注工具，所以本文简单讲一讲如何安装与使用cvat。cvat最好在ubuntu18.04安装，windows平台安装难度很大，然后在其他平台使用。

[toc]

# 1 安装

安装其实一步一步按照官方教程执行就好了，官方地址文档为[cvat安装文档](https://github.com/openvinotoolkit/cvat/blob/develop/cvat/apps/documentation/installation.md)。安装最大的问题就是网速不好。具体步骤如下。

**step1 安装docker**

cvat在docker下运行，所以慢慢安装吧。

```
sudo apt-get update
sudo apt-get --no-install-recommends install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
sudo apt-get update
sudo apt-get --no-install-recommends install -y docker-ce docker-ce-cli containerd.io
```

**step2 获取权限**

在没有root权限的情况下运行docker需要获取权限，获取权限后务必重启系统。

```
sudo groupadd docker
sudo usermod -aG docker $USER
```

**step3 获取权限**

安装docker-compose（1.19.0或更高版本）。Compose是用于定义和运行多容器Docker应用程序的工具

```
sudo apt-get --no-install-recommends install -y python3-pip python3-setuptools
sudo python3 -m pip install setuptools docker-compose
```

**step4 克隆cvat源代码**

官方github仓库很慢，所以我就用了gitee镜像。

```
sudo apt-get --no-install-recommends install -y git
git clone https://gitee.com/luohenyueji/cvat
cd cvat
```

**step5 构建docker镜像**

这一步是最难也是耗时最长的一步，会下载很多东西包括一堆python库，所以慢慢等待。对于python库得安装建议使用镜像。具体做法，打开上一步下载的源代码目录cvat/Dockerfile文件，找到Install requirements这项。然后修改并添加相应的python镜像。我用的是阿里云镜像，可以换成别的。
```
#RUN DATUMARO_HEADLESS=1 python3 -m pip install --no-cache-dir -r /tmp/requirements/${DJANGO_CONFIGURATION}.txt
RUN DATUMARO_HEADLESS=1 python3 -m pip install -r /tmp/requirements/${DJANGO_CONFIGURATION}.txt -i https://mirrors.aliyun.com/pypi/simple/
```

然后在cvat目录输入以下指令就慢慢等待吧，如果中途安装失败，再次输入指令就好了，注意要使用sudo。
```
sudo docker-compose build
```

**step6 运行Docker容器**
这一步要下载公共docker映像，耗时看网速，但是不会太久。
```
docker-compose up -d
```

这一步结束后，就能打开你的cvat网站，但是需要用谷歌浏览器使用。安装过了谷歌浏览器就不用管，没有见安装方法[ubuntu18.10安装chrome浏览器](https://blog.csdn.net/dair6/article/details/113730812)。

这时候谷歌浏览器打开localhost:8080就能够看到cvat页面，如下所示
![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/1.jpg)

点击creaate an account就能够创建普通用户，但是第一次使用最好创建管理员账户，具体看下一步。

**step6 创建管理员用户**

输入以下命令，然后有提示创建管理员账户密码就行了。
```
docker exec -it cvat bash -ic 'python3 ~/manage.py createsuperuser'
```

**step7 关闭cvat服务**

运行step6后，哪怕重启服务器，cvat服务还是继续运行的。要关闭在cvat目录输入以下指令：
```
docker-compose down
```

要重新开启就重复step6。


# 2 使用

cvat的使用具体不多说，官方文档很详细，或者查看文章[CVAT 用户指南](https://alex007.blog.csdn.net/article/details/107711076)。本文具体讲几个配置要点。

## 2.1 其他机器访问cvat服务器

如果要在cvat服务器之外访问，需要指定CVAT_HOST环境变量。最好的方法是创建docker-compose.override.yml并将所有其他设置放在此处。具体做法先把docker-compose.yml复制到cvat目录后命名为docker-compose.override.yml。然后修改docker-compose.override.yml。改动cvat_proxy/environment/CVAT_HOST，将localhost改为你的服务器ip，例如改服务器ip为114.114.114.114。


```
  cvat_proxy:
    container_name: cvat_proxy
    image: nginx:stable-alpine
    restart: always
    depends_on:
      - cvat
      - cvat_ui
    environment:
      # CVAT_HOST: localhost
      CVAT_HOST: server ip 114.114.114.114
    ports:
      - '8080:80'
```

然后step6启动的命令改为
```
docker-compose -f docker-compose.override.yml up
```

然后在另外一台电脑（系统版本无所谓可以是windows或者linux）的谷歌浏览器中，输入114.114.114.114:8080即可访问。要注意的是，重启或者关闭命令行后，服务自动关闭。但是关闭命令窗口前需要输入step7的关闭命令。

## 2.2 共享目录配置

通常情况下我们创建标注任务，需要自己上传数据或者选择某个硬盘地址的数据。自己上传数据当数据量过大很容易崩溃（大概超过1万张图片就会特别慢），所以大型数据选择某个硬盘地址的数据最靠谱，就是下图中的connected file share。

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/2.jpg)

具体需要修改我们上一步的docker-compose.override.yml文件，一共需要添加两处。

**第一处**

在services/cvat/environment中添加CVAT_SHARE_URL项，内容为"Mounted from {/home/my/data} host directory"。**{}中是你要载入的数据地址，注意修改**。然后services/cvat/volumes中添加cvat_share项，内容为- cvat_share:/home/django/share:r。

```
services:

  cvat:
    environment:
      DJANGO_MODWSGI_EXTRA_ARGS: ''
      ALLOWED_HOSTS: '*'
      CVAT_REDIS_HOST: 'cvat_redis'
      CVAT_POSTGRES_HOST: 'cvat_db'
      CVAT_SHARE_URL: "Mounted from {/home/my/data} host directory" # 新加
    volumes:
      - cvat_data:/home/django/data
      - cvat_keys:/home/django/keys
      - cvat_logs:/home/django/logs
      - cvat_models:/home/django/models
      - cvat_share:/home/django/share:ro # 新加
```

**第二处**

第二处在volumes下添加cvat_share项目，直接复制下面的命令就行了，注意将volumes/cvat_share/driver_opts/device中的内容改为你要载入的数据地址。

```
volumes:
  cvat_db:
  cvat_data:
  cvat_keys:
  cvat_logs:
  cvat_models:
  cvat_share: # 整块新加
    driver_opts:
      type: none
      device: {/home/my/data}
      o: bind
```

然后重启，建立任务时就可以选择硬盘文件路径地址了。

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/3.jpg)

## 2.3 标注替换

cvat能够通过模型打伪标签，实现自动标注的功能，但是载入模型比较麻烦。个人觉得这个自动标注功能最好不要用，直接本地模型给图像打标注，然后替换标注文件即可。或者你想查看已有标注文件也可以用这种方式。具体如下

**step1 下载标注**

建立图像数据标注任务，然后下载你本地保存同类型的标注，比如我已经有了当前demo任务的pascal标注，我就下载pascal标注。如下图所示：

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/4.jpg)

**step2 替换标注文件**

在远程服务器下载标注文件后，会得到一个压缩包，解压压缩包找到标注文件。这些标注文件都是空的，用已有标注文件替换这些标注文件。然后原封不动重新打包压缩整个文件夹。

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/5.jpg)

重新打包后的压缩文件包注意和原来的文件包内容都是一样的，比如原来是a.zip，现在我们上传的压缩包除了标注，其他和a.zip中的内容一样。然后压缩文件夹，我们可以改名为b.zip，但是不能是a.rar。

**step3 上传标注**

我们直接上传上一步的标注压缩包，替换文件就行了。

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/6.jpg)

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/7.jpg)

最后倒入后的标注如下图所示，成功搞定。

![](https://gitee.com/luohenyueji/article_picture_warehouse/raw/master/CSDN/%5B%E5%B8%B8%E7%94%A8%E5%B7%A5%E5%85%B7%5D%20cvat%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8C%97/image/8.jpg)

## 2.4 其他使用

**用户权限管理**

比如管理普通用户能否标注，创建任务，可以参考[CVAT用户管理](https://blog.csdn.net/irving512/article/details/110435730)

**windows下安装cvat**

参考[CVAT windows安装](https://blog.csdn.net/qq_38689395/article/details/105771849)。但是非常不建议在windows下安装cvat。

**cvat自动标注**

参考[cvat自动标注](https://docs.onepanel.ai/docs/getting-started/use-cases/computervision/annotation/cvat/cvat_annotation_model)实现cvat加载模型标注数据。cvat自动标注也非常不推荐使用，具体查看2.3。

# 3 参考

+ [cvat安装文档](https://github.com/openvinotoolkit/cvat/blob/develop/cvat/apps/documentation/installation.md)
+ [ubuntu18.10安装chrome浏览器](https://blog.csdn.net/dair6/article/details/113730812)
+ [CVAT 用户指南](https://alex007.blog.csdn.net/article/details/107711076)
+ [Ubuntu18.04 CVAT配置/安装](https://blog.csdn.net/Castlehe/article/details/107529261)
+ [CVAT用户管理](https://blog.csdn.net/irving512/article/details/110435730)
+ [CVAT windows安装](https://blog.csdn.net/qq_38689395/article/details/105771849)
+ [cvat自动标注](https://docs.onepanel.ai/docs/getting-started/use-cases/computervision/annotation/cvat/cvat_annotation_model)