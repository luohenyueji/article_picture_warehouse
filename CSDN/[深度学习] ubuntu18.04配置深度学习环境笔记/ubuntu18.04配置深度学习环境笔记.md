[toc]


最近装过很多ubuntu18.04系统的nvidia驱动，cuda10.2，cudnn7.6.5，发现每次都会出现一些小问题。总结了具体步骤，做个记录。主要分为三个步骤：驱动安装，cuda安装，cudnn安装。本文主要参考了博客[Ubuntu18.04安装CUDA10、CUDNN](https://blog.csdn.net/qq_32408773/article/details/84112166)和[Ubuntu18.04+CUDA10.2 深度学习开发环境配置指南](https://blog.csdn.net/abcdefg90876/article/details/105781005/)。本文也适用于其他linux系统安装不同版本cuda,cudnn。

# 1 nvidia驱动安装
安装驱动前，最好禁用nouveau驱动，这一步是建议，也可以不做。**本文方法不需要禁止，我没有禁用这个nouveau驱动**。禁用的教程见：[Ubuntu下安装INVIDIA显卡驱动（避免循环登录问题）
](https://blog.csdn.net/qq_32408773/article/details/84111244)

nvidia驱动先通过以下命令查看你的系统支持的版本：
> ubuntu-drivers devices

结果如下所示：
![](image/1.jpg)

一般情况下是安装红线对应的recommend驱动，但是如果不是安装cuda10.2需要查看cuda版本和nvidia驱动版本是否对应。具体查看[CUDA Toolkit and Compatible Driver Versions](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html)就有相关说明。

如下图所示，可以看到要安装的nvidia-driver-455符合cuda10.2的版本要求。

![](image/2.jpg)

然后就是直接输入命令安装了，不需要自己下载驱动。命令如下：

```
sudo apt-get purge nvidia* 
sudo add-apt-repository ppa:graphics-drivers/ppa 
sudo apt-get update 
sudo apt-get install nvidia-driver-455
```


应该是傻瓜式安装，安装完后直接重启系统。
> sudo reboot

# 2 CUDA10.2安装
本文安装的是cuda10.2版本。进入[CUDA历史版本下载页面](https://developer.nvidia.com/cuda-toolkit-archive)。进去后先点击要安装的版本页面，

![](image/3.jpg)

打开版本页面后，选择系统和版本信息。这里选择runfile安装，如下所示：

![](image/4.jpg)


**这个cuda安装包国内下载可能会很慢，可以复制wget后面的下载链接，用迅雷、IDM等下载工具先行下载。**

**如果以上方式还是下载太慢或者下载失败，可以找找网上别人的分享镜像。** 比如[CUDA下载分享](https://blog.csdn.net/xhamigua/article/details/107028062)这篇文章(可能cuda版本不全)就有cuda百度网盘分享地址。如下：

> https://pan.baidu.com/s/14Qof5CVRUmeDMFgjBKGCjw 提取码：8488

下载完成后直接输入
> sudo sh cuda_10.2.89_440.33.01_linux.run

**在安装cuda的时候不要选择安装驱动，其他默认，就可以了。**

安装成功后再添加环境变量，先用vim命令打开~/.bashrc文件。再最后添加如下命令就行了：
```
# cuda环境变量
export CUDA_HOME=/usr/local/cuda 
export PATH=$PATH:$CUDA_HOME/bin 
export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```

当然可以看看是否安装成功，输入nvidia-smi看看显卡驱动是否安装成功，然后输入nvcc -V可以看到cuda版本。

![](image/5.jpg)

# 3 cudnn安装
直接打开[cudnn历史版本下载页面](https://developer.nvidia.com/rdp/cudnn-archive)选择对应的版本。一般来说cuda版本和cudnn版本并不是一一对应的。只要符合cuda版本要求的cudnn版本都行。

![](image/6.jpg)

本文用的是7.6.5版本，下载需要注册账号，很容易搞定。然后点击其中的cuDNN library for Linux即可。

![](image/7.jpg)

下载完成后，解压文件。进入解压后的cuda文件夹，输入以下命令，cudnn就算安装好了。
```
sudo cp cuda/include/cudnn.h /usr/local/cuda/include/ 
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64/ 
sudo chmod a+r /usr/local/cuda/include/cudnn.h 
sudo chmod a+r /usr/local/cuda/lib64/libcudnn*
```

通过以下命令查看cudnn版本：
> cat /usr/local/cuda/include/cudnn.h | grep CUDNN_MAJOR -A 2

根据输出可以判断cudnn版本为7.6.5.

![](image/8.jpg)




# 4 参考

+ [Ubuntu18.04安装CUDA10、CUDNN](https://blog.csdn.net/qq_32408773/article/details/84112166)

+ [Ubuntu18.04+CUDA10.2 深度学习开发环境配置指南](https://blog.csdn.net/abcdefg90876/article/details/105781005/)

+ [Ubuntu下安装INVIDIA显卡驱动（避免循环登录问题）](https://blog.csdn.net/qq_32408773/article/details/84111244)

+ [CUDA Toolkit and Compatible Driver Versions](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html)

+ [CUDA历史版本下载页面](https://developer.nvidia.com/cuda-toolkit-archive)

+ [CUDA下载分享](https://blog.csdn.net/xhamigua/article/details/107028062)

+ [cudnn历史版本下载页面](https://developer.nvidia.com/rdp/cudnn-archive)


