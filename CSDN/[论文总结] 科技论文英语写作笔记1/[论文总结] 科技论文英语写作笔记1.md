﻿
[toc]

# 1. 标点符号

## 1.1 逗号

+ 用在直接引语前面
+ 用在信件称呼后面
+ 用在介词后面
![](/1.png) 
+ 用在介绍短语后面
+ 当词或短语在句中做插入语时都需要用逗号
+ 在连词与它后面的主语之间不能加逗号，下图中so后面的逗号应该删掉。
![](/3.png) 
## 1.2 句号

+ 用在末尾或用于缩写。

## 1.3 分号
+ 分号可以用来连接两个并列分句，这个时候就不需要并列连词。原句的第二个并列分句中本身就存在and，如果再用and连接两个并列连词就会变得表达不明确。
![](/4.png) 
+ 通常我们会在连接两句话中间添加逗号，或者在连词前添加逗号。但是如果在因为第一个分句的本身就比较复杂，也不止一个逗号，如下图中“so"的前面再用逗号就会造成语义不清晰的表达。
![](/5.png) 
+ 分号还常用在连接副词之前。注意：however的前面可以是分号不能是逗号。
![](/6.png) 
## 1.4 冒号
用在列举条例之前也可以用在主标题和副标题之间。
+ 表示列举时，冒号前面必须是完整的句子。
![](/7.png) 
+ 不要在介词前使用冒号
![](/8.png) 
# 2. 大小写
+ 季节，某一天的首字母不需要大写
+  科目除了语言类其他均不需要大写
+  课程名称的首字母需要大写
+ Web和Internet的首字母需要大写
![](/9.png) 
## 2.1 标题的大小写规则
+ 标题的第一个单词首字母必须大写
+  副标题的第一个字母必须大写
+  标题最后一个单词的首字母要大写
+ 所有四个字母以上的单词首字母都要大写
+ 不足四个字母的介词不需要大写
+ 不足四个字母的连词不需要大写
## 2.2 引用
### 2.2.1 直接引用
如果直接引用其他作者的原句且超过了5行，那么可以将引用的话以排列形式展开，每行缩进相同距离。如图：
![](/10.png) 
### 2.2.2  间接引用
间接引用文献时不可直接引用原句或者简单的替换单词后加上参考文献，应该在原句基础上加入自己理解，修改语句结构后引用。
# 3.冠词误用
## 3.1 冠词缺失
 + "many of" "one of" "all of" "the rest of"等短语之后所接的复数名词必须有定冠词，否则必需有物主代词或指示代词。
**错误：** Many of principles of a receiver also apply to a transmitter.
**正确：** Many of **the** principles of a receiver also apply to a transmitter.
**错误：** Control of the systems is an interdisciplinary subject.
**正确：** **The** control of the systems is an interdisciplinary subject.
## 3.2 冠词过度使用
+ 图示说明、论文、书籍各章节标题一般可以省去冠词。
**错误：** Fig. 1 **A** keyboard with **an** attached printer and **a** display.
**正确：** Fig. 1 Keyboard with attached printer and display.
+  单数可数名词前一般需用冠词表示一类，或特指时加定冠词“the”, 表示一个时则不加定冠词。名词复数形式或不可数名词表示特指时，需加定冠词“the”,不特指时不加任何冠词。
**错误：**  **The** liner opperation is the ability of an amplifier to amplify **the** signals with litter or no distortion.
**正确：** Liner opperation is the ability of an amplifier to amplify signals with litter or no distortion.
**分析：** 句子中的"liner opperation" 不表示特指，没有必要加定冠"the"。
**错误：** This phenomenon will be discussed in **the** Chapter 3.
**正确：** This phenomenon will be discussed in Chapter 3.
**分析：**  在表示某一章节时不需要加冠词。
# 4. 连词误用
## 4.1 and
+  **错误：** In the formula, q is the amount of heart absorbed in joules, m is the mass of the substance in grams.
**正确：**  In the formula, q is the amount of heart absorbed in joules, **and** m is the mass of the substance in grams.
**分析：** 本句中并列成分为两个主句，期间应该使用and，而不能单独用逗号来连接。
+  **错误：**  Make sure all connectors are tight and no undue strain is applied to mechanical interconections.
**正确：**  Make sure all connectors are tight **and that** no undue strain is applied to mechanical interconections.
**分析：**  本局中“and”连接了“make sure”后的两个形容词宾语从句，根据英语语法，第一个宾语从句引导词“that”可以省略，但是第二以及之后的引导词不能省略。
+ **错误：** He went to Paris, then to London. 
**正确：**  He went to Paris, **and then** to London.
**分析：**  这里在英文中then的前后是两个句子，必须要加连词连接。
# 5. 数词误用
## 5.1 阿拉伯数字的表达方式
+  阿拉伯数字不能用在句子的开头
+  阿拉伯数字只用来给出数据信息而不用来表达一般信息。
## 5.2 分数的表达方式
**错误：** The voltage across the resistor is **zore point several volt**.
 **正确：** The voltage across the resistor is **a few tenths of a vilt.**
 **分析：** 平常1/3 用因为表示为one third而 2/5 用英文表示为two fifths。可以看到one ,two是分子，用基数词来表示，而third和fifths是分母，是用序数词表示。当分子不是1的时候，分母还需要变为复数形式。“零点几”和“零点零几”的表达公式为：A few(several) tenths (hundredths) of a(an)+单位。
 ## 5.3 小数的表达方式
**错误：** A cesium maintians its frequency constant to **one 100 billionth** or better.
 **正确：** A cesium maintians its frequency constant to **one part in one hundred billion** or better.
 **分析：** 类似一千亿分之一的这种极小数的表达就不再遵循普通的分数表达，而采用分子用基数词+parts;分母是per+基数词，或者"in a" +数词。 百万分之20 则用 20 part per million表示。
 ## 5.4 倍数的表达
**错误：** The wavelength of this musical note is 7.8 ft，is **two times longer than** the wavelength of same note in the air.(2.5ft)
**正确：** The wavelength of this musical note is 7.8 ft，is **three times longer than** the wavelength of same note in the air.(2.5ft)
**分析：** "N times" + 比较级 + than = N time as "原级" as。错误例句实际表达的意思是这个音符的波长为7.8英尺，比同一音符在空中的波长（2.5英尺）长两倍多。但错误例句表达的意思是比同一音符的波长长一倍多。

# 6 参考资料

文章主要内容来自于[中国大学MOOC西安电子科技大学的科技英语写作课程](https://www.icourse163.org/learn/XDU-1460555164?tid=1467052484#/learn/content)，感兴趣的小伙伴可点击链接前去观看。