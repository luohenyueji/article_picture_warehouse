﻿/**
 * @brief 代码主要参考https://github.com/leandromoreira/ffmpeg-libav-tutorial/blob/master/0_hello_world.c
 *
 */

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavfilter/avfilter.h"
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"
#include "libavutil/ffversion.h"
#include "libavutil/opt.h"
#include "libavutil/imgutils.h"
#include "libavutil/time.h"
#include "libswresample/swresample.h"
#include "libswscale/swscale.h"
#include "libavdevice/avdevice.h"
}
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>

// 日志打印宏
#define LOG(msg, ...)\
fprintf(stderr,"LOG [line %d] ",__LINE__);\
fprintf(stderr,msg, ##__VA_ARGS__);\
fprintf(stderr, "\n");

// 支持的输入文件形式
enum URLType { file, usbcam, desktop, yuvfile };

// usb摄像头读取函数
void usbcam_get(const char * url, AVInputFormat ** ifmt);

// 解码函数
static int decode_packet(AVPacket *pPacket, AVCodecContext *pCodecContext, AVFrame *pFrame, int skip = 0);

/**
 * 涉及到结构体
 * AVFormatContext	存储媒体文件所有信息的结构体
 * AVInputFormat 存储媒体文件的格式信息
 * AVStream	表示音视频流信息的结构体
 * AVCodecContext	存储解码音视频所有信息的结构体
 * AVCodec	存储视频或音频的编解码器的结构体
 * AVCodecParameters	存储音视频编解码器的相关参数信息的结构体
 * AVPacket	储存解码前数据的结构体
 * AVFrame	存储解码后数据的结构体
 * AVRational	表示有理数的结构体
 * SwsContext	用于图像转换的结构体
 */
int main()
{
	// 设置数据类型
	URLType urltype = usbcam;
	// 初始化结构体
	const char *url = NULL;
	AVFormatContext *pFormatContext = NULL;
	AVInputFormat *ifmt = NULL;
	AVDictionary *options = NULL;
	// AVCodec负责编解码音视频流
	AVCodecContext *pCodecContext = NULL;
	AVCodec *pCodec = NULL;
	AVCodecParameters *pCodecParameters = NULL;
	// 负责保存数据
	AVPacket *pPacket = NULL;
	AVFrame *pFrame = NULL;

	LOG("FFMPEG VERSION: %s", av_version_info());
	LOG("开始运行");

	// 存储音视频封装格式中包含的信息
	// avformat_alloc_context初始化AVFormatContext结构体
	pFormatContext = avformat_alloc_context();

	if (!pFormatContext)
	{
		LOG("pFormatContext分配内存失败");
		return -1;
	}

	// 注册能操作的输入输出设备
	avdevice_register_all();
	if (urltype == file)
	{
		// rtsp流
		//url = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4";
		// 输入图片
		// url = "demo.png";
		// 输入视频
		url = "demo.mp4";
		// 设置超时时间为5秒
		av_dict_set(&options, "stimeout", "5000000", 0);
	}
	else if (urltype == usbcam)
	{
		url = "0";
		// 如果使用以下方式读取本机摄像头，需要自行获得摄像头名称
		// 使用指令：ffmpeg -list_devices true -f dshow -i dummy
		//url = "video=HD WebCam";
		// 输出ffmpeg版本
		usbcam_get(url, &ifmt);
		// 设置图片尺寸
		av_dict_set(&options, "video_size", "640x480", 0);
		av_dict_set(&options, "framerate", "30", 0);
	}
	else if (urltype == desktop)
	{
		// Windows
#ifdef _WIN32
	// 根据不同的url选择不同的格式
		url = "desktop";
		ifmt = av_find_input_format("gdigrab");
		// linux处理
#elif defined linux
		// linux命令行输入echo $DISPALY获得
		url = ":1";
		ifmt = av_find_input_format("x11grab");
#endif
		av_dict_set(&options, "video_size", "1920x1080", 0);
		av_dict_set(&options, "framerate", "15", 0);
	}
	else if (urltype == yuvfile)
	{
		url = "akiyo_cif.yuv";
		// yuv图像尺寸需要提前设置
		av_dict_set(&options, "video_size", "352x288", 0);
	}

	// avformat_open_input打开输入的媒体文件
	if (avformat_open_input(&pFormatContext, url, ifmt, &options) != 0)
	{
		LOG("打开文件失败");
		return -1;
	}

	LOG("打开文件 %s", url);

	// 读取文件音视频编解码器的信息
	LOG("文件格式 %s, 文件时长 %lld us, 比特率 %lld bit/s",
		pFormatContext->iformat->name,
		pFormatContext->duration,
		pFormatContext->bit_rate);

	LOG("获取输入音视频文件的流信息");
	// avformat_find_stream_info获取输入音视频文件的流信息
	if (avformat_find_stream_info(pFormatContext, NULL) < 0)
	{
		LOG("无法获取流信息");
		return -1;
	}

	// 设置是否读取到视频流
	int video_stream_index = -1;

	// 循环浏览所有流并打印其主要信息
	for (int i = 0; i < int(pFormatContext->nb_streams); i++)
	{
		AVCodecParameters *pLocalCodecParameters = NULL;
		// 提取当前流的编解码器参数
		pLocalCodecParameters = pFormatContext->streams[i]->codecpar;

		AVCodec *pLocalCodec = NULL;

		// 查找指定编解码器的解码器
		pLocalCodec = avcodec_find_decoder(pLocalCodecParameters->codec_id);

		if (pLocalCodec == NULL)
		{
			LOG("不支持该解码器！");
			continue;
		}

		// 当流是视频时，我们存储其索引、解码器和编解码器参数
		if (pLocalCodecParameters->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			if (video_stream_index == -1)
			{
				video_stream_index = i;
				pCodec = pLocalCodec;
				pCodecParameters = pLocalCodecParameters;
			}

			LOG("视频编解码器类型： %s ID： %d", pLocalCodec->name, pLocalCodec->id);
			LOG("视频流帧率为：%f", av_q2d(pFormatContext->streams[i]->r_frame_rate));
			LOG("视频流共有：%d帧", pFormatContext->streams[i]->nb_frames);
			LOG("视频图像分辨率为：(%d,%d)", pLocalCodecParameters->width, pLocalCodecParameters->height);
		}
		else if (pLocalCodecParameters->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			LOG("音频编解码器类型： %s ID： %d", pLocalCodec->name, pLocalCodec->id);
			LOG("音频通道数：%d channels, 采样率：%d", pLocalCodecParameters->channels, pLocalCodecParameters->sample_rate);
		}
	}

	if (video_stream_index == -1)
	{
		LOG("%s文件不包含视频流!", url);
		return -1;
	}

	// 分配AVCodecContext结构体并进行初始化
	pCodecContext = avcodec_alloc_context3(pCodec);
	if (!pCodecContext)
	{
		LOG("AVCodecContext初始失败");
		return -1;
	}

	// 将AVCodecParameters中的参数设置到AVCodecContext中
	if (avcodec_parameters_to_context(pCodecContext, pCodecParameters) < 0)
	{
		LOG("AVCodecParameters参数拷贝失败");
		return -1;
	}

	// 打开解码器
	if (avcodec_open2(pCodecContext, pCodec, NULL) < 0)
	{
		LOG("打开解码器失败");
		return -1;
	}

	// 创建AVPacket
	pPacket = av_packet_alloc();
	if (!pPacket)
	{
		LOG("AVPacket初始化失败");
		return -1;
	}

	// 创建AVFrame
	pFrame = av_frame_alloc();
	if (!pFrame)
	{
		LOG("AVFrame初始化失败");
		return -1;
	}

	int response = 0;
	// 最多读取帧数
	int how_many_packets_to_process = 500;
	// 帧处理跨度
	int skip_span = 50;

	// 读取媒体文件中的音视频帧
	while (av_read_frame(pFormatContext, pPacket) >= 0)
	{
		// 判断是否为视频帧
		if (pPacket->stream_index == video_stream_index)
		{
			// 只解码关键帧，关键帧不依赖于其他帧进行解码，所以可以跳过其他帧

			// 关键帧间隔由媒体流数据源决定
			// if (!(pPacket->flags & AV_PKT_FLAG_KEY)) {
			//	continue;
			//}
			int skip = 1;
			// 如果已读取帧数除以skip_span为0，则下一帧进行处理
			if (pCodecContext->frame_number % skip_span == 0)
			{
				skip = 0;
			}

			// 计算时间
			auto start = std::chrono::system_clock::now();
			// 图像解码函数
			response = decode_packet(pPacket, pCodecContext, pFrame, skip);
			auto end = std::chrono::system_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
			if (skip == 0)
			{
				LOG("解码和处理一帧图像耗时：%d ms", duration);
			}
			else
			{
				LOG("仅解码一帧图像耗时：%d ms", duration);
			}
			// 图像解码状态判定
			if (response < 0)
				break;
			// 超过读取图像上限
			if (--how_many_packets_to_process <= 0)
			{
				LOG("读图完毕！");
				break;
			}
		}
		// 释放AVPacket结构体中的内存
		av_packet_unref(pPacket);
	}

	LOG("销毁所有结构体");

	// 销毁结构体
	avformat_close_input(&pFormatContext);
	av_packet_free(&pPacket);
	av_frame_free(&pFrame);
	avcodec_free_context(&pCodecContext);
	av_dict_free(&options);
	system("pause");
	return 0;
}

void usbcam_get(const char * url, AVInputFormat ** ifmt)
{
	// Windows
#ifdef _WIN32
	// 根据不同的url选择不同的格式
	if (url == "0")
		*ifmt = av_find_input_format("vfwcap");
	else
		*ifmt = av_find_input_format("dshow");
	// linux
#elif defined linux
	url = "/dev/video0";
	*ifmt = av_find_input_format("video4linux2");
#endif
}

static int decode_packet(AVPacket *pPacket, AVCodecContext *pCodecContext, AVFrame *pFrame, int skip)
{
	// 将pPacket数据送入pCodecContext进行解码
	int response = avcodec_send_packet(pCodecContext, pPacket);

	if (response < 0)
	{
		return response;
	}

	while (response >= 0)
	{
		// 用于从解码器中获取解码后的视频帧
		response = avcodec_receive_frame(pCodecContext, pFrame);
		if (response == AVERROR(EAGAIN) || response == AVERROR_EOF)
		{
			break;
		}
		else if (response < 0)
		{
			LOG("读图出错: %d", response);
			return response;
		}

		// 仅读取当前帧
		if (skip != 0)
		{
			return 0;
		}
		if (response >= 0)
		{
			LOG("Frame %d，帧类型=%c，视频格式=%d，pts=%d，是否为关键帧=%d",
				pCodecContext->frame_number,
				av_get_picture_type_char(pFrame->pict_type),
				pFrame->format,
				pFrame->pts,
				pFrame->key_frame);

			// 图像保存名
			char frame_filename[1024];
			snprintf(frame_filename, sizeof(frame_filename), "%s-%d.jpg", "frame", pCodecContext->frame_number);

			// 将解码后的帧转换为BGR格式
			// 创建图像转换器，设置图像尺寸缩小一倍
			int dst_w = int(pCodecContext->width / 2);
			int dst_h = int(pCodecContext->height / 2);
			SwsContext *swsCtx = sws_getContext(
				pCodecContext->width, pCodecContext->height, (AVPixelFormat)pCodecContext->pix_fmt,
				dst_w, dst_h, AV_PIX_FMT_BGR24,
				SWS_POINT, NULL, NULL, NULL);
			cv::Mat bgrMat(dst_h, dst_w, CV_8UC3);
			// 拿出opencv的数据
			uint8_t *dest[1] = { bgrMat.data };
			int destStride[1] = { int(bgrMat.step) };
			// 执行格式转换
			sws_scale(swsCtx, pFrame->data, pFrame->linesize, 0, pFrame->height, dest, destStride);
			// 保存图片
			cv::imwrite(frame_filename, bgrMat);
			// 释放swsCtx数据
			sws_freeContext(swsCtx);
		}
	}
	return 0;
}