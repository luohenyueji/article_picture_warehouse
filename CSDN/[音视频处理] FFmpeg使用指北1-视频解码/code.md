```mermaid
timeline
    解封装: 判断输入源
         : 打开文件
         : 查找流信息
         : 查找视频索引
    解码 : 初始化解码器
         : 打开解码器
   取数据 : 初始化数据结构
         : 读取视频帧
         : 发送视频帧给解码器
         : 从解码器获取解码结果
    数据处理: 初始化数据转换结构
         : 初始化opencv结构
         : 执行格式转换
    释放资源: 释放结构体
```