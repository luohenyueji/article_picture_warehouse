PyCaret是一个开源、低代码Python机器学习库，能够自动化机器学习工作流程。它是一个端到端的机器学习和模型管理工具，极大地加快了实验周期，提高了工作效率。PyCaret本质上是围绕几个机器学习库和框架（如scikit-learn、XGBoost、LightGBM、CatBoost、spaCy、Optuna、Hyperopt、Ray等）的Python包装器，与其他开源机器学习库相比，PyCaret可以用少量代码取代数百行代码。PyCaret开源仓库地址：[pycaret](https://github.com/pycaret/pycaret)，官方文档地址为：[pycaret-docs](https://pycaret.gitbook.io/docs)。

PyCaret基础版安装命令如下：

> pip install pycaret

完整版安装代码如下：

> pip install pycaret[full]


此外以下模型可以调用GPU

+ Extreme Gradient Boosting 
+ Catboost
+ Light Gradient Boosting（需要安装[lightgbm](https://lightgbm.readthedocs.io/en/latest/GPU-Tutorial.html))
+ Logistic Regression, Ridge Classifier, Random Forest, K Neighbors Classifier, K Neighbors Regressor, Support Vector Machine, Linear Regression, Ridge Regression, Lasso Regression（需要安装[cuml](https://github.com/rapidsai/cuml)0.15版本以上）


```python
# 查看pycaret版本
import pycaret
pycaret.__version__
```




    '3.3.2'



# 1 快速入门

PyCaret支持多种机器学习任务，包括分类、回归、聚类、异常检测和时序预测。本节主要介绍如何利用PyCaret构建相关任务模型的基础使用方法。关于更详细的PyCaret任务模型使用，请参考：

|Topic|NotebookLink|
|:---|---:|
|二分类BinaryClassification|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Binary%20Classification.ipynb)|
|多分类MulticlassClassification|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Multiclass%20Classification.ipynb)|
|回归Regression|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Regression.ipynb)|
|聚类Clustering|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Clustering.ipynb)|
|异常检测AnomalyDetection|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Anomaly%20Detection.ipynb)|
|时序预测TimeSeriesForecasting|[link](https://github.com/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Time%20Series%20Forecasting.ipynb)|

## 1.1 分类

PyCaret的classification模块是一个可用于二分类或多分类的模块，用于将元素分类到不同的组中。一些常见的用例包括预测客户是否违约、预测客户是否流失、以及诊断疾病（阳性或阴性）。示例代码如下所示：

**数据准备**

加载糖尿病示例数据集：


```python
from pycaret.datasets import get_data
# 从本地加载数据，注意dataset是数据的文件名
data = get_data(dataset='./datasets/diabetes', verbose=False)
# 从pycaret开源仓库下载公开数据
# data = get_data('diabetes', verbose=False)
```


```python
# 查看数据类型和数据维度
type(data), data.shape
```




    (pandas.core.frame.DataFrame, (768, 9))




```python
# 最后一列表示是否为糖尿病患者，其他列为特征列
data.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number of times pregnant</th>
      <th>Plasma glucose concentration a 2 hours in an oral glucose tolerance test</th>
      <th>Diastolic blood pressure (mm Hg)</th>
      <th>Triceps skin fold thickness (mm)</th>
      <th>2-Hour serum insulin (mu U/ml)</th>
      <th>Body mass index (weight in kg/(height in m)^2)</th>
      <th>Diabetes pedigree function</th>
      <th>Age (years)</th>
      <th>Class variable</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>6</td>
      <td>148</td>
      <td>72</td>
      <td>35</td>
      <td>0</td>
      <td>33.6</td>
      <td>0.627</td>
      <td>50</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>85</td>
      <td>66</td>
      <td>29</td>
      <td>0</td>
      <td>26.6</td>
      <td>0.351</td>
      <td>31</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>8</td>
      <td>183</td>
      <td>64</td>
      <td>0</td>
      <td>0</td>
      <td>23.3</td>
      <td>0.672</td>
      <td>32</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>89</td>
      <td>66</td>
      <td>23</td>
      <td>94</td>
      <td>28.1</td>
      <td>0.167</td>
      <td>21</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>137</td>
      <td>40</td>
      <td>35</td>
      <td>168</td>
      <td>43.1</td>
      <td>2.288</td>
      <td>33</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



利用PyCaret核心函数setup，初始化建模环境并准备数据以供模型训练和评估使用：


```python
from pycaret.classification import ClassificationExperiment
s = ClassificationExperiment()
# target目标列，session_id设置随机数种子, preprocesss是否清洗数据，train_size训练集比例, normalize是否归一化数据, normalize_method归一化方式
s.setup(data, target = 'Class variable', session_id = 0, verbose= False, train_size = 0.7, normalize = True, normalize_method = 'minmax')
```




    <pycaret.classification.oop.ClassificationExperiment at 0x200b939df40>



查看基于setup函数创建的变量：


```python
s.get_config()
```




    {'USI',
     'X',
     'X_test',
     'X_test_transformed',
     'X_train',
     'X_train_transformed',
     'X_transformed',
     '_available_plots',
     '_ml_usecase',
     'data',
     'dataset',
     'dataset_transformed',
     'exp_id',
     'exp_name_log',
     'fix_imbalance',
     'fold_generator',
     'fold_groups_param',
     'fold_shuffle_param',
     'gpu_n_jobs_param',
     'gpu_param',
     'html_param',
     'idx',
     'is_multiclass',
     'log_plots_param',
     'logging_param',
     'memory',
     'n_jobs_param',
     'pipeline',
     'seed',
     'target_param',
     'test',
     'test_transformed',
     'train',
     'train_transformed',
     'variable_and_property_keys',
     'variables',
     'y',
     'y_test',
     'y_test_transformed',
     'y_train',
     'y_train_transformed',
     'y_transformed'}



查看归一化的数据：


```python
s.get_config('X_train_transformed')
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number of times pregnant</th>
      <th>Plasma glucose concentration a 2 hours in an oral glucose tolerance test</th>
      <th>Diastolic blood pressure (mm Hg)</th>
      <th>Triceps skin fold thickness (mm)</th>
      <th>2-Hour serum insulin (mu U/ml)</th>
      <th>Body mass index (weight in kg/(height in m)^2)</th>
      <th>Diabetes pedigree function</th>
      <th>Age (years)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>34</th>
      <td>0.588235</td>
      <td>0.613065</td>
      <td>0.639344</td>
      <td>0.313131</td>
      <td>0.000000</td>
      <td>0.411326</td>
      <td>0.185312</td>
      <td>0.400000</td>
    </tr>
    <tr>
      <th>221</th>
      <td>0.117647</td>
      <td>0.793970</td>
      <td>0.737705</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.470939</td>
      <td>0.310418</td>
      <td>0.750000</td>
    </tr>
    <tr>
      <th>531</th>
      <td>0.000000</td>
      <td>0.537688</td>
      <td>0.622951</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.675112</td>
      <td>0.259607</td>
      <td>0.050000</td>
    </tr>
    <tr>
      <th>518</th>
      <td>0.764706</td>
      <td>0.381910</td>
      <td>0.491803</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.488823</td>
      <td>0.043553</td>
      <td>0.333333</td>
    </tr>
    <tr>
      <th>650</th>
      <td>0.058824</td>
      <td>0.457286</td>
      <td>0.442623</td>
      <td>0.252525</td>
      <td>0.118203</td>
      <td>0.375559</td>
      <td>0.066610</td>
      <td>0.033333</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>628</th>
      <td>0.294118</td>
      <td>0.643216</td>
      <td>0.655738</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.515648</td>
      <td>0.028181</td>
      <td>0.400000</td>
    </tr>
    <tr>
      <th>456</th>
      <td>0.058824</td>
      <td>0.678392</td>
      <td>0.442623</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.397914</td>
      <td>0.260034</td>
      <td>0.683333</td>
    </tr>
    <tr>
      <th>398</th>
      <td>0.176471</td>
      <td>0.412060</td>
      <td>0.573770</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.314456</td>
      <td>0.132792</td>
      <td>0.066667</td>
    </tr>
    <tr>
      <th>6</th>
      <td>0.176471</td>
      <td>0.391960</td>
      <td>0.409836</td>
      <td>0.323232</td>
      <td>0.104019</td>
      <td>0.461997</td>
      <td>0.072588</td>
      <td>0.083333</td>
    </tr>
    <tr>
      <th>294</th>
      <td>0.000000</td>
      <td>0.809045</td>
      <td>0.409836</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.326379</td>
      <td>0.075149</td>
      <td>0.733333</td>
    </tr>
  </tbody>
</table>
<p>537 rows × 8 columns</p>
</div>



绘制某列数据的柱状图：


```python
s.get_config('X_train_transformed')['Number of times pregnant'].hist()
```




    <AxesSubplot:>




    
![png](output_16_1.png)
    


当然也可以利用如下代码创建任务示例来初始化环境：


```python
from pycaret.classification import setup
# s = setup(data, target = 'Class variable', session_id = 0, preprocess = True, train_size = 0.7, verbose = False)
```

**模型训练与评估**

PyCaret提供了compare_models函数，通过使用默认的10折交叉验证来训练和评估模型库中所有可用估计器的性能：


```python
best = s.compare_models()
# 选择某些模型进行比较
# best = s.compare_models(include = ['dt', 'rf', 'et', 'gbc', 'lightgbm'])
# 按照召回率返回n_select性能最佳的模型
# best_recall_models_top3 = s.compare_models(sort = 'Recall', n_select = 3)
```






<style type="text/css">
#T_68083 th {
  text-align: left;
}
#T_68083_row0_col0, #T_68083_row0_col3, #T_68083_row0_col5, #T_68083_row0_col6, #T_68083_row0_col7, #T_68083_row1_col0, #T_68083_row1_col2, #T_68083_row1_col3, #T_68083_row1_col4, #T_68083_row1_col5, #T_68083_row1_col6, #T_68083_row1_col7, #T_68083_row2_col0, #T_68083_row2_col2, #T_68083_row2_col3, #T_68083_row2_col4, #T_68083_row3_col0, #T_68083_row3_col1, #T_68083_row3_col2, #T_68083_row3_col3, #T_68083_row3_col4, #T_68083_row3_col5, #T_68083_row3_col6, #T_68083_row3_col7, #T_68083_row4_col0, #T_68083_row4_col1, #T_68083_row4_col2, #T_68083_row4_col4, #T_68083_row4_col5, #T_68083_row4_col6, #T_68083_row4_col7, #T_68083_row5_col0, #T_68083_row5_col1, #T_68083_row5_col2, #T_68083_row5_col3, #T_68083_row5_col4, #T_68083_row5_col5, #T_68083_row5_col6, #T_68083_row5_col7, #T_68083_row6_col0, #T_68083_row6_col1, #T_68083_row6_col2, #T_68083_row6_col3, #T_68083_row6_col4, #T_68083_row6_col5, #T_68083_row6_col6, #T_68083_row6_col7, #T_68083_row7_col0, #T_68083_row7_col1, #T_68083_row7_col2, #T_68083_row7_col3, #T_68083_row7_col4, #T_68083_row7_col5, #T_68083_row7_col6, #T_68083_row7_col7, #T_68083_row8_col0, #T_68083_row8_col1, #T_68083_row8_col2, #T_68083_row8_col3, #T_68083_row8_col4, #T_68083_row8_col5, #T_68083_row8_col6, #T_68083_row8_col7, #T_68083_row9_col0, #T_68083_row9_col1, #T_68083_row9_col2, #T_68083_row9_col3, #T_68083_row9_col4, #T_68083_row9_col5, #T_68083_row9_col6, #T_68083_row9_col7, #T_68083_row10_col0, #T_68083_row10_col1, #T_68083_row10_col2, #T_68083_row10_col3, #T_68083_row10_col4, #T_68083_row10_col5, #T_68083_row10_col6, #T_68083_row10_col7, #T_68083_row11_col0, #T_68083_row11_col1, #T_68083_row11_col2, #T_68083_row11_col3, #T_68083_row11_col4, #T_68083_row11_col5, #T_68083_row11_col6, #T_68083_row11_col7, #T_68083_row12_col0, #T_68083_row12_col1, #T_68083_row12_col2, #T_68083_row12_col3, #T_68083_row12_col4, #T_68083_row12_col5, #T_68083_row12_col6, #T_68083_row12_col7, #T_68083_row13_col0, #T_68083_row13_col1, #T_68083_row13_col2, #T_68083_row13_col3, #T_68083_row13_col4, #T_68083_row13_col5, #T_68083_row13_col6, #T_68083_row13_col7 {
  text-align: left;
}
#T_68083_row0_col1, #T_68083_row0_col2, #T_68083_row0_col4, #T_68083_row1_col1, #T_68083_row2_col1, #T_68083_row2_col5, #T_68083_row2_col6, #T_68083_row2_col7, #T_68083_row4_col3 {
  text-align: left;
  background-color: yellow;
}
#T_68083_row0_col8, #T_68083_row1_col8, #T_68083_row3_col8, #T_68083_row6_col8, #T_68083_row7_col8, #T_68083_row8_col8, #T_68083_row9_col8, #T_68083_row11_col8, #T_68083_row12_col8, #T_68083_row13_col8 {
  text-align: left;
  background-color: lightgrey;
}
#T_68083_row2_col8, #T_68083_row4_col8, #T_68083_row5_col8, #T_68083_row10_col8 {
  text-align: left;
  background-color: yellow;
  background-color: lightgrey;
}
</style>
<table id="T_68083">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_68083_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_68083_level0_col1" class="col_heading level0 col1" >Accuracy</th>
      <th id="T_68083_level0_col2" class="col_heading level0 col2" >AUC</th>
      <th id="T_68083_level0_col3" class="col_heading level0 col3" >Recall</th>
      <th id="T_68083_level0_col4" class="col_heading level0 col4" >Prec.</th>
      <th id="T_68083_level0_col5" class="col_heading level0 col5" >F1</th>
      <th id="T_68083_level0_col6" class="col_heading level0 col6" >Kappa</th>
      <th id="T_68083_level0_col7" class="col_heading level0 col7" >MCC</th>
      <th id="T_68083_level0_col8" class="col_heading level0 col8" >TT (Sec)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_68083_level0_row0" class="row_heading level0 row0" >lr</th>
      <td id="T_68083_row0_col0" class="data row0 col0" >Logistic Regression</td>
      <td id="T_68083_row0_col1" class="data row0 col1" >0.7633</td>
      <td id="T_68083_row0_col2" class="data row0 col2" >0.8132</td>
      <td id="T_68083_row0_col3" class="data row0 col3" >0.4968</td>
      <td id="T_68083_row0_col4" class="data row0 col4" >0.7436</td>
      <td id="T_68083_row0_col5" class="data row0 col5" >0.5939</td>
      <td id="T_68083_row0_col6" class="data row0 col6" >0.4358</td>
      <td id="T_68083_row0_col7" class="data row0 col7" >0.4549</td>
      <td id="T_68083_row0_col8" class="data row0 col8" >0.2720</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row1" class="row_heading level0 row1" >ridge</th>
      <td id="T_68083_row1_col0" class="data row1 col0" >Ridge Classifier</td>
      <td id="T_68083_row1_col1" class="data row1 col1" >0.7633</td>
      <td id="T_68083_row1_col2" class="data row1 col2" >0.8113</td>
      <td id="T_68083_row1_col3" class="data row1 col3" >0.5178</td>
      <td id="T_68083_row1_col4" class="data row1 col4" >0.7285</td>
      <td id="T_68083_row1_col5" class="data row1 col5" >0.6017</td>
      <td id="T_68083_row1_col6" class="data row1 col6" >0.4406</td>
      <td id="T_68083_row1_col7" class="data row1 col7" >0.4560</td>
      <td id="T_68083_row1_col8" class="data row1 col8" >0.0090</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row2" class="row_heading level0 row2" >lda</th>
      <td id="T_68083_row2_col0" class="data row2 col0" >Linear Discriminant Analysis</td>
      <td id="T_68083_row2_col1" class="data row2 col1" >0.7633</td>
      <td id="T_68083_row2_col2" class="data row2 col2" >0.8110</td>
      <td id="T_68083_row2_col3" class="data row2 col3" >0.5497</td>
      <td id="T_68083_row2_col4" class="data row2 col4" >0.7069</td>
      <td id="T_68083_row2_col5" class="data row2 col5" >0.6154</td>
      <td id="T_68083_row2_col6" class="data row2 col6" >0.4489</td>
      <td id="T_68083_row2_col7" class="data row2 col7" >0.4583</td>
      <td id="T_68083_row2_col8" class="data row2 col8" >0.0080</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row3" class="row_heading level0 row3" >ada</th>
      <td id="T_68083_row3_col0" class="data row3 col0" >Ada Boost Classifier</td>
      <td id="T_68083_row3_col1" class="data row3 col1" >0.7465</td>
      <td id="T_68083_row3_col2" class="data row3 col2" >0.7768</td>
      <td id="T_68083_row3_col3" class="data row3 col3" >0.5655</td>
      <td id="T_68083_row3_col4" class="data row3 col4" >0.6580</td>
      <td id="T_68083_row3_col5" class="data row3 col5" >0.6051</td>
      <td id="T_68083_row3_col6" class="data row3 col6" >0.4208</td>
      <td id="T_68083_row3_col7" class="data row3 col7" >0.4255</td>
      <td id="T_68083_row3_col8" class="data row3 col8" >0.0190</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row4" class="row_heading level0 row4" >svm</th>
      <td id="T_68083_row4_col0" class="data row4 col0" >SVM - Linear Kernel</td>
      <td id="T_68083_row4_col1" class="data row4 col1" >0.7408</td>
      <td id="T_68083_row4_col2" class="data row4 col2" >0.8087</td>
      <td id="T_68083_row4_col3" class="data row4 col3" >0.5921</td>
      <td id="T_68083_row4_col4" class="data row4 col4" >0.6980</td>
      <td id="T_68083_row4_col5" class="data row4 col5" >0.6020</td>
      <td id="T_68083_row4_col6" class="data row4 col6" >0.4196</td>
      <td id="T_68083_row4_col7" class="data row4 col7" >0.4480</td>
      <td id="T_68083_row4_col8" class="data row4 col8" >0.0080</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row5" class="row_heading level0 row5" >nb</th>
      <td id="T_68083_row5_col0" class="data row5 col0" >Naive Bayes</td>
      <td id="T_68083_row5_col1" class="data row5 col1" >0.7391</td>
      <td id="T_68083_row5_col2" class="data row5 col2" >0.7939</td>
      <td id="T_68083_row5_col3" class="data row5 col3" >0.5442</td>
      <td id="T_68083_row5_col4" class="data row5 col4" >0.6515</td>
      <td id="T_68083_row5_col5" class="data row5 col5" >0.5857</td>
      <td id="T_68083_row5_col6" class="data row5 col6" >0.3995</td>
      <td id="T_68083_row5_col7" class="data row5 col7" >0.4081</td>
      <td id="T_68083_row5_col8" class="data row5 col8" >0.0080</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row6" class="row_heading level0 row6" >rf</th>
      <td id="T_68083_row6_col0" class="data row6 col0" >Random Forest Classifier</td>
      <td id="T_68083_row6_col1" class="data row6 col1" >0.7337</td>
      <td id="T_68083_row6_col2" class="data row6 col2" >0.8033</td>
      <td id="T_68083_row6_col3" class="data row6 col3" >0.5406</td>
      <td id="T_68083_row6_col4" class="data row6 col4" >0.6331</td>
      <td id="T_68083_row6_col5" class="data row6 col5" >0.5778</td>
      <td id="T_68083_row6_col6" class="data row6 col6" >0.3883</td>
      <td id="T_68083_row6_col7" class="data row6 col7" >0.3929</td>
      <td id="T_68083_row6_col8" class="data row6 col8" >0.0350</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row7" class="row_heading level0 row7" >et</th>
      <td id="T_68083_row7_col0" class="data row7 col0" >Extra Trees Classifier</td>
      <td id="T_68083_row7_col1" class="data row7 col1" >0.7298</td>
      <td id="T_68083_row7_col2" class="data row7 col2" >0.7899</td>
      <td id="T_68083_row7_col3" class="data row7 col3" >0.5181</td>
      <td id="T_68083_row7_col4" class="data row7 col4" >0.6416</td>
      <td id="T_68083_row7_col5" class="data row7 col5" >0.5677</td>
      <td id="T_68083_row7_col6" class="data row7 col6" >0.3761</td>
      <td id="T_68083_row7_col7" class="data row7 col7" >0.3840</td>
      <td id="T_68083_row7_col8" class="data row7 col8" >0.0450</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row8" class="row_heading level0 row8" >gbc</th>
      <td id="T_68083_row8_col0" class="data row8 col0" >Gradient Boosting Classifier</td>
      <td id="T_68083_row8_col1" class="data row8 col1" >0.7281</td>
      <td id="T_68083_row8_col2" class="data row8 col2" >0.8007</td>
      <td id="T_68083_row8_col3" class="data row8 col3" >0.5567</td>
      <td id="T_68083_row8_col4" class="data row8 col4" >0.6267</td>
      <td id="T_68083_row8_col5" class="data row8 col5" >0.5858</td>
      <td id="T_68083_row8_col6" class="data row8 col6" >0.3857</td>
      <td id="T_68083_row8_col7" class="data row8 col7" >0.3896</td>
      <td id="T_68083_row8_col8" class="data row8 col8" >0.0260</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row9" class="row_heading level0 row9" >lightgbm</th>
      <td id="T_68083_row9_col0" class="data row9 col0" >Light Gradient Boosting Machine</td>
      <td id="T_68083_row9_col1" class="data row9 col1" >0.7242</td>
      <td id="T_68083_row9_col2" class="data row9 col2" >0.7811</td>
      <td id="T_68083_row9_col3" class="data row9 col3" >0.5827</td>
      <td id="T_68083_row9_col4" class="data row9 col4" >0.6096</td>
      <td id="T_68083_row9_col5" class="data row9 col5" >0.5935</td>
      <td id="T_68083_row9_col6" class="data row9 col6" >0.3859</td>
      <td id="T_68083_row9_col7" class="data row9 col7" >0.3876</td>
      <td id="T_68083_row9_col8" class="data row9 col8" >0.0860</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row10" class="row_heading level0 row10" >qda</th>
      <td id="T_68083_row10_col0" class="data row10 col0" >Quadratic Discriminant Analysis</td>
      <td id="T_68083_row10_col1" class="data row10 col1" >0.7150</td>
      <td id="T_68083_row10_col2" class="data row10 col2" >0.7875</td>
      <td id="T_68083_row10_col3" class="data row10 col3" >0.4962</td>
      <td id="T_68083_row10_col4" class="data row10 col4" >0.6225</td>
      <td id="T_68083_row10_col5" class="data row10 col5" >0.5447</td>
      <td id="T_68083_row10_col6" class="data row10 col6" >0.3428</td>
      <td id="T_68083_row10_col7" class="data row10 col7" >0.3524</td>
      <td id="T_68083_row10_col8" class="data row10 col8" >0.0080</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row11" class="row_heading level0 row11" >knn</th>
      <td id="T_68083_row11_col0" class="data row11 col0" >K Neighbors Classifier</td>
      <td id="T_68083_row11_col1" class="data row11 col1" >0.7131</td>
      <td id="T_68083_row11_col2" class="data row11 col2" >0.7425</td>
      <td id="T_68083_row11_col3" class="data row11 col3" >0.5287</td>
      <td id="T_68083_row11_col4" class="data row11 col4" >0.6005</td>
      <td id="T_68083_row11_col5" class="data row11 col5" >0.5577</td>
      <td id="T_68083_row11_col6" class="data row11 col6" >0.3480</td>
      <td id="T_68083_row11_col7" class="data row11 col7" >0.3528</td>
      <td id="T_68083_row11_col8" class="data row11 col8" >0.2200</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row12" class="row_heading level0 row12" >dt</th>
      <td id="T_68083_row12_col0" class="data row12 col0" >Decision Tree Classifier</td>
      <td id="T_68083_row12_col1" class="data row12 col1" >0.6685</td>
      <td id="T_68083_row12_col2" class="data row12 col2" >0.6461</td>
      <td id="T_68083_row12_col3" class="data row12 col3" >0.5722</td>
      <td id="T_68083_row12_col4" class="data row12 col4" >0.5266</td>
      <td id="T_68083_row12_col5" class="data row12 col5" >0.5459</td>
      <td id="T_68083_row12_col6" class="data row12 col6" >0.2868</td>
      <td id="T_68083_row12_col7" class="data row12 col7" >0.2889</td>
      <td id="T_68083_row12_col8" class="data row12 col8" >0.0100</td>
    </tr>
    <tr>
      <th id="T_68083_level0_row13" class="row_heading level0 row13" >dummy</th>
      <td id="T_68083_row13_col0" class="data row13 col0" >Dummy Classifier</td>
      <td id="T_68083_row13_col1" class="data row13 col1" >0.6518</td>
      <td id="T_68083_row13_col2" class="data row13 col2" >0.5000</td>
      <td id="T_68083_row13_col3" class="data row13 col3" >0.0000</td>
      <td id="T_68083_row13_col4" class="data row13 col4" >0.0000</td>
      <td id="T_68083_row13_col5" class="data row13 col5" >0.0000</td>
      <td id="T_68083_row13_col6" class="data row13 col6" >0.0000</td>
      <td id="T_68083_row13_col7" class="data row13 col7" >0.0000</td>
      <td id="T_68083_row13_col8" class="data row13 col8" >0.0120</td>
    </tr>
  </tbody>
</table>







返回当前设置中所有经过训练的模型中的最佳模型:


```python
best_ml = s.automl()
# best_ml
```


```python
# 打印效果最佳的模型
print(best)
```

    LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
                       intercept_scaling=1, l1_ratio=None, max_iter=1000,
                       multi_class='auto', n_jobs=None, penalty='l2',
                       random_state=0, solver='lbfgs', tol=0.0001, verbose=0,
                       warm_start=False)
    

**数据可视化**

PyCaret也提供了plot_model函数可视化模型的评估指标，plot_model函数中的plot用于设置评估指标类型。plot可用参数如下（注意并不是所有的模型都支持以下评估指标）：

- pipeline: Schematic drawing of the preprocessing pipeline
- auc: Area Under the Curve
- threshold: Discrimination Threshold
- pr: Precision Recall Curve
- confusion_matrix: Confusion Matrix
- error: Class Prediction Error
- class_report: Classification Report
- boundary: Decision Boundary
- rfe: Recursive Feature Selection
- learning: Learning Curve
- manifold: Manifold Learning
- calibration: Calibration Curve
- vc: Validation Curve
- dimension: Dimension Learning
- feature: Feature Importance
- feature_all: Feature Importance (All)
- parameter: Model Hyperparameter
- lift: Lift Curve
- gain: Gain Chart
- tree: Decision Tree
- ks: KS Statistic Plot


```python
# 提取所有模型预测结果
models_results = s.pull()
models_results
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Model</th>
      <th>Accuracy</th>
      <th>AUC</th>
      <th>Recall</th>
      <th>Prec.</th>
      <th>F1</th>
      <th>Kappa</th>
      <th>MCC</th>
      <th>TT (Sec)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>lr</th>
      <td>Logistic Regression</td>
      <td>0.7633</td>
      <td>0.8132</td>
      <td>0.4968</td>
      <td>0.7436</td>
      <td>0.5939</td>
      <td>0.4358</td>
      <td>0.4549</td>
      <td>0.272</td>
    </tr>
    <tr>
      <th>ridge</th>
      <td>Ridge Classifier</td>
      <td>0.7633</td>
      <td>0.8113</td>
      <td>0.5178</td>
      <td>0.7285</td>
      <td>0.6017</td>
      <td>0.4406</td>
      <td>0.4560</td>
      <td>0.009</td>
    </tr>
    <tr>
      <th>lda</th>
      <td>Linear Discriminant Analysis</td>
      <td>0.7633</td>
      <td>0.8110</td>
      <td>0.5497</td>
      <td>0.7069</td>
      <td>0.6154</td>
      <td>0.4489</td>
      <td>0.4583</td>
      <td>0.008</td>
    </tr>
    <tr>
      <th>ada</th>
      <td>Ada Boost Classifier</td>
      <td>0.7465</td>
      <td>0.7768</td>
      <td>0.5655</td>
      <td>0.6580</td>
      <td>0.6051</td>
      <td>0.4208</td>
      <td>0.4255</td>
      <td>0.019</td>
    </tr>
    <tr>
      <th>svm</th>
      <td>SVM - Linear Kernel</td>
      <td>0.7408</td>
      <td>0.8087</td>
      <td>0.5921</td>
      <td>0.6980</td>
      <td>0.6020</td>
      <td>0.4196</td>
      <td>0.4480</td>
      <td>0.008</td>
    </tr>
    <tr>
      <th>nb</th>
      <td>Naive Bayes</td>
      <td>0.7391</td>
      <td>0.7939</td>
      <td>0.5442</td>
      <td>0.6515</td>
      <td>0.5857</td>
      <td>0.3995</td>
      <td>0.4081</td>
      <td>0.008</td>
    </tr>
    <tr>
      <th>rf</th>
      <td>Random Forest Classifier</td>
      <td>0.7337</td>
      <td>0.8033</td>
      <td>0.5406</td>
      <td>0.6331</td>
      <td>0.5778</td>
      <td>0.3883</td>
      <td>0.3929</td>
      <td>0.035</td>
    </tr>
    <tr>
      <th>et</th>
      <td>Extra Trees Classifier</td>
      <td>0.7298</td>
      <td>0.7899</td>
      <td>0.5181</td>
      <td>0.6416</td>
      <td>0.5677</td>
      <td>0.3761</td>
      <td>0.3840</td>
      <td>0.045</td>
    </tr>
    <tr>
      <th>gbc</th>
      <td>Gradient Boosting Classifier</td>
      <td>0.7281</td>
      <td>0.8007</td>
      <td>0.5567</td>
      <td>0.6267</td>
      <td>0.5858</td>
      <td>0.3857</td>
      <td>0.3896</td>
      <td>0.026</td>
    </tr>
    <tr>
      <th>lightgbm</th>
      <td>Light Gradient Boosting Machine</td>
      <td>0.7242</td>
      <td>0.7811</td>
      <td>0.5827</td>
      <td>0.6096</td>
      <td>0.5935</td>
      <td>0.3859</td>
      <td>0.3876</td>
      <td>0.086</td>
    </tr>
    <tr>
      <th>qda</th>
      <td>Quadratic Discriminant Analysis</td>
      <td>0.7150</td>
      <td>0.7875</td>
      <td>0.4962</td>
      <td>0.6225</td>
      <td>0.5447</td>
      <td>0.3428</td>
      <td>0.3524</td>
      <td>0.008</td>
    </tr>
    <tr>
      <th>knn</th>
      <td>K Neighbors Classifier</td>
      <td>0.7131</td>
      <td>0.7425</td>
      <td>0.5287</td>
      <td>0.6005</td>
      <td>0.5577</td>
      <td>0.3480</td>
      <td>0.3528</td>
      <td>0.220</td>
    </tr>
    <tr>
      <th>dt</th>
      <td>Decision Tree Classifier</td>
      <td>0.6685</td>
      <td>0.6461</td>
      <td>0.5722</td>
      <td>0.5266</td>
      <td>0.5459</td>
      <td>0.2868</td>
      <td>0.2889</td>
      <td>0.010</td>
    </tr>
    <tr>
      <th>dummy</th>
      <td>Dummy Classifier</td>
      <td>0.6518</td>
      <td>0.5000</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.012</td>
    </tr>
  </tbody>
</table>
</div>




```python
s.plot_model(best, plot = 'confusion_matrix')
```






    
![png](output_27_1.png)
    


如果在jupyter环境，可以通过evaluate_model函数来交互式展示模型的性能：


```python
# s.evaluate_model(best)
```

**模型预测**

predict_model函数实现对数据进行预测，并返回包含预测标签prediction_label和分数prediction_score的Pandas表格。当data为None时，它预测测试集（在设置功能期间创建）上的标签和分数。


```python
# 预测整个数据集
res = s.predict_model(best, data=data)
# 查看各行预测结果
# res
```


<style type="text/css">
</style>
<table id="T_b5b97">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_b5b97_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_b5b97_level0_col1" class="col_heading level0 col1" >Accuracy</th>
      <th id="T_b5b97_level0_col2" class="col_heading level0 col2" >AUC</th>
      <th id="T_b5b97_level0_col3" class="col_heading level0 col3" >Recall</th>
      <th id="T_b5b97_level0_col4" class="col_heading level0 col4" >Prec.</th>
      <th id="T_b5b97_level0_col5" class="col_heading level0 col5" >F1</th>
      <th id="T_b5b97_level0_col6" class="col_heading level0 col6" >Kappa</th>
      <th id="T_b5b97_level0_col7" class="col_heading level0 col7" >MCC</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_b5b97_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_b5b97_row0_col0" class="data row0 col0" >Logistic Regression</td>
      <td id="T_b5b97_row0_col1" class="data row0 col1" >0.7708</td>
      <td id="T_b5b97_row0_col2" class="data row0 col2" >0.8312</td>
      <td id="T_b5b97_row0_col3" class="data row0 col3" >0.5149</td>
      <td id="T_b5b97_row0_col4" class="data row0 col4" >0.7500</td>
      <td id="T_b5b97_row0_col5" class="data row0 col5" >0.6106</td>
      <td id="T_b5b97_row0_col6" class="data row0 col6" >0.4561</td>
      <td id="T_b5b97_row0_col7" class="data row0 col7" >0.4723</td>
    </tr>
  </tbody>
</table>




```python
# 预测用于数据训练的测试集
res = s.predict_model(best)
```


<style type="text/css">
</style>
<table id="T_0fb78">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_0fb78_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_0fb78_level0_col1" class="col_heading level0 col1" >Accuracy</th>
      <th id="T_0fb78_level0_col2" class="col_heading level0 col2" >AUC</th>
      <th id="T_0fb78_level0_col3" class="col_heading level0 col3" >Recall</th>
      <th id="T_0fb78_level0_col4" class="col_heading level0 col4" >Prec.</th>
      <th id="T_0fb78_level0_col5" class="col_heading level0 col5" >F1</th>
      <th id="T_0fb78_level0_col6" class="col_heading level0 col6" >Kappa</th>
      <th id="T_0fb78_level0_col7" class="col_heading level0 col7" >MCC</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_0fb78_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_0fb78_row0_col0" class="data row0 col0" >Logistic Regression</td>
      <td id="T_0fb78_row0_col1" class="data row0 col1" >0.7576</td>
      <td id="T_0fb78_row0_col2" class="data row0 col2" >0.8553</td>
      <td id="T_0fb78_row0_col3" class="data row0 col3" >0.5062</td>
      <td id="T_0fb78_row0_col4" class="data row0 col4" >0.7193</td>
      <td id="T_0fb78_row0_col5" class="data row0 col5" >0.5942</td>
      <td id="T_0fb78_row0_col6" class="data row0 col6" >0.4287</td>
      <td id="T_0fb78_row0_col7" class="data row0 col7" >0.4422</td>
    </tr>
  </tbody>
</table>



**模型保存与导入**


```python
# 保存模型到本地
_ = s.save_model(best, 'best_model', verbose = False)
```


```python
# 导入模型
model = s.load_model( 'best_model')
# 查看模型结构
# model
```

    Transformation Pipeline and Model Successfully Loaded
    


```python
# 预测整个数据集
res = s.predict_model(model, data=data)
```


<style type="text/css">
</style>
<table id="T_e1b70">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_e1b70_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_e1b70_level0_col1" class="col_heading level0 col1" >Accuracy</th>
      <th id="T_e1b70_level0_col2" class="col_heading level0 col2" >AUC</th>
      <th id="T_e1b70_level0_col3" class="col_heading level0 col3" >Recall</th>
      <th id="T_e1b70_level0_col4" class="col_heading level0 col4" >Prec.</th>
      <th id="T_e1b70_level0_col5" class="col_heading level0 col5" >F1</th>
      <th id="T_e1b70_level0_col6" class="col_heading level0 col6" >Kappa</th>
      <th id="T_e1b70_level0_col7" class="col_heading level0 col7" >MCC</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_e1b70_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_e1b70_row0_col0" class="data row0 col0" >Logistic Regression</td>
      <td id="T_e1b70_row0_col1" class="data row0 col1" >0.7708</td>
      <td id="T_e1b70_row0_col2" class="data row0 col2" >0.8312</td>
      <td id="T_e1b70_row0_col3" class="data row0 col3" >0.5149</td>
      <td id="T_e1b70_row0_col4" class="data row0 col4" >0.7500</td>
      <td id="T_e1b70_row0_col5" class="data row0 col5" >0.6106</td>
      <td id="T_e1b70_row0_col6" class="data row0 col6" >0.4561</td>
      <td id="T_e1b70_row0_col7" class="data row0 col7" >0.4723</td>
    </tr>
  </tbody>
</table>



## 1.2 回归

PyCaret提供了regression模型实现回归任务，regression模块与classification模块使用方法一致。


```python
# 加载保险费用示例数据集
from pycaret.datasets import get_data
data = get_data(dataset='./datasets/insurance', verbose=False)
# 从网络下载
# data = get_data(dataset='insurance', verbose=False)
```


```python
data.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>sex</th>
      <th>bmi</th>
      <th>children</th>
      <th>smoker</th>
      <th>region</th>
      <th>charges</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>19</td>
      <td>female</td>
      <td>27.900</td>
      <td>0</td>
      <td>yes</td>
      <td>southwest</td>
      <td>16884.92400</td>
    </tr>
    <tr>
      <th>1</th>
      <td>18</td>
      <td>male</td>
      <td>33.770</td>
      <td>1</td>
      <td>no</td>
      <td>southeast</td>
      <td>1725.55230</td>
    </tr>
    <tr>
      <th>2</th>
      <td>28</td>
      <td>male</td>
      <td>33.000</td>
      <td>3</td>
      <td>no</td>
      <td>southeast</td>
      <td>4449.46200</td>
    </tr>
    <tr>
      <th>3</th>
      <td>33</td>
      <td>male</td>
      <td>22.705</td>
      <td>0</td>
      <td>no</td>
      <td>northwest</td>
      <td>21984.47061</td>
    </tr>
    <tr>
      <th>4</th>
      <td>32</td>
      <td>male</td>
      <td>28.880</td>
      <td>0</td>
      <td>no</td>
      <td>northwest</td>
      <td>3866.85520</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 创建数据管道
from pycaret.regression import RegressionExperiment
s = RegressionExperiment()
# 预测charges列
s.setup(data, target = 'charges', session_id = 0)
# 另一种数据管道创建方式
# from pycaret.regression import *
# s = setup(data, target = 'charges', session_id = 0)
```


<style type="text/css">
#T_e42e1_row9_col1 {
  background-color: lightgreen;
}
</style>
<table id="T_e42e1">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_e42e1_level0_col0" class="col_heading level0 col0" >Description</th>
      <th id="T_e42e1_level0_col1" class="col_heading level0 col1" >Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_e42e1_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_e42e1_row0_col0" class="data row0 col0" >Session id</td>
      <td id="T_e42e1_row0_col1" class="data row0 col1" >0</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row1" class="row_heading level0 row1" >1</th>
      <td id="T_e42e1_row1_col0" class="data row1 col0" >Target</td>
      <td id="T_e42e1_row1_col1" class="data row1 col1" >charges</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row2" class="row_heading level0 row2" >2</th>
      <td id="T_e42e1_row2_col0" class="data row2 col0" >Target type</td>
      <td id="T_e42e1_row2_col1" class="data row2 col1" >Regression</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row3" class="row_heading level0 row3" >3</th>
      <td id="T_e42e1_row3_col0" class="data row3 col0" >Original data shape</td>
      <td id="T_e42e1_row3_col1" class="data row3 col1" >(1338, 7)</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row4" class="row_heading level0 row4" >4</th>
      <td id="T_e42e1_row4_col0" class="data row4 col0" >Transformed data shape</td>
      <td id="T_e42e1_row4_col1" class="data row4 col1" >(1338, 10)</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row5" class="row_heading level0 row5" >5</th>
      <td id="T_e42e1_row5_col0" class="data row5 col0" >Transformed train set shape</td>
      <td id="T_e42e1_row5_col1" class="data row5 col1" >(936, 10)</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row6" class="row_heading level0 row6" >6</th>
      <td id="T_e42e1_row6_col0" class="data row6 col0" >Transformed test set shape</td>
      <td id="T_e42e1_row6_col1" class="data row6 col1" >(402, 10)</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row7" class="row_heading level0 row7" >7</th>
      <td id="T_e42e1_row7_col0" class="data row7 col0" >Numeric features</td>
      <td id="T_e42e1_row7_col1" class="data row7 col1" >3</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row8" class="row_heading level0 row8" >8</th>
      <td id="T_e42e1_row8_col0" class="data row8 col0" >Categorical features</td>
      <td id="T_e42e1_row8_col1" class="data row8 col1" >3</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row9" class="row_heading level0 row9" >9</th>
      <td id="T_e42e1_row9_col0" class="data row9 col0" >Preprocess</td>
      <td id="T_e42e1_row9_col1" class="data row9 col1" >True</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row10" class="row_heading level0 row10" >10</th>
      <td id="T_e42e1_row10_col0" class="data row10 col0" >Imputation type</td>
      <td id="T_e42e1_row10_col1" class="data row10 col1" >simple</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row11" class="row_heading level0 row11" >11</th>
      <td id="T_e42e1_row11_col0" class="data row11 col0" >Numeric imputation</td>
      <td id="T_e42e1_row11_col1" class="data row11 col1" >mean</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row12" class="row_heading level0 row12" >12</th>
      <td id="T_e42e1_row12_col0" class="data row12 col0" >Categorical imputation</td>
      <td id="T_e42e1_row12_col1" class="data row12 col1" >mode</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row13" class="row_heading level0 row13" >13</th>
      <td id="T_e42e1_row13_col0" class="data row13 col0" >Maximum one-hot encoding</td>
      <td id="T_e42e1_row13_col1" class="data row13 col1" >25</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row14" class="row_heading level0 row14" >14</th>
      <td id="T_e42e1_row14_col0" class="data row14 col0" >Encoding method</td>
      <td id="T_e42e1_row14_col1" class="data row14 col1" >None</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row15" class="row_heading level0 row15" >15</th>
      <td id="T_e42e1_row15_col0" class="data row15 col0" >Fold Generator</td>
      <td id="T_e42e1_row15_col1" class="data row15 col1" >KFold</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row16" class="row_heading level0 row16" >16</th>
      <td id="T_e42e1_row16_col0" class="data row16 col0" >Fold Number</td>
      <td id="T_e42e1_row16_col1" class="data row16 col1" >10</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row17" class="row_heading level0 row17" >17</th>
      <td id="T_e42e1_row17_col0" class="data row17 col0" >CPU Jobs</td>
      <td id="T_e42e1_row17_col1" class="data row17 col1" >-1</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row18" class="row_heading level0 row18" >18</th>
      <td id="T_e42e1_row18_col0" class="data row18 col0" >Use GPU</td>
      <td id="T_e42e1_row18_col1" class="data row18 col1" >False</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row19" class="row_heading level0 row19" >19</th>
      <td id="T_e42e1_row19_col0" class="data row19 col0" >Log Experiment</td>
      <td id="T_e42e1_row19_col1" class="data row19 col1" >False</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row20" class="row_heading level0 row20" >20</th>
      <td id="T_e42e1_row20_col0" class="data row20 col0" >Experiment Name</td>
      <td id="T_e42e1_row20_col1" class="data row20 col1" >reg-default-name</td>
    </tr>
    <tr>
      <th id="T_e42e1_level0_row21" class="row_heading level0 row21" >21</th>
      <td id="T_e42e1_row21_col0" class="data row21 col0" >USI</td>
      <td id="T_e42e1_row21_col1" class="data row21 col1" >eb9d</td>
    </tr>
  </tbody>
</table>






    <pycaret.regression.oop.RegressionExperiment at 0x200dedc2d30>




```python
# 评估各类模型
best = s.compare_models()
```






<style type="text/css">
#T_4a76a th {
  text-align: left;
}
#T_4a76a_row0_col0, #T_4a76a_row0_col6, #T_4a76a_row1_col0, #T_4a76a_row1_col1, #T_4a76a_row1_col2, #T_4a76a_row1_col3, #T_4a76a_row1_col4, #T_4a76a_row1_col5, #T_4a76a_row1_col6, #T_4a76a_row2_col0, #T_4a76a_row2_col1, #T_4a76a_row2_col2, #T_4a76a_row2_col3, #T_4a76a_row2_col4, #T_4a76a_row2_col5, #T_4a76a_row2_col6, #T_4a76a_row3_col0, #T_4a76a_row3_col1, #T_4a76a_row3_col2, #T_4a76a_row3_col3, #T_4a76a_row3_col4, #T_4a76a_row3_col5, #T_4a76a_row3_col6, #T_4a76a_row4_col0, #T_4a76a_row4_col1, #T_4a76a_row4_col2, #T_4a76a_row4_col3, #T_4a76a_row4_col4, #T_4a76a_row4_col5, #T_4a76a_row4_col6, #T_4a76a_row5_col0, #T_4a76a_row5_col1, #T_4a76a_row5_col2, #T_4a76a_row5_col3, #T_4a76a_row5_col4, #T_4a76a_row5_col5, #T_4a76a_row5_col6, #T_4a76a_row6_col0, #T_4a76a_row6_col1, #T_4a76a_row6_col2, #T_4a76a_row6_col3, #T_4a76a_row6_col4, #T_4a76a_row6_col5, #T_4a76a_row6_col6, #T_4a76a_row7_col0, #T_4a76a_row7_col1, #T_4a76a_row7_col2, #T_4a76a_row7_col3, #T_4a76a_row7_col4, #T_4a76a_row7_col5, #T_4a76a_row7_col6, #T_4a76a_row8_col0, #T_4a76a_row8_col1, #T_4a76a_row8_col2, #T_4a76a_row8_col3, #T_4a76a_row8_col4, #T_4a76a_row8_col5, #T_4a76a_row8_col6, #T_4a76a_row9_col0, #T_4a76a_row9_col1, #T_4a76a_row9_col2, #T_4a76a_row9_col3, #T_4a76a_row9_col4, #T_4a76a_row9_col5, #T_4a76a_row9_col6, #T_4a76a_row10_col0, #T_4a76a_row10_col1, #T_4a76a_row10_col2, #T_4a76a_row10_col3, #T_4a76a_row10_col4, #T_4a76a_row10_col5, #T_4a76a_row10_col6, #T_4a76a_row11_col0, #T_4a76a_row11_col1, #T_4a76a_row11_col2, #T_4a76a_row11_col3, #T_4a76a_row11_col4, #T_4a76a_row11_col5, #T_4a76a_row11_col6, #T_4a76a_row12_col0, #T_4a76a_row12_col1, #T_4a76a_row12_col2, #T_4a76a_row12_col3, #T_4a76a_row12_col4, #T_4a76a_row12_col5, #T_4a76a_row13_col0, #T_4a76a_row13_col1, #T_4a76a_row13_col2, #T_4a76a_row13_col3, #T_4a76a_row13_col4, #T_4a76a_row13_col5, #T_4a76a_row13_col6, #T_4a76a_row14_col0, #T_4a76a_row14_col1, #T_4a76a_row14_col2, #T_4a76a_row14_col3, #T_4a76a_row14_col4, #T_4a76a_row14_col5, #T_4a76a_row14_col6, #T_4a76a_row15_col0, #T_4a76a_row15_col1, #T_4a76a_row15_col2, #T_4a76a_row15_col3, #T_4a76a_row15_col4, #T_4a76a_row15_col5, #T_4a76a_row15_col6, #T_4a76a_row16_col0, #T_4a76a_row16_col1, #T_4a76a_row16_col2, #T_4a76a_row16_col3, #T_4a76a_row16_col4, #T_4a76a_row16_col5, #T_4a76a_row16_col6, #T_4a76a_row17_col0, #T_4a76a_row17_col1, #T_4a76a_row17_col2, #T_4a76a_row17_col3, #T_4a76a_row17_col4, #T_4a76a_row17_col5, #T_4a76a_row17_col6 {
  text-align: left;
}
#T_4a76a_row0_col1, #T_4a76a_row0_col2, #T_4a76a_row0_col3, #T_4a76a_row0_col4, #T_4a76a_row0_col5, #T_4a76a_row12_col6 {
  text-align: left;
  background-color: yellow;
}
#T_4a76a_row0_col7, #T_4a76a_row1_col7, #T_4a76a_row2_col7, #T_4a76a_row3_col7, #T_4a76a_row4_col7, #T_4a76a_row5_col7, #T_4a76a_row6_col7, #T_4a76a_row7_col7, #T_4a76a_row8_col7, #T_4a76a_row9_col7, #T_4a76a_row10_col7, #T_4a76a_row11_col7, #T_4a76a_row12_col7, #T_4a76a_row13_col7, #T_4a76a_row14_col7, #T_4a76a_row16_col7, #T_4a76a_row17_col7 {
  text-align: left;
  background-color: lightgrey;
}
#T_4a76a_row15_col7 {
  text-align: left;
  background-color: yellow;
  background-color: lightgrey;
}
</style>
<table id="T_4a76a">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_4a76a_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_4a76a_level0_col1" class="col_heading level0 col1" >MAE</th>
      <th id="T_4a76a_level0_col2" class="col_heading level0 col2" >MSE</th>
      <th id="T_4a76a_level0_col3" class="col_heading level0 col3" >RMSE</th>
      <th id="T_4a76a_level0_col4" class="col_heading level0 col4" >R2</th>
      <th id="T_4a76a_level0_col5" class="col_heading level0 col5" >RMSLE</th>
      <th id="T_4a76a_level0_col6" class="col_heading level0 col6" >MAPE</th>
      <th id="T_4a76a_level0_col7" class="col_heading level0 col7" >TT (Sec)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_4a76a_level0_row0" class="row_heading level0 row0" >gbr</th>
      <td id="T_4a76a_row0_col0" class="data row0 col0" >Gradient Boosting Regressor</td>
      <td id="T_4a76a_row0_col1" class="data row0 col1" >2723.2453</td>
      <td id="T_4a76a_row0_col2" class="data row0 col2" >23787529.5872</td>
      <td id="T_4a76a_row0_col3" class="data row0 col3" >4832.4785</td>
      <td id="T_4a76a_row0_col4" class="data row0 col4" >0.8254</td>
      <td id="T_4a76a_row0_col5" class="data row0 col5" >0.4427</td>
      <td id="T_4a76a_row0_col6" class="data row0 col6" >0.3140</td>
      <td id="T_4a76a_row0_col7" class="data row0 col7" >0.0550</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row1" class="row_heading level0 row1" >lightgbm</th>
      <td id="T_4a76a_row1_col0" class="data row1 col0" >Light Gradient Boosting Machine</td>
      <td id="T_4a76a_row1_col1" class="data row1 col1" >2998.1311</td>
      <td id="T_4a76a_row1_col2" class="data row1 col2" >25738691.2181</td>
      <td id="T_4a76a_row1_col3" class="data row1 col3" >5012.2404</td>
      <td id="T_4a76a_row1_col4" class="data row1 col4" >0.8106</td>
      <td id="T_4a76a_row1_col5" class="data row1 col5" >0.5525</td>
      <td id="T_4a76a_row1_col6" class="data row1 col6" >0.3709</td>
      <td id="T_4a76a_row1_col7" class="data row1 col7" >0.1140</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row2" class="row_heading level0 row2" >rf</th>
      <td id="T_4a76a_row2_col0" class="data row2 col0" >Random Forest Regressor</td>
      <td id="T_4a76a_row2_col1" class="data row2 col1" >2915.7018</td>
      <td id="T_4a76a_row2_col2" class="data row2 col2" >26780127.0016</td>
      <td id="T_4a76a_row2_col3" class="data row2 col3" >5109.5098</td>
      <td id="T_4a76a_row2_col4" class="data row2 col4" >0.8031</td>
      <td id="T_4a76a_row2_col5" class="data row2 col5" >0.4855</td>
      <td id="T_4a76a_row2_col6" class="data row2 col6" >0.3520</td>
      <td id="T_4a76a_row2_col7" class="data row2 col7" >0.0670</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row3" class="row_heading level0 row3" >et</th>
      <td id="T_4a76a_row3_col0" class="data row3 col0" >Extra Trees Regressor</td>
      <td id="T_4a76a_row3_col1" class="data row3 col1" >2841.8257</td>
      <td id="T_4a76a_row3_col2" class="data row3 col2" >28559316.9533</td>
      <td id="T_4a76a_row3_col3" class="data row3 col3" >5243.5828</td>
      <td id="T_4a76a_row3_col4" class="data row3 col4" >0.7931</td>
      <td id="T_4a76a_row3_col5" class="data row3 col5" >0.4671</td>
      <td id="T_4a76a_row3_col6" class="data row3 col6" >0.3218</td>
      <td id="T_4a76a_row3_col7" class="data row3 col7" >0.0670</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row4" class="row_heading level0 row4" >ada</th>
      <td id="T_4a76a_row4_col0" class="data row4 col0" >AdaBoost Regressor</td>
      <td id="T_4a76a_row4_col1" class="data row4 col1" >4180.2669</td>
      <td id="T_4a76a_row4_col2" class="data row4 col2" >28289551.0048</td>
      <td id="T_4a76a_row4_col3" class="data row4 col3" >5297.6817</td>
      <td id="T_4a76a_row4_col4" class="data row4 col4" >0.7886</td>
      <td id="T_4a76a_row4_col5" class="data row4 col5" >0.5935</td>
      <td id="T_4a76a_row4_col6" class="data row4 col6" >0.6545</td>
      <td id="T_4a76a_row4_col7" class="data row4 col7" >0.0210</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row5" class="row_heading level0 row5" >ridge</th>
      <td id="T_4a76a_row5_col0" class="data row5 col0" >Ridge Regression</td>
      <td id="T_4a76a_row5_col1" class="data row5 col1" >4304.2640</td>
      <td id="T_4a76a_row5_col2" class="data row5 col2" >38786967.4768</td>
      <td id="T_4a76a_row5_col3" class="data row5 col3" >6188.6966</td>
      <td id="T_4a76a_row5_col4" class="data row5 col4" >0.7152</td>
      <td id="T_4a76a_row5_col5" class="data row5 col5" >0.5794</td>
      <td id="T_4a76a_row5_col6" class="data row5 col6" >0.4283</td>
      <td id="T_4a76a_row5_col7" class="data row5 col7" >0.0230</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row6" class="row_heading level0 row6" >lar</th>
      <td id="T_4a76a_row6_col0" class="data row6 col0" >Least Angle Regression</td>
      <td id="T_4a76a_row6_col1" class="data row6 col1" >4293.9886</td>
      <td id="T_4a76a_row6_col2" class="data row6 col2" >38781666.5991</td>
      <td id="T_4a76a_row6_col3" class="data row6 col3" >6188.3301</td>
      <td id="T_4a76a_row6_col4" class="data row6 col4" >0.7151</td>
      <td id="T_4a76a_row6_col5" class="data row6 col5" >0.5893</td>
      <td id="T_4a76a_row6_col6" class="data row6 col6" >0.4263</td>
      <td id="T_4a76a_row6_col7" class="data row6 col7" >0.0210</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row7" class="row_heading level0 row7" >llar</th>
      <td id="T_4a76a_row7_col0" class="data row7 col0" >Lasso Least Angle Regression</td>
      <td id="T_4a76a_row7_col1" class="data row7 col1" >4294.2135</td>
      <td id="T_4a76a_row7_col2" class="data row7 col2" >38780221.0039</td>
      <td id="T_4a76a_row7_col3" class="data row7 col3" >6188.1906</td>
      <td id="T_4a76a_row7_col4" class="data row7 col4" >0.7151</td>
      <td id="T_4a76a_row7_col5" class="data row7 col5" >0.5891</td>
      <td id="T_4a76a_row7_col6" class="data row7 col6" >0.4264</td>
      <td id="T_4a76a_row7_col7" class="data row7 col7" >0.0200</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row8" class="row_heading level0 row8" >br</th>
      <td id="T_4a76a_row8_col0" class="data row8 col0" >Bayesian Ridge</td>
      <td id="T_4a76a_row8_col1" class="data row8 col1" >4299.8532</td>
      <td id="T_4a76a_row8_col2" class="data row8 col2" >38785479.0984</td>
      <td id="T_4a76a_row8_col3" class="data row8 col3" >6188.6026</td>
      <td id="T_4a76a_row8_col4" class="data row8 col4" >0.7151</td>
      <td id="T_4a76a_row8_col5" class="data row8 col5" >0.5784</td>
      <td id="T_4a76a_row8_col6" class="data row8 col6" >0.4274</td>
      <td id="T_4a76a_row8_col7" class="data row8 col7" >0.0200</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row9" class="row_heading level0 row9" >lasso</th>
      <td id="T_4a76a_row9_col0" class="data row9 col0" >Lasso Regression</td>
      <td id="T_4a76a_row9_col1" class="data row9 col1" >4294.2186</td>
      <td id="T_4a76a_row9_col2" class="data row9 col2" >38780210.5665</td>
      <td id="T_4a76a_row9_col3" class="data row9 col3" >6188.1898</td>
      <td id="T_4a76a_row9_col4" class="data row9 col4" >0.7151</td>
      <td id="T_4a76a_row9_col5" class="data row9 col5" >0.5892</td>
      <td id="T_4a76a_row9_col6" class="data row9 col6" >0.4264</td>
      <td id="T_4a76a_row9_col7" class="data row9 col7" >0.0370</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row10" class="row_heading level0 row10" >lr</th>
      <td id="T_4a76a_row10_col0" class="data row10 col0" >Linear Regression</td>
      <td id="T_4a76a_row10_col1" class="data row10 col1" >4293.9886</td>
      <td id="T_4a76a_row10_col2" class="data row10 col2" >38781666.5991</td>
      <td id="T_4a76a_row10_col3" class="data row10 col3" >6188.3301</td>
      <td id="T_4a76a_row10_col4" class="data row10 col4" >0.7151</td>
      <td id="T_4a76a_row10_col5" class="data row10 col5" >0.5893</td>
      <td id="T_4a76a_row10_col6" class="data row10 col6" >0.4263</td>
      <td id="T_4a76a_row10_col7" class="data row10 col7" >0.0350</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row11" class="row_heading level0 row11" >dt</th>
      <td id="T_4a76a_row11_col0" class="data row11 col0" >Decision Tree Regressor</td>
      <td id="T_4a76a_row11_col1" class="data row11 col1" >3550.6534</td>
      <td id="T_4a76a_row11_col2" class="data row11 col2" >51149204.9032</td>
      <td id="T_4a76a_row11_col3" class="data row11 col3" >7095.9170</td>
      <td id="T_4a76a_row11_col4" class="data row11 col4" >0.6127</td>
      <td id="T_4a76a_row11_col5" class="data row11 col5" >0.5839</td>
      <td id="T_4a76a_row11_col6" class="data row11 col6" >0.4537</td>
      <td id="T_4a76a_row11_col7" class="data row11 col7" >0.0230</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row12" class="row_heading level0 row12" >huber</th>
      <td id="T_4a76a_row12_col0" class="data row12 col0" >Huber Regressor</td>
      <td id="T_4a76a_row12_col1" class="data row12 col1" >3769.3076</td>
      <td id="T_4a76a_row12_col2" class="data row12 col2" >53638697.2337</td>
      <td id="T_4a76a_row12_col3" class="data row12 col3" >7254.7108</td>
      <td id="T_4a76a_row12_col4" class="data row12 col4" >0.6095</td>
      <td id="T_4a76a_row12_col5" class="data row12 col5" >0.4528</td>
      <td id="T_4a76a_row12_col6" class="data row12 col6" >0.2187</td>
      <td id="T_4a76a_row12_col7" class="data row12 col7" >0.0250</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row13" class="row_heading level0 row13" >par</th>
      <td id="T_4a76a_row13_col0" class="data row13 col0" >Passive Aggressive Regressor</td>
      <td id="T_4a76a_row13_col1" class="data row13 col1" >4144.7180</td>
      <td id="T_4a76a_row13_col2" class="data row13 col2" >62949698.1775</td>
      <td id="T_4a76a_row13_col3" class="data row13 col3" >7862.7604</td>
      <td id="T_4a76a_row13_col4" class="data row13 col4" >0.5433</td>
      <td id="T_4a76a_row13_col5" class="data row13 col5" >0.4634</td>
      <td id="T_4a76a_row13_col6" class="data row13 col6" >0.2465</td>
      <td id="T_4a76a_row13_col7" class="data row13 col7" >0.0210</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row14" class="row_heading level0 row14" >en</th>
      <td id="T_4a76a_row14_col0" class="data row14 col0" >Elastic Net</td>
      <td id="T_4a76a_row14_col1" class="data row14 col1" >7248.9376</td>
      <td id="T_4a76a_row14_col2" class="data row14 col2" >89841235.9517</td>
      <td id="T_4a76a_row14_col3" class="data row14 col3" >9405.5846</td>
      <td id="T_4a76a_row14_col4" class="data row14 col4" >0.3534</td>
      <td id="T_4a76a_row14_col5" class="data row14 col5" >0.7346</td>
      <td id="T_4a76a_row14_col6" class="data row14 col6" >0.9238</td>
      <td id="T_4a76a_row14_col7" class="data row14 col7" >0.0210</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row15" class="row_heading level0 row15" >omp</th>
      <td id="T_4a76a_row15_col0" class="data row15 col0" >Orthogonal Matching Pursuit</td>
      <td id="T_4a76a_row15_col1" class="data row15 col1" >8916.1927</td>
      <td id="T_4a76a_row15_col2" class="data row15 col2" >130904492.3067</td>
      <td id="T_4a76a_row15_col3" class="data row15 col3" >11356.4120</td>
      <td id="T_4a76a_row15_col4" class="data row15 col4" >0.0561</td>
      <td id="T_4a76a_row15_col5" class="data row15 col5" >0.8781</td>
      <td id="T_4a76a_row15_col6" class="data row15 col6" >1.1598</td>
      <td id="T_4a76a_row15_col7" class="data row15 col7" >0.0180</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row16" class="row_heading level0 row16" >knn</th>
      <td id="T_4a76a_row16_col0" class="data row16 col0" >K Neighbors Regressor</td>
      <td id="T_4a76a_row16_col1" class="data row16 col1" >8161.8875</td>
      <td id="T_4a76a_row16_col2" class="data row16 col2" >137982796.8000</td>
      <td id="T_4a76a_row16_col3" class="data row16 col3" >11676.3735</td>
      <td id="T_4a76a_row16_col4" class="data row16 col4" >-0.0011</td>
      <td id="T_4a76a_row16_col5" class="data row16 col5" >0.8744</td>
      <td id="T_4a76a_row16_col6" class="data row16 col6" >0.9742</td>
      <td id="T_4a76a_row16_col7" class="data row16 col7" >0.0250</td>
    </tr>
    <tr>
      <th id="T_4a76a_level0_row17" class="row_heading level0 row17" >dummy</th>
      <td id="T_4a76a_row17_col0" class="data row17 col0" >Dummy Regressor</td>
      <td id="T_4a76a_row17_col1" class="data row17 col1" >8892.4478</td>
      <td id="T_4a76a_row17_col2" class="data row17 col2" >141597492.8000</td>
      <td id="T_4a76a_row17_col3" class="data row17 col3" >11823.4271</td>
      <td id="T_4a76a_row17_col4" class="data row17 col4" >-0.0221</td>
      <td id="T_4a76a_row17_col5" class="data row17 col5" >0.9868</td>
      <td id="T_4a76a_row17_col6" class="data row17 col6" >1.4909</td>
      <td id="T_4a76a_row17_col7" class="data row17 col7" >0.0210</td>
    </tr>
  </tbody>
</table>








```python
print(best)
```

    GradientBoostingRegressor(alpha=0.9, ccp_alpha=0.0, criterion='friedman_mse',
                              init=None, learning_rate=0.1, loss='squared_error',
                              max_depth=3, max_features=None, max_leaf_nodes=None,
                              min_impurity_decrease=0.0, min_samples_leaf=1,
                              min_samples_split=2, min_weight_fraction_leaf=0.0,
                              n_estimators=100, n_iter_no_change=None,
                              random_state=0, subsample=1.0, tol=0.0001,
                              validation_fraction=0.1, verbose=0, warm_start=False)
    

## 1.3 聚类

ParCaret提供了clustering模块实现无监督聚类。

**数据准备**


```python
# 导入珠宝数据集
from pycaret.datasets import get_data
# 根据数据集特征进行聚类
data = get_data('./datasets/jewellery')
# data = get_data('jewellery')
```


<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Income</th>
      <th>SpendingScore</th>
      <th>Savings</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>58</td>
      <td>77769</td>
      <td>0.791329</td>
      <td>6559.829923</td>
    </tr>
    <tr>
      <th>1</th>
      <td>59</td>
      <td>81799</td>
      <td>0.791082</td>
      <td>5417.661426</td>
    </tr>
    <tr>
      <th>2</th>
      <td>62</td>
      <td>74751</td>
      <td>0.702657</td>
      <td>9258.992965</td>
    </tr>
    <tr>
      <th>3</th>
      <td>59</td>
      <td>74373</td>
      <td>0.765680</td>
      <td>7346.334504</td>
    </tr>
    <tr>
      <th>4</th>
      <td>87</td>
      <td>17760</td>
      <td>0.348778</td>
      <td>16869.507130</td>
    </tr>
  </tbody>
</table>
</div>



```python
# 创建数据管道
from pycaret.clustering import ClusteringExperiment
s = ClusteringExperiment()
# normalize归一化数据
s.setup(data, normalize = True, verbose = False)
# 另一种数据管道创建方式
# from pycaret.clustering import *
# s = setup(data, normalize = True)
```




    <pycaret.clustering.oop.ClusteringExperiment at 0x200dec86340>



**模型创建**

PyCaret在聚类任务中提供create_model选择合适的方法来构建聚类模型，而不是全部比较。


```python
kmeans = s.create_model('kmeans')
```






<style type="text/css">
</style>
<table id="T_69bcb">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_69bcb_level0_col0" class="col_heading level0 col0" >Silhouette</th>
      <th id="T_69bcb_level0_col1" class="col_heading level0 col1" >Calinski-Harabasz</th>
      <th id="T_69bcb_level0_col2" class="col_heading level0 col2" >Davies-Bouldin</th>
      <th id="T_69bcb_level0_col3" class="col_heading level0 col3" >Homogeneity</th>
      <th id="T_69bcb_level0_col4" class="col_heading level0 col4" >Rand Index</th>
      <th id="T_69bcb_level0_col5" class="col_heading level0 col5" >Completeness</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_69bcb_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_69bcb_row0_col0" class="data row0 col0" >0.7581</td>
      <td id="T_69bcb_row0_col1" class="data row0 col1" >1611.2647</td>
      <td id="T_69bcb_row0_col2" class="data row0 col2" >0.3743</td>
      <td id="T_69bcb_row0_col3" class="data row0 col3" >0</td>
      <td id="T_69bcb_row0_col4" class="data row0 col4" >0</td>
      <td id="T_69bcb_row0_col5" class="data row0 col5" >0</td>
    </tr>
  </tbody>
</table>







create_model函数支持的聚类方法如下：


```python
s.models()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Reference</th>
    </tr>
    <tr>
      <th>ID</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>kmeans</th>
      <td>K-Means Clustering</td>
      <td>sklearn.cluster._kmeans.KMeans</td>
    </tr>
    <tr>
      <th>ap</th>
      <td>Affinity Propagation</td>
      <td>sklearn.cluster._affinity_propagation.Affinity...</td>
    </tr>
    <tr>
      <th>meanshift</th>
      <td>Mean Shift Clustering</td>
      <td>sklearn.cluster._mean_shift.MeanShift</td>
    </tr>
    <tr>
      <th>sc</th>
      <td>Spectral Clustering</td>
      <td>sklearn.cluster._spectral.SpectralClustering</td>
    </tr>
    <tr>
      <th>hclust</th>
      <td>Agglomerative Clustering</td>
      <td>sklearn.cluster._agglomerative.AgglomerativeCl...</td>
    </tr>
    <tr>
      <th>dbscan</th>
      <td>Density-Based Spatial Clustering</td>
      <td>sklearn.cluster._dbscan.DBSCAN</td>
    </tr>
    <tr>
      <th>optics</th>
      <td>OPTICS Clustering</td>
      <td>sklearn.cluster._optics.OPTICS</td>
    </tr>
    <tr>
      <th>birch</th>
      <td>Birch Clustering</td>
      <td>sklearn.cluster._birch.Birch</td>
    </tr>
  </tbody>
</table>
</div>




```python
print(kmeans)
# 查看聚类数
print(kmeans.n_clusters)
```

    KMeans(algorithm='lloyd', copy_x=True, init='k-means++', max_iter=300,
           n_clusters=4, n_init='auto', random_state=1459, tol=0.0001, verbose=0)
    4
    

**数据展示**


```python
# jupyter环境下交互可视化展示
# s.evaluate_model(kmeans)
```


```python
# 结果可视化 
# 'cluster' - Cluster PCA Plot (2d)
# 'tsne' - Cluster t-SNE (3d)
# 'elbow' - Elbow Plot
# 'silhouette' - Silhouette Plot
# 'distance' - Distance Plot
# 'distribution' - Distribution Plot
s.plot_model(kmeans, plot = 'elbow')
```






    
![png](output_55_1.png)
    


**标签分配与数据预测**

为训练数据分配聚类标签：


```python
result = s.assign_model(kmeans)
result.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Income</th>
      <th>SpendingScore</th>
      <th>Savings</th>
      <th>Cluster</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>58</td>
      <td>77769</td>
      <td>0.791329</td>
      <td>6559.830078</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>59</td>
      <td>81799</td>
      <td>0.791082</td>
      <td>5417.661621</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>62</td>
      <td>74751</td>
      <td>0.702657</td>
      <td>9258.993164</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>59</td>
      <td>74373</td>
      <td>0.765680</td>
      <td>7346.334473</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>87</td>
      <td>17760</td>
      <td>0.348778</td>
      <td>16869.507812</td>
      <td>Cluster 1</td>
    </tr>
  </tbody>
</table>
</div>



为新的数据进行标签分配：


```python
predictions = s.predict_model(kmeans, data = data)
predictions.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Income</th>
      <th>SpendingScore</th>
      <th>Savings</th>
      <th>Cluster</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.042287</td>
      <td>0.062733</td>
      <td>1.103593</td>
      <td>-1.072467</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-0.000821</td>
      <td>0.174811</td>
      <td>1.102641</td>
      <td>-1.303473</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.123577</td>
      <td>-0.021200</td>
      <td>0.761727</td>
      <td>-0.526556</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-0.000821</td>
      <td>-0.031712</td>
      <td>1.004705</td>
      <td>-0.913395</td>
      <td>Cluster 2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.160228</td>
      <td>-1.606165</td>
      <td>-0.602619</td>
      <td>1.012686</td>
      <td>Cluster 1</td>
    </tr>
  </tbody>
</table>
</div>



## 1.4 异常检测

PyCaret的anomaly detection模块是一个无监督的机器学习模块，用于识别与大多数数据存在显著差异的罕见项目、事件或观测值。通常，这些异常项目会转化为某种问题，如银行欺诈、结构缺陷、医疗问题或错误。anomaly detection模块的使用类似于cluster模块。

**数据准备**


```python
from pycaret.datasets import get_data
data = get_data('./datasets/anomaly')
# data = get_data('anomaly')
```


<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Col1</th>
      <th>Col2</th>
      <th>Col3</th>
      <th>Col4</th>
      <th>Col5</th>
      <th>Col6</th>
      <th>Col7</th>
      <th>Col8</th>
      <th>Col9</th>
      <th>Col10</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.263995</td>
      <td>0.764929</td>
      <td>0.138424</td>
      <td>0.935242</td>
      <td>0.605867</td>
      <td>0.518790</td>
      <td>0.912225</td>
      <td>0.608234</td>
      <td>0.723782</td>
      <td>0.733591</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.546092</td>
      <td>0.653975</td>
      <td>0.065575</td>
      <td>0.227772</td>
      <td>0.845269</td>
      <td>0.837066</td>
      <td>0.272379</td>
      <td>0.331679</td>
      <td>0.429297</td>
      <td>0.367422</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.336714</td>
      <td>0.538842</td>
      <td>0.192801</td>
      <td>0.553563</td>
      <td>0.074515</td>
      <td>0.332993</td>
      <td>0.365792</td>
      <td>0.861309</td>
      <td>0.899017</td>
      <td>0.088600</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.092108</td>
      <td>0.995017</td>
      <td>0.014465</td>
      <td>0.176371</td>
      <td>0.241530</td>
      <td>0.514724</td>
      <td>0.562208</td>
      <td>0.158963</td>
      <td>0.073715</td>
      <td>0.208463</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.325261</td>
      <td>0.805968</td>
      <td>0.957033</td>
      <td>0.331665</td>
      <td>0.307923</td>
      <td>0.355315</td>
      <td>0.501899</td>
      <td>0.558449</td>
      <td>0.885169</td>
      <td>0.182754</td>
    </tr>
  </tbody>
</table>
</div>



```python
from pycaret.anomaly import AnomalyExperiment
s = AnomalyExperiment()
s.setup(data, session_id = 0)
# 另一种加载方式
# from pycaret.anomaly import *
# s = setup(data, session_id = 0)
```


<style type="text/css">
#T_f8b5c_row4_col1 {
  background-color: lightgreen;
}
</style>
<table id="T_f8b5c">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_f8b5c_level0_col0" class="col_heading level0 col0" >Description</th>
      <th id="T_f8b5c_level0_col1" class="col_heading level0 col1" >Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_f8b5c_level0_row0" class="row_heading level0 row0" >0</th>
      <td id="T_f8b5c_row0_col0" class="data row0 col0" >Session id</td>
      <td id="T_f8b5c_row0_col1" class="data row0 col1" >0</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row1" class="row_heading level0 row1" >1</th>
      <td id="T_f8b5c_row1_col0" class="data row1 col0" >Original data shape</td>
      <td id="T_f8b5c_row1_col1" class="data row1 col1" >(1000, 10)</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row2" class="row_heading level0 row2" >2</th>
      <td id="T_f8b5c_row2_col0" class="data row2 col0" >Transformed data shape</td>
      <td id="T_f8b5c_row2_col1" class="data row2 col1" >(1000, 10)</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row3" class="row_heading level0 row3" >3</th>
      <td id="T_f8b5c_row3_col0" class="data row3 col0" >Numeric features</td>
      <td id="T_f8b5c_row3_col1" class="data row3 col1" >10</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row4" class="row_heading level0 row4" >4</th>
      <td id="T_f8b5c_row4_col0" class="data row4 col0" >Preprocess</td>
      <td id="T_f8b5c_row4_col1" class="data row4 col1" >True</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row5" class="row_heading level0 row5" >5</th>
      <td id="T_f8b5c_row5_col0" class="data row5 col0" >Imputation type</td>
      <td id="T_f8b5c_row5_col1" class="data row5 col1" >simple</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row6" class="row_heading level0 row6" >6</th>
      <td id="T_f8b5c_row6_col0" class="data row6 col0" >Numeric imputation</td>
      <td id="T_f8b5c_row6_col1" class="data row6 col1" >mean</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row7" class="row_heading level0 row7" >7</th>
      <td id="T_f8b5c_row7_col0" class="data row7 col0" >Categorical imputation</td>
      <td id="T_f8b5c_row7_col1" class="data row7 col1" >mode</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row8" class="row_heading level0 row8" >8</th>
      <td id="T_f8b5c_row8_col0" class="data row8 col0" >CPU Jobs</td>
      <td id="T_f8b5c_row8_col1" class="data row8 col1" >-1</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row9" class="row_heading level0 row9" >9</th>
      <td id="T_f8b5c_row9_col0" class="data row9 col0" >Use GPU</td>
      <td id="T_f8b5c_row9_col1" class="data row9 col1" >False</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row10" class="row_heading level0 row10" >10</th>
      <td id="T_f8b5c_row10_col0" class="data row10 col0" >Log Experiment</td>
      <td id="T_f8b5c_row10_col1" class="data row10 col1" >False</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row11" class="row_heading level0 row11" >11</th>
      <td id="T_f8b5c_row11_col0" class="data row11 col0" >Experiment Name</td>
      <td id="T_f8b5c_row11_col1" class="data row11 col1" >anomaly-default-name</td>
    </tr>
    <tr>
      <th id="T_f8b5c_level0_row12" class="row_heading level0 row12" >12</th>
      <td id="T_f8b5c_row12_col0" class="data row12 col0" >USI</td>
      <td id="T_f8b5c_row12_col1" class="data row12 col1" >54db</td>
    </tr>
  </tbody>
</table>






    <pycaret.anomaly.oop.AnomalyExperiment at 0x200e14f5250>



**模型创建**


```python
iforest = s.create_model('iforest')
print(iforest)
```













    IForest(behaviour='new', bootstrap=False, contamination=0.05,
        max_features=1.0, max_samples='auto', n_estimators=100, n_jobs=-1,
        random_state=0, verbose=0)
    

anomaly detection模块所支持的模型列表如下：


```python
s.models()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Reference</th>
    </tr>
    <tr>
      <th>ID</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>abod</th>
      <td>Angle-base Outlier Detection</td>
      <td>pyod.models.abod.ABOD</td>
    </tr>
    <tr>
      <th>cluster</th>
      <td>Clustering-Based Local Outlier</td>
      <td>pycaret.internal.patches.pyod.CBLOFForceToDouble</td>
    </tr>
    <tr>
      <th>cof</th>
      <td>Connectivity-Based Local Outlier</td>
      <td>pyod.models.cof.COF</td>
    </tr>
    <tr>
      <th>iforest</th>
      <td>Isolation Forest</td>
      <td>pyod.models.iforest.IForest</td>
    </tr>
    <tr>
      <th>histogram</th>
      <td>Histogram-based Outlier Detection</td>
      <td>pyod.models.hbos.HBOS</td>
    </tr>
    <tr>
      <th>knn</th>
      <td>K-Nearest Neighbors Detector</td>
      <td>pyod.models.knn.KNN</td>
    </tr>
    <tr>
      <th>lof</th>
      <td>Local Outlier Factor</td>
      <td>pyod.models.lof.LOF</td>
    </tr>
    <tr>
      <th>svm</th>
      <td>One-class SVM detector</td>
      <td>pyod.models.ocsvm.OCSVM</td>
    </tr>
    <tr>
      <th>pca</th>
      <td>Principal Component Analysis</td>
      <td>pyod.models.pca.PCA</td>
    </tr>
    <tr>
      <th>mcd</th>
      <td>Minimum Covariance Determinant</td>
      <td>pyod.models.mcd.MCD</td>
    </tr>
    <tr>
      <th>sod</th>
      <td>Subspace Outlier Detection</td>
      <td>pyod.models.sod.SOD</td>
    </tr>
    <tr>
      <th>sos</th>
      <td>Stochastic Outlier Selection</td>
      <td>pyod.models.sos.SOS</td>
    </tr>
  </tbody>
</table>
</div>



**标签分配与数据预测**

为训练数据分配聚类标签：


```python
result = s.assign_model(iforest)
result.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Col1</th>
      <th>Col2</th>
      <th>Col3</th>
      <th>Col4</th>
      <th>Col5</th>
      <th>Col6</th>
      <th>Col7</th>
      <th>Col8</th>
      <th>Col9</th>
      <th>Col10</th>
      <th>Anomaly</th>
      <th>Anomaly_Score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.263995</td>
      <td>0.764929</td>
      <td>0.138424</td>
      <td>0.935242</td>
      <td>0.605867</td>
      <td>0.518790</td>
      <td>0.912225</td>
      <td>0.608234</td>
      <td>0.723782</td>
      <td>0.733591</td>
      <td>0</td>
      <td>-0.016205</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.546092</td>
      <td>0.653975</td>
      <td>0.065575</td>
      <td>0.227772</td>
      <td>0.845269</td>
      <td>0.837066</td>
      <td>0.272379</td>
      <td>0.331679</td>
      <td>0.429297</td>
      <td>0.367422</td>
      <td>0</td>
      <td>-0.068052</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.336714</td>
      <td>0.538842</td>
      <td>0.192801</td>
      <td>0.553563</td>
      <td>0.074515</td>
      <td>0.332993</td>
      <td>0.365792</td>
      <td>0.861309</td>
      <td>0.899017</td>
      <td>0.088600</td>
      <td>1</td>
      <td>0.009221</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.092108</td>
      <td>0.995017</td>
      <td>0.014465</td>
      <td>0.176371</td>
      <td>0.241530</td>
      <td>0.514724</td>
      <td>0.562208</td>
      <td>0.158963</td>
      <td>0.073715</td>
      <td>0.208463</td>
      <td>1</td>
      <td>0.056690</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.325261</td>
      <td>0.805968</td>
      <td>0.957033</td>
      <td>0.331665</td>
      <td>0.307923</td>
      <td>0.355315</td>
      <td>0.501899</td>
      <td>0.558449</td>
      <td>0.885169</td>
      <td>0.182754</td>
      <td>0</td>
      <td>-0.012945</td>
    </tr>
  </tbody>
</table>
</div>



为新的数据进行标签分配：


```python
predictions = s.predict_model(iforest, data = data)
predictions.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Col1</th>
      <th>Col2</th>
      <th>Col3</th>
      <th>Col4</th>
      <th>Col5</th>
      <th>Col6</th>
      <th>Col7</th>
      <th>Col8</th>
      <th>Col9</th>
      <th>Col10</th>
      <th>Anomaly</th>
      <th>Anomaly_Score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.263995</td>
      <td>0.764929</td>
      <td>0.138424</td>
      <td>0.935242</td>
      <td>0.605867</td>
      <td>0.518790</td>
      <td>0.912225</td>
      <td>0.608234</td>
      <td>0.723782</td>
      <td>0.733591</td>
      <td>0</td>
      <td>-0.016205</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.546092</td>
      <td>0.653975</td>
      <td>0.065575</td>
      <td>0.227772</td>
      <td>0.845269</td>
      <td>0.837066</td>
      <td>0.272379</td>
      <td>0.331679</td>
      <td>0.429297</td>
      <td>0.367422</td>
      <td>0</td>
      <td>-0.068052</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.336714</td>
      <td>0.538842</td>
      <td>0.192801</td>
      <td>0.553563</td>
      <td>0.074515</td>
      <td>0.332993</td>
      <td>0.365792</td>
      <td>0.861309</td>
      <td>0.899017</td>
      <td>0.088600</td>
      <td>1</td>
      <td>0.009221</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.092108</td>
      <td>0.995017</td>
      <td>0.014465</td>
      <td>0.176371</td>
      <td>0.241530</td>
      <td>0.514724</td>
      <td>0.562208</td>
      <td>0.158963</td>
      <td>0.073715</td>
      <td>0.208463</td>
      <td>1</td>
      <td>0.056690</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.325261</td>
      <td>0.805968</td>
      <td>0.957033</td>
      <td>0.331665</td>
      <td>0.307923</td>
      <td>0.355315</td>
      <td>0.501899</td>
      <td>0.558449</td>
      <td>0.885169</td>
      <td>0.182754</td>
      <td>0</td>
      <td>-0.012945</td>
    </tr>
  </tbody>
</table>
</div>



## 1.5 时序预测

PyCaret时间序列预测Time Series模块支持多种预测方法，如ARIMA、Prophet和LSTM。它还提供了各种功能来处理缺失值、时间序列分解和数据可视化。

**数据准备**


```python
# 乘客时序数据
from pycaret.datasets import get_data
# 下载路径：https://raw.githubusercontent.com/sktime/sktime/main/sktime/datasets/data/Airline/Airline.csv
data = get_data('./datasets/airline')
# data = get_data('airline')
```


<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Date</th>
      <th>Passengers</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1949-01</td>
      <td>112</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1949-02</td>
      <td>118</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1949-03</td>
      <td>132</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1949-04</td>
      <td>129</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1949-05</td>
      <td>121</td>
    </tr>
  </tbody>
</table>
</div>



```python
import pandas as pd
data['Date'] = pd.to_datetime(data['Date'])
# 并将Date设置为列号
data.set_index('Date', inplace=True)
```


```python
from pycaret.time_series import TSForecastingExperiment
s = TSForecastingExperiment()
# fh: 用于预测的预测范围。默认值设置为1，即预测前方一点。,fold: 交叉验证中折数
s.setup(data, fh = 3, fold = 5, session_id = 0, verbose = False)
# from pycaret.time_series import *
# s = setup(data, fh = 3, fold = 5, session_id = 0)
```




    <pycaret.time_series.forecasting.oop.TSForecastingExperiment at 0x200dee26910>



**模型训练与评估**


```python
best = s.compare_models()
```






<style type="text/css">
#T_06b81 th {
  text-align: left;
}
#T_06b81_row0_col0, #T_06b81_row1_col0, #T_06b81_row1_col1, #T_06b81_row1_col2, #T_06b81_row1_col3, #T_06b81_row1_col4, #T_06b81_row1_col5, #T_06b81_row1_col6, #T_06b81_row1_col7, #T_06b81_row2_col0, #T_06b81_row2_col1, #T_06b81_row2_col2, #T_06b81_row2_col3, #T_06b81_row2_col4, #T_06b81_row2_col5, #T_06b81_row2_col6, #T_06b81_row2_col7, #T_06b81_row3_col0, #T_06b81_row3_col1, #T_06b81_row3_col2, #T_06b81_row3_col3, #T_06b81_row3_col4, #T_06b81_row3_col5, #T_06b81_row3_col6, #T_06b81_row3_col7, #T_06b81_row4_col0, #T_06b81_row4_col1, #T_06b81_row4_col2, #T_06b81_row4_col3, #T_06b81_row4_col4, #T_06b81_row4_col5, #T_06b81_row4_col6, #T_06b81_row4_col7, #T_06b81_row5_col0, #T_06b81_row5_col1, #T_06b81_row5_col2, #T_06b81_row5_col3, #T_06b81_row5_col4, #T_06b81_row5_col5, #T_06b81_row5_col6, #T_06b81_row5_col7, #T_06b81_row6_col0, #T_06b81_row6_col1, #T_06b81_row6_col2, #T_06b81_row6_col3, #T_06b81_row6_col4, #T_06b81_row6_col5, #T_06b81_row6_col6, #T_06b81_row6_col7, #T_06b81_row7_col0, #T_06b81_row7_col1, #T_06b81_row7_col2, #T_06b81_row7_col3, #T_06b81_row7_col4, #T_06b81_row7_col5, #T_06b81_row7_col6, #T_06b81_row7_col7, #T_06b81_row8_col0, #T_06b81_row8_col1, #T_06b81_row8_col2, #T_06b81_row8_col3, #T_06b81_row8_col4, #T_06b81_row8_col5, #T_06b81_row8_col6, #T_06b81_row8_col7, #T_06b81_row9_col0, #T_06b81_row9_col1, #T_06b81_row9_col2, #T_06b81_row9_col3, #T_06b81_row9_col4, #T_06b81_row9_col5, #T_06b81_row9_col6, #T_06b81_row9_col7, #T_06b81_row10_col0, #T_06b81_row10_col1, #T_06b81_row10_col2, #T_06b81_row10_col3, #T_06b81_row10_col4, #T_06b81_row10_col5, #T_06b81_row10_col6, #T_06b81_row10_col7, #T_06b81_row11_col0, #T_06b81_row11_col1, #T_06b81_row11_col2, #T_06b81_row11_col3, #T_06b81_row11_col4, #T_06b81_row11_col5, #T_06b81_row11_col6, #T_06b81_row11_col7, #T_06b81_row12_col0, #T_06b81_row12_col1, #T_06b81_row12_col2, #T_06b81_row12_col3, #T_06b81_row12_col4, #T_06b81_row12_col5, #T_06b81_row12_col6, #T_06b81_row12_col7, #T_06b81_row13_col0, #T_06b81_row13_col1, #T_06b81_row13_col2, #T_06b81_row13_col3, #T_06b81_row13_col4, #T_06b81_row13_col5, #T_06b81_row13_col6, #T_06b81_row13_col7, #T_06b81_row14_col0, #T_06b81_row14_col1, #T_06b81_row14_col2, #T_06b81_row14_col3, #T_06b81_row14_col4, #T_06b81_row14_col5, #T_06b81_row14_col6, #T_06b81_row14_col7, #T_06b81_row15_col0, #T_06b81_row15_col1, #T_06b81_row15_col2, #T_06b81_row15_col3, #T_06b81_row15_col4, #T_06b81_row15_col5, #T_06b81_row15_col6, #T_06b81_row15_col7, #T_06b81_row16_col0, #T_06b81_row16_col1, #T_06b81_row16_col2, #T_06b81_row16_col3, #T_06b81_row16_col4, #T_06b81_row16_col5, #T_06b81_row16_col6, #T_06b81_row16_col7, #T_06b81_row17_col0, #T_06b81_row17_col1, #T_06b81_row17_col2, #T_06b81_row17_col3, #T_06b81_row17_col4, #T_06b81_row17_col5, #T_06b81_row17_col6, #T_06b81_row17_col7, #T_06b81_row18_col0, #T_06b81_row18_col1, #T_06b81_row18_col2, #T_06b81_row18_col3, #T_06b81_row18_col4, #T_06b81_row18_col5, #T_06b81_row18_col6, #T_06b81_row18_col7, #T_06b81_row19_col0, #T_06b81_row19_col1, #T_06b81_row19_col2, #T_06b81_row19_col3, #T_06b81_row19_col4, #T_06b81_row19_col5, #T_06b81_row19_col6, #T_06b81_row19_col7, #T_06b81_row20_col0, #T_06b81_row20_col1, #T_06b81_row20_col2, #T_06b81_row20_col3, #T_06b81_row20_col4, #T_06b81_row20_col5, #T_06b81_row20_col6, #T_06b81_row20_col7, #T_06b81_row21_col0, #T_06b81_row21_col1, #T_06b81_row21_col2, #T_06b81_row21_col3, #T_06b81_row21_col4, #T_06b81_row21_col5, #T_06b81_row21_col6, #T_06b81_row21_col7, #T_06b81_row22_col0, #T_06b81_row22_col1, #T_06b81_row22_col2, #T_06b81_row22_col3, #T_06b81_row22_col4, #T_06b81_row22_col5, #T_06b81_row22_col6, #T_06b81_row22_col7, #T_06b81_row23_col0, #T_06b81_row23_col1, #T_06b81_row23_col2, #T_06b81_row23_col3, #T_06b81_row23_col4, #T_06b81_row23_col5, #T_06b81_row23_col6, #T_06b81_row23_col7, #T_06b81_row24_col0, #T_06b81_row24_col1, #T_06b81_row24_col2, #T_06b81_row24_col3, #T_06b81_row24_col4, #T_06b81_row24_col5, #T_06b81_row24_col6, #T_06b81_row24_col7, #T_06b81_row25_col0, #T_06b81_row25_col1, #T_06b81_row25_col2, #T_06b81_row25_col3, #T_06b81_row25_col4, #T_06b81_row25_col5, #T_06b81_row25_col6, #T_06b81_row25_col7 {
  text-align: left;
}
#T_06b81_row0_col1, #T_06b81_row0_col2, #T_06b81_row0_col3, #T_06b81_row0_col4, #T_06b81_row0_col5, #T_06b81_row0_col6, #T_06b81_row0_col7 {
  text-align: left;
  background-color: yellow;
}
#T_06b81_row0_col8, #T_06b81_row1_col8, #T_06b81_row2_col8, #T_06b81_row3_col8, #T_06b81_row4_col8, #T_06b81_row5_col8, #T_06b81_row6_col8, #T_06b81_row7_col8, #T_06b81_row8_col8, #T_06b81_row9_col8, #T_06b81_row10_col8, #T_06b81_row11_col8, #T_06b81_row12_col8, #T_06b81_row13_col8, #T_06b81_row14_col8, #T_06b81_row15_col8, #T_06b81_row16_col8, #T_06b81_row17_col8, #T_06b81_row18_col8, #T_06b81_row19_col8, #T_06b81_row20_col8, #T_06b81_row21_col8, #T_06b81_row22_col8, #T_06b81_row23_col8, #T_06b81_row25_col8 {
  text-align: left;
  background-color: lightgrey;
}
#T_06b81_row24_col8 {
  text-align: left;
  background-color: yellow;
  background-color: lightgrey;
}
</style>
<table id="T_06b81">
  <thead>
    <tr>
      <th class="blank level0" >&nbsp;</th>
      <th id="T_06b81_level0_col0" class="col_heading level0 col0" >Model</th>
      <th id="T_06b81_level0_col1" class="col_heading level0 col1" >MASE</th>
      <th id="T_06b81_level0_col2" class="col_heading level0 col2" >RMSSE</th>
      <th id="T_06b81_level0_col3" class="col_heading level0 col3" >MAE</th>
      <th id="T_06b81_level0_col4" class="col_heading level0 col4" >RMSE</th>
      <th id="T_06b81_level0_col5" class="col_heading level0 col5" >MAPE</th>
      <th id="T_06b81_level0_col6" class="col_heading level0 col6" >SMAPE</th>
      <th id="T_06b81_level0_col7" class="col_heading level0 col7" >R2</th>
      <th id="T_06b81_level0_col8" class="col_heading level0 col8" >TT (Sec)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th id="T_06b81_level0_row0" class="row_heading level0 row0" >stlf</th>
      <td id="T_06b81_row0_col0" class="data row0 col0" >STLF</td>
      <td id="T_06b81_row0_col1" class="data row0 col1" >0.4240</td>
      <td id="T_06b81_row0_col2" class="data row0 col2" >0.4429</td>
      <td id="T_06b81_row0_col3" class="data row0 col3" >12.8002</td>
      <td id="T_06b81_row0_col4" class="data row0 col4" >15.1933</td>
      <td id="T_06b81_row0_col5" class="data row0 col5" >0.0266</td>
      <td id="T_06b81_row0_col6" class="data row0 col6" >0.0268</td>
      <td id="T_06b81_row0_col7" class="data row0 col7" >0.4296</td>
      <td id="T_06b81_row0_col8" class="data row0 col8" >0.0300</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row1" class="row_heading level0 row1" >exp_smooth</th>
      <td id="T_06b81_row1_col0" class="data row1 col0" >Exponential Smoothing</td>
      <td id="T_06b81_row1_col1" class="data row1 col1" >0.5063</td>
      <td id="T_06b81_row1_col2" class="data row1 col2" >0.5378</td>
      <td id="T_06b81_row1_col3" class="data row1 col3" >15.2900</td>
      <td id="T_06b81_row1_col4" class="data row1 col4" >18.4455</td>
      <td id="T_06b81_row1_col5" class="data row1 col5" >0.0334</td>
      <td id="T_06b81_row1_col6" class="data row1 col6" >0.0335</td>
      <td id="T_06b81_row1_col7" class="data row1 col7" >-0.0521</td>
      <td id="T_06b81_row1_col8" class="data row1 col8" >0.0500</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row2" class="row_heading level0 row2" >ets</th>
      <td id="T_06b81_row2_col0" class="data row2 col0" >ETS</td>
      <td id="T_06b81_row2_col1" class="data row2 col1" >0.5520</td>
      <td id="T_06b81_row2_col2" class="data row2 col2" >0.5801</td>
      <td id="T_06b81_row2_col3" class="data row2 col3" >16.6164</td>
      <td id="T_06b81_row2_col4" class="data row2 col4" >19.8391</td>
      <td id="T_06b81_row2_col5" class="data row2 col5" >0.0354</td>
      <td id="T_06b81_row2_col6" class="data row2 col6" >0.0357</td>
      <td id="T_06b81_row2_col7" class="data row2 col7" >-0.0740</td>
      <td id="T_06b81_row2_col8" class="data row2 col8" >0.0680</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row3" class="row_heading level0 row3" >arima</th>
      <td id="T_06b81_row3_col0" class="data row3 col0" >ARIMA</td>
      <td id="T_06b81_row3_col1" class="data row3 col1" >0.6480</td>
      <td id="T_06b81_row3_col2" class="data row3 col2" >0.6501</td>
      <td id="T_06b81_row3_col3" class="data row3 col3" >19.5728</td>
      <td id="T_06b81_row3_col4" class="data row3 col4" >22.3027</td>
      <td id="T_06b81_row3_col5" class="data row3 col5" >0.0412</td>
      <td id="T_06b81_row3_col6" class="data row3 col6" >0.0420</td>
      <td id="T_06b81_row3_col7" class="data row3 col7" >-0.0796</td>
      <td id="T_06b81_row3_col8" class="data row3 col8" >0.0420</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row4" class="row_heading level0 row4" >auto_arima</th>
      <td id="T_06b81_row4_col0" class="data row4 col0" >Auto ARIMA</td>
      <td id="T_06b81_row4_col1" class="data row4 col1" >0.6526</td>
      <td id="T_06b81_row4_col2" class="data row4 col2" >0.6300</td>
      <td id="T_06b81_row4_col3" class="data row4 col3" >19.7405</td>
      <td id="T_06b81_row4_col4" class="data row4 col4" >21.6202</td>
      <td id="T_06b81_row4_col5" class="data row4 col5" >0.0414</td>
      <td id="T_06b81_row4_col6" class="data row4 col6" >0.0421</td>
      <td id="T_06b81_row4_col7" class="data row4 col7" >-0.0560</td>
      <td id="T_06b81_row4_col8" class="data row4 col8" >10.5220</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row5" class="row_heading level0 row5" >theta</th>
      <td id="T_06b81_row5_col0" class="data row5 col0" >Theta Forecaster</td>
      <td id="T_06b81_row5_col1" class="data row5 col1" >0.8458</td>
      <td id="T_06b81_row5_col2" class="data row5 col2" >0.8223</td>
      <td id="T_06b81_row5_col3" class="data row5 col3" >25.7024</td>
      <td id="T_06b81_row5_col4" class="data row5 col4" >28.3332</td>
      <td id="T_06b81_row5_col5" class="data row5 col5" >0.0524</td>
      <td id="T_06b81_row5_col6" class="data row5 col6" >0.0541</td>
      <td id="T_06b81_row5_col7" class="data row5 col7" >-0.7710</td>
      <td id="T_06b81_row5_col8" class="data row5 col8" >0.0220</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row6" class="row_heading level0 row6" >huber_cds_dt</th>
      <td id="T_06b81_row6_col0" class="data row6 col0" >Huber w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row6_col1" class="data row6 col1" >0.9002</td>
      <td id="T_06b81_row6_col2" class="data row6 col2" >0.8900</td>
      <td id="T_06b81_row6_col3" class="data row6 col3" >27.2568</td>
      <td id="T_06b81_row6_col4" class="data row6 col4" >30.5782</td>
      <td id="T_06b81_row6_col5" class="data row6 col5" >0.0550</td>
      <td id="T_06b81_row6_col6" class="data row6 col6" >0.0572</td>
      <td id="T_06b81_row6_col7" class="data row6 col7" >-0.0309</td>
      <td id="T_06b81_row6_col8" class="data row6 col8" >0.0680</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row7" class="row_heading level0 row7" >knn_cds_dt</th>
      <td id="T_06b81_row7_col0" class="data row7 col0" >K Neighbors w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row7_col1" class="data row7 col1" >0.9381</td>
      <td id="T_06b81_row7_col2" class="data row7 col2" >0.8830</td>
      <td id="T_06b81_row7_col3" class="data row7 col3" >28.5678</td>
      <td id="T_06b81_row7_col4" class="data row7 col4" >30.5007</td>
      <td id="T_06b81_row7_col5" class="data row7 col5" >0.0555</td>
      <td id="T_06b81_row7_col6" class="data row7 col6" >0.0575</td>
      <td id="T_06b81_row7_col7" class="data row7 col7" >0.0908</td>
      <td id="T_06b81_row7_col8" class="data row7 col8" >0.0920</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row8" class="row_heading level0 row8" >lr_cds_dt</th>
      <td id="T_06b81_row8_col0" class="data row8 col0" >Linear w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row8_col1" class="data row8 col1" >0.9469</td>
      <td id="T_06b81_row8_col2" class="data row8 col2" >0.9297</td>
      <td id="T_06b81_row8_col3" class="data row8 col3" >28.6337</td>
      <td id="T_06b81_row8_col4" class="data row8 col4" >31.9163</td>
      <td id="T_06b81_row8_col5" class="data row8 col5" >0.0581</td>
      <td id="T_06b81_row8_col6" class="data row8 col6" >0.0605</td>
      <td id="T_06b81_row8_col7" class="data row8 col7" >-0.1620</td>
      <td id="T_06b81_row8_col8" class="data row8 col8" >0.0820</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row9" class="row_heading level0 row9" >ridge_cds_dt</th>
      <td id="T_06b81_row9_col0" class="data row9 col0" >Ridge w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row9_col1" class="data row9 col1" >0.9469</td>
      <td id="T_06b81_row9_col2" class="data row9 col2" >0.9297</td>
      <td id="T_06b81_row9_col3" class="data row9 col3" >28.6340</td>
      <td id="T_06b81_row9_col4" class="data row9 col4" >31.9164</td>
      <td id="T_06b81_row9_col5" class="data row9 col5" >0.0581</td>
      <td id="T_06b81_row9_col6" class="data row9 col6" >0.0605</td>
      <td id="T_06b81_row9_col7" class="data row9 col7" >-0.1620</td>
      <td id="T_06b81_row9_col8" class="data row9 col8" >0.0680</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row10" class="row_heading level0 row10" >en_cds_dt</th>
      <td id="T_06b81_row10_col0" class="data row10 col0" >Elastic Net w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row10_col1" class="data row10 col1" >0.9499</td>
      <td id="T_06b81_row10_col2" class="data row10 col2" >0.9320</td>
      <td id="T_06b81_row10_col3" class="data row10 col3" >28.7271</td>
      <td id="T_06b81_row10_col4" class="data row10 col4" >31.9952</td>
      <td id="T_06b81_row10_col5" class="data row10 col5" >0.0582</td>
      <td id="T_06b81_row10_col6" class="data row10 col6" >0.0606</td>
      <td id="T_06b81_row10_col7" class="data row10 col7" >-0.1579</td>
      <td id="T_06b81_row10_col8" class="data row10 col8" >0.0700</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row11" class="row_heading level0 row11" >llar_cds_dt</th>
      <td id="T_06b81_row11_col0" class="data row11 col0" >Lasso Least Angular Regressor w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row11_col1" class="data row11 col1" >0.9520</td>
      <td id="T_06b81_row11_col2" class="data row11 col2" >0.9336</td>
      <td id="T_06b81_row11_col3" class="data row11 col3" >28.7917</td>
      <td id="T_06b81_row11_col4" class="data row11 col4" >32.0528</td>
      <td id="T_06b81_row11_col5" class="data row11 col5" >0.0583</td>
      <td id="T_06b81_row11_col6" class="data row11 col6" >0.0607</td>
      <td id="T_06b81_row11_col7" class="data row11 col7" >-0.1559</td>
      <td id="T_06b81_row11_col8" class="data row11 col8" >0.0560</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row12" class="row_heading level0 row12" >lasso_cds_dt</th>
      <td id="T_06b81_row12_col0" class="data row12 col0" >Lasso w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row12_col1" class="data row12 col1" >0.9521</td>
      <td id="T_06b81_row12_col2" class="data row12 col2" >0.9337</td>
      <td id="T_06b81_row12_col3" class="data row12 col3" >28.7941</td>
      <td id="T_06b81_row12_col4" class="data row12 col4" >32.0557</td>
      <td id="T_06b81_row12_col5" class="data row12 col5" >0.0583</td>
      <td id="T_06b81_row12_col6" class="data row12 col6" >0.0607</td>
      <td id="T_06b81_row12_col7" class="data row12 col7" >-0.1560</td>
      <td id="T_06b81_row12_col8" class="data row12 col8" >0.0720</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row13" class="row_heading level0 row13" >br_cds_dt</th>
      <td id="T_06b81_row13_col0" class="data row13 col0" >Bayesian Ridge w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row13_col1" class="data row13 col1" >0.9551</td>
      <td id="T_06b81_row13_col2" class="data row13 col2" >0.9347</td>
      <td id="T_06b81_row13_col3" class="data row13 col3" >28.9018</td>
      <td id="T_06b81_row13_col4" class="data row13 col4" >32.1013</td>
      <td id="T_06b81_row13_col5" class="data row13 col5" >0.0582</td>
      <td id="T_06b81_row13_col6" class="data row13 col6" >0.0606</td>
      <td id="T_06b81_row13_col7" class="data row13 col7" >-0.1377</td>
      <td id="T_06b81_row13_col8" class="data row13 col8" >0.0580</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row14" class="row_heading level0 row14" >et_cds_dt</th>
      <td id="T_06b81_row14_col0" class="data row14 col0" >Extra Trees w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row14_col1" class="data row14 col1" >1.0322</td>
      <td id="T_06b81_row14_col2" class="data row14 col2" >0.9942</td>
      <td id="T_06b81_row14_col3" class="data row14 col3" >31.4048</td>
      <td id="T_06b81_row14_col4" class="data row14 col4" >34.3054</td>
      <td id="T_06b81_row14_col5" class="data row14 col5" >0.0607</td>
      <td id="T_06b81_row14_col6" class="data row14 col6" >0.0633</td>
      <td id="T_06b81_row14_col7" class="data row14 col7" >-0.1660</td>
      <td id="T_06b81_row14_col8" class="data row14 col8" >0.1280</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row15" class="row_heading level0 row15" >rf_cds_dt</th>
      <td id="T_06b81_row15_col0" class="data row15 col0" >Random Forest w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row15_col1" class="data row15 col1" >1.0851</td>
      <td id="T_06b81_row15_col2" class="data row15 col2" >1.0286</td>
      <td id="T_06b81_row15_col3" class="data row15 col3" >32.9791</td>
      <td id="T_06b81_row15_col4" class="data row15 col4" >35.4666</td>
      <td id="T_06b81_row15_col5" class="data row15 col5" >0.0641</td>
      <td id="T_06b81_row15_col6" class="data row15 col6" >0.0670</td>
      <td id="T_06b81_row15_col7" class="data row15 col7" >-0.3545</td>
      <td id="T_06b81_row15_col8" class="data row15 col8" >0.1400</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row16" class="row_heading level0 row16" >lightgbm_cds_dt</th>
      <td id="T_06b81_row16_col0" class="data row16 col0" >Light Gradient Boosting w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row16_col1" class="data row16 col1" >1.1409</td>
      <td id="T_06b81_row16_col2" class="data row16 col2" >1.1040</td>
      <td id="T_06b81_row16_col3" class="data row16 col3" >34.5999</td>
      <td id="T_06b81_row16_col4" class="data row16 col4" >37.9918</td>
      <td id="T_06b81_row16_col5" class="data row16 col5" >0.0670</td>
      <td id="T_06b81_row16_col6" class="data row16 col6" >0.0701</td>
      <td id="T_06b81_row16_col7" class="data row16 col7" >-0.3994</td>
      <td id="T_06b81_row16_col8" class="data row16 col8" >0.0900</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row17" class="row_heading level0 row17" >ada_cds_dt</th>
      <td id="T_06b81_row17_col0" class="data row17 col0" >AdaBoost w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row17_col1" class="data row17 col1" >1.1441</td>
      <td id="T_06b81_row17_col2" class="data row17 col2" >1.0843</td>
      <td id="T_06b81_row17_col3" class="data row17 col3" >34.7451</td>
      <td id="T_06b81_row17_col4" class="data row17 col4" >37.3681</td>
      <td id="T_06b81_row17_col5" class="data row17 col5" >0.0664</td>
      <td id="T_06b81_row17_col6" class="data row17 col6" >0.0697</td>
      <td id="T_06b81_row17_col7" class="data row17 col7" >-0.3004</td>
      <td id="T_06b81_row17_col8" class="data row17 col8" >0.0920</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row18" class="row_heading level0 row18" >gbr_cds_dt</th>
      <td id="T_06b81_row18_col0" class="data row18 col0" >Gradient Boosting w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row18_col1" class="data row18 col1" >1.1697</td>
      <td id="T_06b81_row18_col2" class="data row18 col2" >1.1094</td>
      <td id="T_06b81_row18_col3" class="data row18 col3" >35.4408</td>
      <td id="T_06b81_row18_col4" class="data row18 col4" >38.1373</td>
      <td id="T_06b81_row18_col5" class="data row18 col5" >0.0697</td>
      <td id="T_06b81_row18_col6" class="data row18 col6" >0.0729</td>
      <td id="T_06b81_row18_col7" class="data row18 col7" >-0.4163</td>
      <td id="T_06b81_row18_col8" class="data row18 col8" >0.0900</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row19" class="row_heading level0 row19" >omp_cds_dt</th>
      <td id="T_06b81_row19_col0" class="data row19 col0" >Orthogonal Matching Pursuit w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row19_col1" class="data row19 col1" >1.1793</td>
      <td id="T_06b81_row19_col2" class="data row19 col2" >1.1250</td>
      <td id="T_06b81_row19_col3" class="data row19 col3" >35.7348</td>
      <td id="T_06b81_row19_col4" class="data row19 col4" >38.6755</td>
      <td id="T_06b81_row19_col5" class="data row19 col5" >0.0706</td>
      <td id="T_06b81_row19_col6" class="data row19 col6" >0.0732</td>
      <td id="T_06b81_row19_col7" class="data row19 col7" >-0.5095</td>
      <td id="T_06b81_row19_col8" class="data row19 col8" >0.0620</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row20" class="row_heading level0 row20" >dt_cds_dt</th>
      <td id="T_06b81_row20_col0" class="data row20 col0" >Decision Tree w/ Cond. Deseasonalize & Detrending</td>
      <td id="T_06b81_row20_col1" class="data row20 col1" >1.2704</td>
      <td id="T_06b81_row20_col2" class="data row20 col2" >1.2371</td>
      <td id="T_06b81_row20_col3" class="data row20 col3" >38.4976</td>
      <td id="T_06b81_row20_col4" class="data row20 col4" >42.4846</td>
      <td id="T_06b81_row20_col5" class="data row20 col5" >0.0773</td>
      <td id="T_06b81_row20_col6" class="data row20 col6" >0.0814</td>
      <td id="T_06b81_row20_col7" class="data row20 col7" >-1.0382</td>
      <td id="T_06b81_row20_col8" class="data row20 col8" >0.0860</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row21" class="row_heading level0 row21" >snaive</th>
      <td id="T_06b81_row21_col0" class="data row21 col0" >Seasonal Naive Forecaster</td>
      <td id="T_06b81_row21_col1" class="data row21 col1" >1.7700</td>
      <td id="T_06b81_row21_col2" class="data row21 col2" >1.5999</td>
      <td id="T_06b81_row21_col3" class="data row21 col3" >53.5333</td>
      <td id="T_06b81_row21_col4" class="data row21 col4" >54.9143</td>
      <td id="T_06b81_row21_col5" class="data row21 col5" >0.1136</td>
      <td id="T_06b81_row21_col6" class="data row21 col6" >0.1211</td>
      <td id="T_06b81_row21_col7" class="data row21 col7" >-4.1630</td>
      <td id="T_06b81_row21_col8" class="data row21 col8" >0.1580</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row22" class="row_heading level0 row22" >naive</th>
      <td id="T_06b81_row22_col0" class="data row22 col0" >Naive Forecaster</td>
      <td id="T_06b81_row22_col1" class="data row22 col1" >1.8145</td>
      <td id="T_06b81_row22_col2" class="data row22 col2" >1.7444</td>
      <td id="T_06b81_row22_col3" class="data row22 col3" >54.8667</td>
      <td id="T_06b81_row22_col4" class="data row22 col4" >59.8160</td>
      <td id="T_06b81_row22_col5" class="data row22 col5" >0.1135</td>
      <td id="T_06b81_row22_col6" class="data row22 col6" >0.1151</td>
      <td id="T_06b81_row22_col7" class="data row22 col7" >-3.7710</td>
      <td id="T_06b81_row22_col8" class="data row22 col8" >0.1460</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row23" class="row_heading level0 row23" >polytrend</th>
      <td id="T_06b81_row23_col0" class="data row23 col0" >Polynomial Trend Forecaster</td>
      <td id="T_06b81_row23_col1" class="data row23 col1" >2.3154</td>
      <td id="T_06b81_row23_col2" class="data row23 col2" >2.2507</td>
      <td id="T_06b81_row23_col3" class="data row23 col3" >70.1138</td>
      <td id="T_06b81_row23_col4" class="data row23 col4" >77.3400</td>
      <td id="T_06b81_row23_col5" class="data row23 col5" >0.1363</td>
      <td id="T_06b81_row23_col6" class="data row23 col6" >0.1468</td>
      <td id="T_06b81_row23_col7" class="data row23 col7" >-4.6202</td>
      <td id="T_06b81_row23_col8" class="data row23 col8" >0.1080</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row24" class="row_heading level0 row24" >croston</th>
      <td id="T_06b81_row24_col0" class="data row24 col0" >Croston</td>
      <td id="T_06b81_row24_col1" class="data row24 col1" >2.6211</td>
      <td id="T_06b81_row24_col2" class="data row24 col2" >2.4985</td>
      <td id="T_06b81_row24_col3" class="data row24 col3" >79.3645</td>
      <td id="T_06b81_row24_col4" class="data row24 col4" >85.8439</td>
      <td id="T_06b81_row24_col5" class="data row24 col5" >0.1515</td>
      <td id="T_06b81_row24_col6" class="data row24 col6" >0.1684</td>
      <td id="T_06b81_row24_col7" class="data row24 col7" >-5.2294</td>
      <td id="T_06b81_row24_col8" class="data row24 col8" >0.0140</td>
    </tr>
    <tr>
      <th id="T_06b81_level0_row25" class="row_heading level0 row25" >grand_means</th>
      <td id="T_06b81_row25_col0" class="data row25 col0" >Grand Means Forecaster</td>
      <td id="T_06b81_row25_col1" class="data row25 col1" >7.1261</td>
      <td id="T_06b81_row25_col2" class="data row25 col2" >6.3506</td>
      <td id="T_06b81_row25_col3" class="data row25 col3" >216.0214</td>
      <td id="T_06b81_row25_col4" class="data row25 col4" >218.4259</td>
      <td id="T_06b81_row25_col5" class="data row25 col5" >0.4377</td>
      <td id="T_06b81_row25_col6" class="data row25 col6" >0.5682</td>
      <td id="T_06b81_row25_col7" class="data row25 col7" >-59.2684</td>
      <td id="T_06b81_row25_col8" class="data row25 col8" >0.1400</td>
    </tr>
  </tbody>
</table>







**数据展示**


```python
# jupyter环境下交互可视化展示
# plot参数支持：
# - 'ts' - Time Series Plot
# - 'train_test_split' - Train Test Split
# - 'cv' - Cross Validation
# - 'acf' - Auto Correlation (ACF)
# - 'pacf' - Partial Auto Correlation (PACF)
# - 'decomp' - Classical Decomposition
# - 'decomp_stl' - STL Decomposition
# - 'diagnostics' - Diagnostics Plot
# - 'diff' - Difference Plot
# - 'periodogram' - Frequency Components (Periodogram)
# - 'fft' - Frequency Components (FFT)
# - 'ccf' - Cross Correlation (CCF)
# - 'forecast' - "Out-of-Sample" Forecast Plot
# - 'insample' - "In-Sample" Forecast Plot
# - 'residuals' - Residuals Plot
# s.plot_model(best, plot = 'forecast', data_kwargs = {'fh' : 24})
```

**数据预测**


```python
# 使模型拟合包括测试样本在内的完整数据集
final_best = s.finalize_model(best)
s.predict_model(best, fh = 24)
s.save_model(final_best, 'final_best_model')
```

    Transformation Pipeline and Model Successfully Saved
    




    (ForecastingPipeline(steps=[('forecaster',
                                 TransformedTargetForecaster(steps=[('model',
                                                                     ForecastingPipeline(steps=[('forecaster',
                                                                                                 TransformedTargetForecaster(steps=[('model',
                                                                                                                                     STLForecaster(sp=12))]))]))]))]),
     'final_best_model.pkl')



# 2 数据处理与清洗

## 2.1 缺失值处理

各种数据集可能由于多种原因存在缺失值或空记录。移除具有缺失值的样本是一种常见策略，但这会导致丢失可能有价值的数据。一种可替代的策略是对缺失值进行插值填充。在setup函数中可以指定如下参数，实现缺失值处理：

+ imputation_type：取值可以是 'simple' 或 'iterative'或 None。当imputation_type设置为 'simple' 时，PyCaret 将使用简单的方式（numeric_imputation和categorical_imputation）对缺失值进行填充。而当设置为 'iterative' 时，则会使用模型估计的方式（numeric_iterative_imputer，categorical_iterative_imputer）进行填充处理。如果设置为 None，则不会执行任何缺失值填充操作
+ numeric_imputation: 设置数值类型的缺失值，方式如下：
    + mean: 用列的平均值填充，默认
    + drop: 删除包含缺失值的行
    + median: 用列的中值填充
    + mode: 用列最常见值填充
    + knn: 使用K-最近邻方法拟合
    + int or float: 用指定值替代
+ categorical_imputation:
    + mode: 用列最常见值填充，默认
    + drop: 删除包含缺失值的行
    + str: 用指定字符替代
+ numeric_iterative_imputer: 使用估计模型拟合值，可输入str或sklearn模型, 默认使用lightgbm
+ categorical_iterative_imputer: 使用估计模型差值，可输入str或sklearn模型, 默认使用lightgbm

**加载数据**


```python
# load dataset
from pycaret.datasets import get_data
# 从本地加载数据，注意dataset是数据的文件名
data = get_data(dataset='./datasets/hepatitis', verbose=False)
# data = get_data('hepatitis',verbose=False)
# 可以看到第三行STEROID列出现NaN值
data.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Class</th>
      <th>AGE</th>
      <th>SEX</th>
      <th>STEROID</th>
      <th>ANTIVIRALS</th>
      <th>FATIGUE</th>
      <th>MALAISE</th>
      <th>ANOREXIA</th>
      <th>LIVER BIG</th>
      <th>LIVER FIRM</th>
      <th>SPLEEN PALPABLE</th>
      <th>SPIDERS</th>
      <th>ASCITES</th>
      <th>VARICES</th>
      <th>BILIRUBIN</th>
      <th>ALK PHOSPHATE</th>
      <th>SGOT</th>
      <th>ALBUMIN</th>
      <th>PROTIME</th>
      <th>HISTOLOGY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>30</td>
      <td>2</td>
      <td>1.0</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>85.0</td>
      <td>18.0</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>50</td>
      <td>1</td>
      <td>1.0</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.9</td>
      <td>135.0</td>
      <td>42.0</td>
      <td>3.5</td>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0</td>
      <td>78</td>
      <td>1</td>
      <td>2.0</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>96.0</td>
      <td>32.0</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>31</td>
      <td>1</td>
      <td>NaN</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>46.0</td>
      <td>52.0</td>
      <td>4.0</td>
      <td>80.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>34</td>
      <td>1</td>
      <td>2.0</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>200.0</td>
      <td>4.0</td>
      <td>NaN</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 使用均值填充数据
from pycaret.classification import ClassificationExperiment
s = ClassificationExperiment()
# 均值
# s.data['STEROID'].mean()
s.setup(data = data, session_id=0, target = 'Class',verbose=False, 
        # 设置data_split_shuffle和data_split_stratify为False不打乱数据
        data_split_shuffle = False, data_split_stratify = False,
        imputation_type='simple', numeric_iterative_imputer='drop')
# 查看转换后的数据
s.get_config('dataset_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>AGE</th>
      <th>SEX</th>
      <th>STEROID</th>
      <th>ANTIVIRALS</th>
      <th>FATIGUE</th>
      <th>MALAISE</th>
      <th>ANOREXIA</th>
      <th>LIVER BIG</th>
      <th>LIVER FIRM</th>
      <th>SPLEEN PALPABLE</th>
      <th>SPIDERS</th>
      <th>ASCITES</th>
      <th>VARICES</th>
      <th>BILIRUBIN</th>
      <th>ALK PHOSPHATE</th>
      <th>SGOT</th>
      <th>ALBUMIN</th>
      <th>PROTIME</th>
      <th>HISTOLOGY</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>30.0</td>
      <td>2.0</td>
      <td>1.000000</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>85.000000</td>
      <td>18.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>50.0</td>
      <td>1.0</td>
      <td>1.000000</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.9</td>
      <td>135.000000</td>
      <td>42.0</td>
      <td>3.5</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>78.0</td>
      <td>1.0</td>
      <td>2.000000</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>96.000000</td>
      <td>32.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>31.0</td>
      <td>1.0</td>
      <td>1.509434</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>46.000000</td>
      <td>52.0</td>
      <td>4.0</td>
      <td>80.00000</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>34.0</td>
      <td>1.0</td>
      <td>2.000000</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>99.659088</td>
      <td>200.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 使用knn拟合数据
from pycaret.classification import ClassificationExperiment
s = ClassificationExperiment()
s.setup(data = data, session_id=0, target = 'Class',verbose=False, 
        # 设置data_split_shuffle和data_split_stratify为False不打乱数据
        data_split_shuffle = False, data_split_stratify = False,
        imputation_type='simple', numeric_imputation = 'knn')
# 查看转换后的数据
s.get_config('dataset_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>AGE</th>
      <th>SEX</th>
      <th>STEROID</th>
      <th>ANTIVIRALS</th>
      <th>FATIGUE</th>
      <th>MALAISE</th>
      <th>ANOREXIA</th>
      <th>LIVER BIG</th>
      <th>LIVER FIRM</th>
      <th>SPLEEN PALPABLE</th>
      <th>SPIDERS</th>
      <th>ASCITES</th>
      <th>VARICES</th>
      <th>BILIRUBIN</th>
      <th>ALK PHOSPHATE</th>
      <th>SGOT</th>
      <th>ALBUMIN</th>
      <th>PROTIME</th>
      <th>HISTOLOGY</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>30.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>85.000000</td>
      <td>18.0</td>
      <td>4.0</td>
      <td>91.800003</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>50.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.9</td>
      <td>135.000000</td>
      <td>42.0</td>
      <td>3.5</td>
      <td>61.599998</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>78.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>96.000000</td>
      <td>32.0</td>
      <td>4.0</td>
      <td>75.800003</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>31.0</td>
      <td>1.0</td>
      <td>1.8</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>46.000000</td>
      <td>52.0</td>
      <td>4.0</td>
      <td>80.000000</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>34.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>108.400002</td>
      <td>200.0</td>
      <td>4.0</td>
      <td>62.799999</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 使用lightgbmn拟合数据
# from pycaret.classification import ClassificationExperiment
# s = ClassificationExperiment()
# s.setup(data = data, session_id=0, target = 'Class',verbose=False, 
#         # 设置data_split_shuffle和data_split_stratify为False不打乱数据
#         data_split_shuffle = False, data_split_stratify = False,
#         imputation_type='iterative', numeric_iterative_imputer = 'lightgbm')
# 查看转换后的数据
# s.get_config('dataset_transformed').head()
```

## 2.2 类型转换

虽然 PyCaret具有自动识别特征类型的功能，但PyCaret提供了数据类型自定义参数，用户可以对数据集进行更精细的控制和指导，以确保模型训练和特征工程的效果更加符合用户的预期和需求。这些自定义参数如下：

+ numeric_features：用于指定数据集中的数值特征列的参数。这些特征将被视为连续型变量进行处理
+ categorical_features：用于指定数据集中的分类特征列的参数。这些特征将被视为离散型变量进行处理
+ date_features：用于指定数据集中的日期特征列的参数。这些特征将被视为日期型变量进行处理
+ create_date_columns：用于指定是否从日期特征中创建新的日期相关列的参数
+ text_features：用于指定数据集中的文本特征列的参数。这些特征将被视为文本型变量进行处理
+ text_features_method：用于指定对文本特征进行处理的方法的参数
+ ignore_features：用于指定在建模过程中需要忽略的特征列的参数
+ keep_features：用于指定在建模过程中需要保留的特征列的参数


```python
# 转换变量类型
from pycaret.datasets import get_data
data = get_data(dataset='./datasets/hepatitis', verbose=False)

from pycaret.classification import *
s = setup(data = data, target = 'Class', ignore_features  = ['SEX','AGE'], categorical_features=['STEROID'],verbose = False,
         data_split_shuffle = False, data_split_stratify = False)
```


```python
# 查看转换后的数据，前两列消失，STEROID变为分类变量
s.get_config('dataset_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>STEROID</th>
      <th>ANTIVIRALS</th>
      <th>FATIGUE</th>
      <th>MALAISE</th>
      <th>ANOREXIA</th>
      <th>LIVER BIG</th>
      <th>LIVER FIRM</th>
      <th>SPLEEN PALPABLE</th>
      <th>SPIDERS</th>
      <th>ASCITES</th>
      <th>VARICES</th>
      <th>BILIRUBIN</th>
      <th>ALK PHOSPHATE</th>
      <th>SGOT</th>
      <th>ALBUMIN</th>
      <th>PROTIME</th>
      <th>HISTOLOGY</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>85.000000</td>
      <td>18.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.9</td>
      <td>135.000000</td>
      <td>42.0</td>
      <td>3.5</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>96.000000</td>
      <td>32.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.0</td>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>0.7</td>
      <td>46.000000</td>
      <td>52.0</td>
      <td>4.0</td>
      <td>80.00000</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>2.0</td>
      <td>1.0</td>
      <td>99.659088</td>
      <td>200.0</td>
      <td>4.0</td>
      <td>66.53968</td>
      <td>1.0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



## 2.3 独热编码

当数据集中包含分类变量时，这些变量通常需要转换为模型可以理解的数值形式。独热编码是一种常用的方法，它将每个分类变量转换为一组二进制变量，其中每个变量对应一个可能的分类值，并且只有一个变量在任何给定时间点上取值为 1，其余变量均为 0。可以通过传递参数categorical_features来指定要进行独热编码的列。例如：


```python
# load dataset
from pycaret.datasets import get_data
data = get_data(dataset='./datasets/pokemon', verbose=False)
# data = get_data('pokemon')
data.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1</th>
      <th>Type 2</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
      <th>Legendary</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Bulbasaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>318</td>
      <td>45</td>
      <td>49</td>
      <td>49</td>
      <td>65</td>
      <td>65</td>
      <td>45</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Ivysaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>405</td>
      <td>60</td>
      <td>62</td>
      <td>63</td>
      <td>80</td>
      <td>80</td>
      <td>60</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>525</td>
      <td>80</td>
      <td>82</td>
      <td>83</td>
      <td>100</td>
      <td>100</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>VenusaurMega Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>625</td>
      <td>80</td>
      <td>100</td>
      <td>123</td>
      <td>122</td>
      <td>120</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Charmander</td>
      <td>Fire</td>
      <td>NaN</td>
      <td>309</td>
      <td>39</td>
      <td>52</td>
      <td>43</td>
      <td>60</td>
      <td>50</td>
      <td>65</td>
      <td>1</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 对Type 1实现独热编码
len(set(data['Type 1']))
```




    18




```python
from pycaret.classification import *
s = setup(data = data, categorical_features =["Type 1"],target = 'Legendary', verbose=False)
# 查看转换后的数据Type 1变为独热编码
s.get_config('dataset_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1_Grass</th>
      <th>Type 1_Ghost</th>
      <th>Type 1_Water</th>
      <th>Type 1_Steel</th>
      <th>Type 1_Psychic</th>
      <th>Type 1_Fire</th>
      <th>Type 1_Poison</th>
      <th>Type 1_Fairy</th>
      <th>...</th>
      <th>Type 2</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
      <th>Legendary</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>202</th>
      <td>187.0</td>
      <td>Hoppip</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>Flying</td>
      <td>250.0</td>
      <td>35.0</td>
      <td>35.0</td>
      <td>40.0</td>
      <td>35.0</td>
      <td>55.0</td>
      <td>50.0</td>
      <td>2.0</td>
      <td>False</td>
    </tr>
    <tr>
      <th>477</th>
      <td>429.0</td>
      <td>Mismagius</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>NaN</td>
      <td>495.0</td>
      <td>60.0</td>
      <td>60.0</td>
      <td>60.0</td>
      <td>105.0</td>
      <td>105.0</td>
      <td>105.0</td>
      <td>4.0</td>
      <td>False</td>
    </tr>
    <tr>
      <th>349</th>
      <td>319.0</td>
      <td>SharpedoMega Sharpedo</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>Dark</td>
      <td>560.0</td>
      <td>70.0</td>
      <td>140.0</td>
      <td>70.0</td>
      <td>110.0</td>
      <td>65.0</td>
      <td>105.0</td>
      <td>3.0</td>
      <td>False</td>
    </tr>
    <tr>
      <th>777</th>
      <td>707.0</td>
      <td>Klefki</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>Fairy</td>
      <td>470.0</td>
      <td>57.0</td>
      <td>80.0</td>
      <td>91.0</td>
      <td>80.0</td>
      <td>87.0</td>
      <td>75.0</td>
      <td>6.0</td>
      <td>False</td>
    </tr>
    <tr>
      <th>50</th>
      <td>45.0</td>
      <td>Vileplume</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>Poison</td>
      <td>490.0</td>
      <td>75.0</td>
      <td>80.0</td>
      <td>85.0</td>
      <td>110.0</td>
      <td>90.0</td>
      <td>50.0</td>
      <td>1.0</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 30 columns</p>
</div>



## 2.4 数据平衡

在 PyCaret 中，fix_imbalance 和 fix_imbalance_method 是用于处理不平衡数据集的两个参数。这些参数通常用于在训练模型之前对数据集进行预处理，以解决类别不平衡问题。

+ fix_imbalance 参数：这是一个布尔值参数，用于指示是否对不平衡数据集进行处理。当设置为 True 时，PyCaret 将自动检测数据集中的类别不平衡问题，并尝试通过采样方法来解决。当设置为 False 时，PyCaret 将使用原始的不平衡数据集进行模型训练
+ fix_imbalance_method 参数：这是一个字符串参数，用于指定处理不平衡数据集的方法。可选的值包括：
    - 使用 SMOTE（Synthetic Minority Over-sampling Technique）来生成人工合成样本，从而平衡类别（默认参数smote）
    - 使用[imbalanced-learn](https://imbalanced-learn.org/stable/)提供的估算模型


```python
# 加载数据
from pycaret.datasets import get_data
data = get_data(dataset='./datasets/credit', verbose=False)
# data = get_data('credit')
data.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LIMIT_BAL</th>
      <th>SEX</th>
      <th>EDUCATION</th>
      <th>MARRIAGE</th>
      <th>AGE</th>
      <th>PAY_1</th>
      <th>PAY_2</th>
      <th>PAY_3</th>
      <th>PAY_4</th>
      <th>PAY_5</th>
      <th>...</th>
      <th>BILL_AMT4</th>
      <th>BILL_AMT5</th>
      <th>BILL_AMT6</th>
      <th>PAY_AMT1</th>
      <th>PAY_AMT2</th>
      <th>PAY_AMT3</th>
      <th>PAY_AMT4</th>
      <th>PAY_AMT5</th>
      <th>PAY_AMT6</th>
      <th>default</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>20000</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>24</td>
      <td>2</td>
      <td>2</td>
      <td>-1</td>
      <td>-1</td>
      <td>-2</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>689.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>90000</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>34</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>14331.0</td>
      <td>14948.0</td>
      <td>15549.0</td>
      <td>1518.0</td>
      <td>1500.0</td>
      <td>1000.0</td>
      <td>1000.0</td>
      <td>1000.0</td>
      <td>5000.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>50000</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>37</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>28314.0</td>
      <td>28959.0</td>
      <td>29547.0</td>
      <td>2000.0</td>
      <td>2019.0</td>
      <td>1200.0</td>
      <td>1100.0</td>
      <td>1069.0</td>
      <td>1000.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>50000</td>
      <td>1</td>
      <td>2</td>
      <td>1</td>
      <td>57</td>
      <td>-1</td>
      <td>0</td>
      <td>-1</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>20940.0</td>
      <td>19146.0</td>
      <td>19131.0</td>
      <td>2000.0</td>
      <td>36681.0</td>
      <td>10000.0</td>
      <td>9000.0</td>
      <td>689.0</td>
      <td>679.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>50000</td>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>37</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>19394.0</td>
      <td>19619.0</td>
      <td>20024.0</td>
      <td>2500.0</td>
      <td>1815.0</td>
      <td>657.0</td>
      <td>1000.0</td>
      <td>1000.0</td>
      <td>800.0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 24 columns</p>
</div>




```python
# 查看数据各类别数
category_counts = data['default'].value_counts()
category_counts
```




    default
    0    18694
    1     5306
    Name: count, dtype: int64




```python
from pycaret.classification import *
s = setup(data = data, target = 'default', fix_imbalance = True, verbose = False)
```


```python
# 可以看到类1数据量变多了
s.get_config('dataset_transformed')['default'].value_counts()
```




    default
    0    18694
    1    14678
    Name: count, dtype: int64



## 2.5 异常值处理

PyCaret的remove_outliers函数可以在训练模型之前识别和删除数据集中的异常值。它使用奇异值分解技术进行PCA线性降维来识别异常值，并可以通过setup中的outliers_threshold参数控制异常值的比例（默认0.05）。


```python
from pycaret.datasets import get_data

data = get_data(dataset='./datasets/insurance', verbose=False)
# insurance = get_data('insurance')
# 数据维度
data.shape
```




    (1338, 7)




```python
from pycaret.regression import *
s = setup(data = data, target = 'charges', remove_outliers = True ,verbose = False, outliers_threshold = 0.02)
# 移除异常数据后，数据量变少
s.get_config('dataset_transformed').shape
```




    (1319, 10)



## 2.6 特征重要性

特征重要性是一种用于选择数据集中对预测目标变量最有贡献的特征的过程。与使用所有特征相比，仅使用选定的特征可以减少过拟合的风险，提高准确性，并缩短训练时间。在PyCaret中，可以通过使用feature_selection参数来实现这一目的。对于PyCaret中几个与特征选择相关参数的解释如下：

+ feature_selection：用于指定是否在模型训练过程中进行特征选择。可以设置为 True 或 False。
+ feature_selection_method：特征选择方法：
    - 'univariate': 使用sklearn的SelectKBest，基于统计测试来选择与目标变量最相关的特征。
    - 'classic（默认）': 使用sklearn的SelectFromModel，利用监督学习模型的特征重要性或系数来选择最重要的特征。
    - 'sequential': 使用sklearn的SequentialFeatureSelector，该类根据指定的算法（如前向选择、后向选择等）以及性能指标（如交叉验证得分）逐步选择特征。
+ n_features_to_select：特征选择的最大特征数量或比例。如果<1，则为起始特征的比例。默认为0.2。该参数在计数时不考虑 ignore_features 和 keep_features 中的特征。



```python
from pycaret.datasets import get_data
data = get_data('./datasets/diabetes')
```


<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number of times pregnant</th>
      <th>Plasma glucose concentration a 2 hours in an oral glucose tolerance test</th>
      <th>Diastolic blood pressure (mm Hg)</th>
      <th>Triceps skin fold thickness (mm)</th>
      <th>2-Hour serum insulin (mu U/ml)</th>
      <th>Body mass index (weight in kg/(height in m)^2)</th>
      <th>Diabetes pedigree function</th>
      <th>Age (years)</th>
      <th>Class variable</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>6</td>
      <td>148</td>
      <td>72</td>
      <td>35</td>
      <td>0</td>
      <td>33.6</td>
      <td>0.627</td>
      <td>50</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>85</td>
      <td>66</td>
      <td>29</td>
      <td>0</td>
      <td>26.6</td>
      <td>0.351</td>
      <td>31</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>8</td>
      <td>183</td>
      <td>64</td>
      <td>0</td>
      <td>0</td>
      <td>23.3</td>
      <td>0.672</td>
      <td>32</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>89</td>
      <td>66</td>
      <td>23</td>
      <td>94</td>
      <td>28.1</td>
      <td>0.167</td>
      <td>21</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>137</td>
      <td>40</td>
      <td>35</td>
      <td>168</td>
      <td>43.1</td>
      <td>2.288</td>
      <td>33</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>



```python
from pycaret.regression import *
# feature_selection选择特征, n_features_to_select选择特征比例
s = setup(data = data, target = 'Class variable', feature_selection = True, feature_selection_method = 'univariate',
          n_features_to_select = 0.3, verbose = False)
```


```python
# 查看哪些特征保留下来
s.get_config('X_transformed').columns
s.get_config('X_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Plasma glucose concentration a 2 hours in an oral glucose tolerance test</th>
      <th>Body mass index (weight in kg/(height in m)^2)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>56</th>
      <td>187.0</td>
      <td>37.700001</td>
    </tr>
    <tr>
      <th>541</th>
      <td>128.0</td>
      <td>32.400002</td>
    </tr>
    <tr>
      <th>269</th>
      <td>146.0</td>
      <td>27.500000</td>
    </tr>
    <tr>
      <th>304</th>
      <td>150.0</td>
      <td>21.000000</td>
    </tr>
    <tr>
      <th>32</th>
      <td>88.0</td>
      <td>24.799999</td>
    </tr>
  </tbody>
</table>
</div>



## 2.7 归一化

**数据归一化**

在 PyCaret 中，normalize 和 normalize_method 参数用于数据预处理中的特征缩放操作。特征缩放是指将数据的特征值按比例缩放，使之落入一个小的特定范围，这样可以消除特征之间的量纲影响，使模型训练更加稳定和准确。下面是关于这两个参数的说明：

+ normalize: 这是一个布尔值参数，用于指定是否对特征进行缩放。默认情况下，它的取值为 False，表示不进行特征缩放。如果将其设置为 True，则会启用特征缩放功能。
+ normalize_method: 这是一个字符串参数，用于指定特征缩放的方法。可选的值有：
    + zscore（默认）: 使用 Z 分数标准化方法，也称为标准化或 Z 标准化。该方法将特征的值转换为其 Z 分数，即将特征值减去其均值，然后除以其标准差，从而使得特征的均值为 0，标准差为 1。
    + minmax: 使用 Min-Max 标准化方法，也称为归一化。该方法将特征的值线性转换到指定的最小值和最大值之间，默认情况下是 [0, 1] 范围。
    + maxabs: 使用 MaxAbs 标准化方法。该方法将特征的值除以特征的最大绝对值，将特征的值缩放到 [-1, 1] 范围内。
    + robust: 使用 RobustScaler 标准化方法。该方法对数据的每个特征进行中心化和缩放，使用特征的中位数和四分位数范围来缩放特征。




```python
from pycaret.datasets import get_data
data = get_data('./datasets/pokemon')
data.head()
```


<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1</th>
      <th>Type 2</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
      <th>Legendary</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Bulbasaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>318</td>
      <td>45</td>
      <td>49</td>
      <td>49</td>
      <td>65</td>
      <td>65</td>
      <td>45</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Ivysaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>405</td>
      <td>60</td>
      <td>62</td>
      <td>63</td>
      <td>80</td>
      <td>80</td>
      <td>60</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>525</td>
      <td>80</td>
      <td>82</td>
      <td>83</td>
      <td>100</td>
      <td>100</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>VenusaurMega Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>625</td>
      <td>80</td>
      <td>100</td>
      <td>123</td>
      <td>122</td>
      <td>120</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Charmander</td>
      <td>Fire</td>
      <td>NaN</td>
      <td>309</td>
      <td>39</td>
      <td>52</td>
      <td>43</td>
      <td>60</td>
      <td>50</td>
      <td>65</td>
      <td>1</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>





<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1</th>
      <th>Type 2</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
      <th>Legendary</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Bulbasaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>318</td>
      <td>45</td>
      <td>49</td>
      <td>49</td>
      <td>65</td>
      <td>65</td>
      <td>45</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Ivysaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>405</td>
      <td>60</td>
      <td>62</td>
      <td>63</td>
      <td>80</td>
      <td>80</td>
      <td>60</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>525</td>
      <td>80</td>
      <td>82</td>
      <td>83</td>
      <td>100</td>
      <td>100</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>VenusaurMega Venusaur</td>
      <td>Grass</td>
      <td>Poison</td>
      <td>625</td>
      <td>80</td>
      <td>100</td>
      <td>123</td>
      <td>122</td>
      <td>120</td>
      <td>80</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Charmander</td>
      <td>Fire</td>
      <td>NaN</td>
      <td>309</td>
      <td>39</td>
      <td>52</td>
      <td>43</td>
      <td>60</td>
      <td>50</td>
      <td>65</td>
      <td>1</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>




```python
# 归一化
from pycaret.classification import *
s = setup(data, target='Legendary', normalize=True, normalize_method='robust', verbose=False)
```

数据归一化结果：


```python
s.get_config('X_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1_Water</th>
      <th>Type 1_Normal</th>
      <th>Type 1_Ice</th>
      <th>Type 1_Psychic</th>
      <th>Type 1_Fire</th>
      <th>Type 1_Rock</th>
      <th>Type 1_Fighting</th>
      <th>Type 1_Grass</th>
      <th>...</th>
      <th>Type 2_Electric</th>
      <th>Type 2_Normal</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>403</th>
      <td>-0.021629</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.195387</td>
      <td>-0.333333</td>
      <td>0.200000</td>
      <td>0.875</td>
      <td>1.088889</td>
      <td>0.125</td>
      <td>-0.288889</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>471</th>
      <td>0.139870</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.179104</td>
      <td>0.333333</td>
      <td>0.555556</td>
      <td>-0.100</td>
      <td>-0.111111</td>
      <td>-0.100</td>
      <td>1.111111</td>
      <td>0.333333</td>
    </tr>
    <tr>
      <th>238</th>
      <td>-0.448450</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>-1.080054</td>
      <td>-0.500000</td>
      <td>-0.555556</td>
      <td>-0.750</td>
      <td>-0.777778</td>
      <td>-1.000</td>
      <td>-0.333333</td>
      <td>-0.333333</td>
    </tr>
    <tr>
      <th>646</th>
      <td>0.604182</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>-0.618725</td>
      <td>-0.166667</td>
      <td>-0.333333</td>
      <td>-0.500</td>
      <td>-0.555556</td>
      <td>-0.500</td>
      <td>0.222222</td>
      <td>0.666667</td>
    </tr>
    <tr>
      <th>69</th>
      <td>-0.898342</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>-0.265943</td>
      <td>-0.833333</td>
      <td>-0.888889</td>
      <td>-1.000</td>
      <td>1.222222</td>
      <td>0.000</td>
      <td>0.888889</td>
      <td>-0.666667</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 46 columns</p>
</div>



**特征变换**

归一化会重新调整数据，使其在新的范围内，以减少方差中幅度的影响。特征变换是一种更彻底的技术。通过转换改变数据的分布形状，使得转换后的数据可以被表示为正态分布或近似正态分布。PyCaret中通过transformation参数开启特征转换，transformation_method设置转换方法：yeo-johnson（默认）和分位数。此外除了特征变换，还有目标变换。目标变换它将改变目标变量而不是特征的分布形状。此功能仅在pycarte.regression模块中可用。使用transform_target开启目标变换，transformation_method设置转换方法。


```python
from pycaret.classification import *
s = setup(data = data, target = 'Legendary', transformation = True, verbose = False)
# 特征变换结果
s.get_config('X_transformed').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>Name</th>
      <th>Type 1_Psychic</th>
      <th>Type 1_Water</th>
      <th>Type 1_Rock</th>
      <th>Type 1_Grass</th>
      <th>Type 1_Dragon</th>
      <th>Type 1_Ghost</th>
      <th>Type 1_Bug</th>
      <th>Type 1_Fairy</th>
      <th>...</th>
      <th>Type 2_Electric</th>
      <th>Type 2_Bug</th>
      <th>Total</th>
      <th>HP</th>
      <th>Attack</th>
      <th>Defense</th>
      <th>Sp. Atk</th>
      <th>Sp. Def</th>
      <th>Speed</th>
      <th>Generation</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>165</th>
      <td>52.899003</td>
      <td>0.009216</td>
      <td>0.043322</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>...</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>93.118403</td>
      <td>12.336844</td>
      <td>23.649090</td>
      <td>13.573010</td>
      <td>10.692443</td>
      <td>8.081703</td>
      <td>26.134255</td>
      <td>0.900773</td>
    </tr>
    <tr>
      <th>625</th>
      <td>140.730289</td>
      <td>0.009216</td>
      <td>-0.000000</td>
      <td>0.095739</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>...</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>66.091344</td>
      <td>9.286671</td>
      <td>20.259153</td>
      <td>13.764668</td>
      <td>8.160482</td>
      <td>6.056644</td>
      <td>9.552506</td>
      <td>3.679456</td>
    </tr>
    <tr>
      <th>628</th>
      <td>141.283084</td>
      <td>0.009216</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>0.043322</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>...</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>89.747939</td>
      <td>10.823299</td>
      <td>29.105379</td>
      <td>11.029571</td>
      <td>11.203335</td>
      <td>6.942091</td>
      <td>27.793080</td>
      <td>3.679456</td>
    </tr>
    <tr>
      <th>606</th>
      <td>137.396878</td>
      <td>0.009216</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>0.061897</td>
      <td>-0.000000</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>...</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>56.560577</td>
      <td>8.043018</td>
      <td>10.276208</td>
      <td>10.604937</td>
      <td>6.949265</td>
      <td>6.302465</td>
      <td>19.943809</td>
      <td>3.679456</td>
    </tr>
    <tr>
      <th>672</th>
      <td>149.303914</td>
      <td>0.009216</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>-0.000000</td>
      <td>0.029706</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>...</td>
      <td>-0.0</td>
      <td>-0.0</td>
      <td>72.626190</td>
      <td>10.202245</td>
      <td>26.061259</td>
      <td>11.435493</td>
      <td>7.199607</td>
      <td>6.302465</td>
      <td>20.141156</td>
      <td>3.679456</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 46 columns</p>
</div>






# 3 参考

+ [pycaret](https://github.com/pycaret/pycaret)
+ [pycaret-docs](https://pycaret.gitbook.io/docs)
+ [pycaret-datasets](https://github.com/pycaret/pycaret/tree/master/datasets)
+ [lightgbm](https://lightgbm.readthedocs.io/en/latest/GPU-Tutorial.html)
+ [cuml](https://github.com/rapidsai/cuml)
+ [imbalanced-learn](https://imbalanced-learn.org/stable/)
