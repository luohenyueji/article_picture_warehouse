# 导入需要的包
from PIL import Image, ImageFont, ImageDraw,ImageEnhance
import os,random

def gen(word1,word2=None,src_img=None):
    index = src_img.split('.')[0]
    src_img = os.path.join('img',src_img)
    # 读取图片
    src_img = Image.open(src_img)
    # 调整亮度
    enhancer = ImageEnhance.Brightness(src_img)
    src_img = enhancer.enhance(0.64)
    # pil_image 接收住这个图片对象
    # width 为图片的宽, height为图片的高
    width, height = src_img.size
    src_img = src_img.resize((int(height/3.2/3*4), int(height/3.2)))
    width, height = src_img.size

    SimHei = "./SourceHanSansSC-Heavy-2.otf"    # 一个字体文件
    font = ImageFont.truetype(SimHei, 200)  # 设置字体和大小
    
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(src_img)
    if word2 is not None:  
        w1, h1 = font.getsize(word1)
        w2, h2 = font.getsize(word2)
        draw.text(((width-w1)/2, (height)/2-h1), word1, fill="#FFFFFF", font=font)
        draw.text(((width-w2)/2, (height)/2), word2, fill="#FFFFFF", font=font)
        # 保存画布
        src_img.save(f"my/{word1+word2}.png", "PNG",dpi=(300,300))
    else:
        w, h = font.getsize(word1)
        draw.text(((width-w)/2, (height-h)/2), word1, fill="#FFFFFF", font=font)
        # 保存画布
        src_img.save(f"my/{word1}.png", "PNG",dpi=(300,300))


with open("list.txt",'r',encoding='utf-8') as f:
    
    src_imgs = os.listdir('img')
    random.shuffle(src_imgs)
    for index,line in enumerate(f.readlines()):
        line = line.strip().split(' ')
        if len(line)>1: 
            gen(line[0],line[1],src_img=src_imgs[index])
        else:
            gen(line[0],src_img=src_imgs[index])
        